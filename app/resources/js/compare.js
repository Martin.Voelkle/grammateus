$(document).ready(function () {
            $('#cat1').DataTable({
                //default order
                order: [[1, "asc"],[0, 'asc']],
                //hide ID column
                "columnDefs": [
                    { "visible": false, "targets": [2, 4, 6, 7] }
                ],
                //remove table features
                searching: false,
                lengthChange: false,
                "pagingType": "numbers"
            });
            $('#cat2').DataTable({
                //default order
                order: [[1, "asc"],[0, 'asc']],
                // hide ID column
                "columnDefs": [
                    { "visible": false, "targets": [2, 4, 6, 7] }
                ],
                searching: false,
                lengthChange: false,
                "pagingType": "numbers"
            });
        });