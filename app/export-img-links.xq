xquery version "3.1";

import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare option exist:serialize "method=html media-type=text/html";


    <html>
        <ol>
            {for $d in doc($g:data || "external_links_images.xml")//tei:item
                
                return
                <li>
                    {"TM " || $d/@xml:id => functx:substring-after-last("_") || ": "}
                    <ul>{for $link in $d/tei:ref[not(@type)]
                        return
                            <li><a href="{$link/@target/string()}">{$link/@target/string()}</a></li>
                    }</ul>
                </li>
            }
        </ol>
    </html>