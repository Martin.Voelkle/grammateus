xquery version "3.1";

module namespace papyri="http://grammateus.ch/pages/papyri";

import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "../gramm.xql";
import module namespace search-facets="http://grammateus.ch/search-facets" at "../search-facets.xql";

(: default value for search-facets:advanced-search parameters. :)
declare variable $papyri:pagelength := 25;
declare variable $papyri:keyword := request:get-parameter("keyword", "");

declare function papyri:page() {
(
    <div class="col">
        <div class="card search-info">
            <div class="card-header search-info-btn" id="headingOne">
                    
                        Search Help &amp; Tips {gramm:fa-question()}
                    
            </div>
            <div id="collapseOne" class="" aria-labelledby="headingOne">
                <div class="card-body">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
    <div class="card-header" id="headingTwo">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          {gramm:fa-down()} General
        </button>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <ul>
            <li>By default, all the papyri of our <a href="{$g:root}introduction/general#corpus">database</a> are shown. You can start a search by filtering the database using the filters (below).</li>
            <li>If your search returns <span class="alert-secondary">0 papyri</span>, it means that the parameters you have selected cannot be combined. Try removing one of them! </li>
            <li>It is recommended to filter one parameter at a time (e.g. first select one or more document type, and later refine your search by selecting a subtype).</li>
            <li>You can <b>sort</b> the papyri according to publication title (default), TM numbers, Type of document, and Place.</li>
            <li>For further analysis, you can <b>export</b> your search results in csv format.</li>
            <li>If you see the IIIF <img src="{$g:img}logo_iiif.png" height="20px"></img> or Figshare <img src="{$g:img}logo-figshare.jpg" height="20px"></img> logo, it means that you will be able to see the papyrus image next to the text.</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          {gramm:fa-down()} Keyword
        </button>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <ul>
            <li>The keyword search is a full-text search of our metadata. It includes the papyrus title, identifiers such as Trismegistos numbers, subjects and place names.</li>
            <li>The place names come from HGV metadata and are generally in German (e.g. Tebtynis instead of Tebtunis).</li>
            <li>The <b>subjects</b> are derived from HGV subjects, which you can see in <a href="{$g:papinfo}" target="_blank">papyri.info</a> among the HGV metadata, under the heading 'Subjects'. They are available in English and can be used to find documents that are known under different labels. Example: searching for <span class="alert-secondary">warrant</span>, <span class="alert-secondary">order to arrest</span> or <span class="alert-secondary">summons</span> will return all the documents corresponding to these terms.</li>
            <li><b>Operators</b>: it is possible to use operators like OR, NOT or the * wildcard to make more complex searches. Here are some examples: <ol>
                <li><span class="alert-secondary">BGU Arsinoites</span> is the equivalent of searching <span class="alert-secondary">BGU AND Arsinoites</span>, since AND is the default operator.</li>
                <li><span class="alert-secondary">BGU NOT Arsinoites</span> will find BGU papyri not originating from the Arsinoite Nome. Note that you cannot use the NOT operator alone! It is not possible to search for <span class="alert-secondary">NOT Arsinoites</span>, however you could search for <span class="alert-secondary">g* NOT Arsinoites</span> which is roughly equivalent since all papyri have the word "grammateus" in their metadata.</li>
                <li><span class="alert-secondary">BGU OR P.Oxy</span> will find either BGU or P.Oxy. papyri.</li>
                <li><span class="alert-secondary">oxy*</span> will find papyri with a provenance of Oxyrhynchos, Oxyrhynchos?, Oxyrhynchites, Oxyrhyncha, etc. The wildcard replaces 0 or more characters.</li>
            </ol> </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          {gramm:fa-down()} Texts - Categories, Sections, Shape and Fibres
        </button>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
        <ul>
            <li>The Types, Subtypes and Variations of documents are further described in the <a href="{$g:root}typology">Typology</a>. See the <a href="{$g:root}descriptions">Descriptions</a> pages.</li>
            <li>You can search papyri that contain a particular text section or a combination of them (e.g. papyri that have both a heading and a description of content).</li>
            <li>We have divided papyri in three possible shapes: horizontal, vertical, and squarish.</li>
            <li>Fibres are vertical, horizontal, or mixed. Mixed fibres papyri have a part of horizontal and a part of vertical fibres, for instance due to two sheets pasted together.</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFive">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          {gramm:fa-down()} Provenance and Date
        </button>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
      <p>Provenance and date are from  the HGV data on <a href="{$g:papinfo}" target="_blank">papyri.info</a>, encoded in <a href="https://www.stoa.org/epidoc/gl/latest/supp-history.html" target="_blank">EpiDoc</a> XML.</p>
        <ul>
            <li>The <b>Provenance</b> is the Nome when available, or a more precise place name depending on the precision of the HGV metadata. <br/>
                <div class="alert alert-primary" role="alert">
                    <i class="fas fa-exclamation-circle"/>
                    <b> Technical note</b><br/> the Nome is usually encoded with a <span class="code">&lt;provenance&gt;</span> element. However, not all papyri have provenance element in their metadata. When there is no <span class="code">&lt;provenance&gt;</span> element, the Nome may also be extracted from <span class="code">&lt;origPlace&gt;</span>, if it is present in parenthesis. If the Nome cannot be safely deduced, then the content of <span class="code">&lt;origPlace&gt;</span> is used.
                        
                </div>
            </li>
            <li>The <b>Date</b> can be filtered by selecting a range of centuries, within the corpus bounds (Ptolemaic and Roman period). The search is loose, meaning that the date range of a document must overlap with the selected range. Papyri that have an incomplete range, i.e. only one terminus post or ante quem, are given a range of 50 years before/after their terminus.<br/>
            When there are multiple datations for one papyrus, we consider only the first one. For a precise datation, please check <a href="https://aquila.zaw.uni-heidelberg.de/start">HGV</a>.
                                        </li>
        </ul>
      </div>
    </div>
  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>,
    search-facets:advanced-search($papyri:keyword, $papyri:pagelength)   
)
};