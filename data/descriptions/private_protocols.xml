<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d31">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Private Protocol</title>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:ejxvvgq2dnec5pl5sejttw6wai</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_pp"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2020-05-25">TEI file created</change>
            <change who="../authority-lists.xml#PS" when="2020-05-20">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Private Protocol</head>
            <div xml:id="d31_div1_introduction" type="section" n="1">
                <head xml:id="introduction">Introduction</head>
                <p xml:id="d31_p1">The so-called private protocol was described in detail by
                            <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1975"/>
                    </bibl> and <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978b"/>
                    </bibl>, who underlines the dual nature of such documents: on the one hand, they
                    seem to follow the formulation in use in documents written by notarial offices
                    (objective formulation or homology, <term xml:lang="lat">praxis</term> and <term xml:lang="lat">kyria</term> clauses, often also <term xml:lang="lat">stipulatio</term> from III CE); on the other hand, they display features
                    that belong to privately drafted contracts, especially the [date@end], which is
                    a clear sign that they were not produced by a notarial office. Wolff uses the
                    label <q>private protocol</q> to highlight the fact that such documents were
                    indeed produced in a private environment. They are, however, consistently
                    written by trained scribes.</p>
                <p xml:id="d31_p2">Private protocols are attested for the most part in the
                    Oxyrhynchite nome, but also occur in the Arsinoite nome, and sporadically in
                    other nomes. Although the Oxyrhynchite and Arsinoite types display some common
                    features, there are also differences. <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1975"/>
                        <citedRange>351</citedRange>
                    </bibl> argues that both types developed independently.</p>
                <div xml:id="d31_div12_pp_oxy" type="subsection" n="1">
                    <head xml:id="pp_oxy">Oxyrhynchite nome</head>
                    <p xml:id="d31_p3">Oxyrhynchite private protocols are first attested in 19 BCE
                            [<ref target="https://papyri.info/ddbdp/p.oxy;2;277" type="external">20548</ref>], where the formulation mirrors that of documents produced
                        by notarial offices only a few years earlier, e.g. [<ref target="https://papyri.info/ddbdp/p.oxy;14;1629" type="external">5255</ref> 44 BCE]; this does not exclude the possibility that private
                        protocols existed at an earlier date, in the Ptolemaic period. They are
                        found throughout the Roman period, up to the IV CE.</p>
                    <p xml:id="d31_p4">Documents drawn up as private protocols in the Oxyrhynchite
                        nome do not appear to relate to sales or loans; they are used mostly for
                        contracts of lease [<ref target="https://papyri.info/ddbdp/p.oxy;38;2874" type="external">22558</ref> 108 CE; <ref type="internal" target="20633.xml">20633</ref> II CE; <ref type="internal" target="80724.xml">80724</ref> II CE; <ref type="internal" target="20636.xml">20636</ref> II CE; <ref type="internal" target="21362.xml">21362</ref> III CE]. It is only in the III CE that
                        lease agreements in the form of a <term xml:lang="lat">hypomnema</term>
                        (e.g. [<ref type="descr" target="#gt_proposalContract">proposal to
                            contract</ref>], [<ref type="descr" target="#gt_undertaking">undertaking</ref>]) start to compete with the private protocol in this
                        nome.</p>
                    <p xml:id="d31_p5">There are also contracts of apprenticeship [<ref target="https://papyri.info/ddbdp/p.oxy;41;2971" type="external">16535</ref> 66 CE; <ref type="internal" target="20425.xml">20425</ref>
                        II CE; <ref type="internal" target="16900.xml">16900</ref> III CE], as well
                        as other small transactions between individuals, e.g. exchange of liturgies
                            [<ref type="internal" target="18507.xml">18507</ref> III CE; <ref type="internal" target="16560.xml">16560</ref> III CE], payment for the
                        superintendence of transport [<ref target="https://papyri.info/ddbdp/p.oxy;14;1626" type="external">21939</ref> 325 CE], hire of a secretary [<ref type="internal" target="21360.xml">21360</ref> III CE] or entertainers [<ref type="internal" target="16593.xml">16593</ref> III CE].</p>
                </div>
                <div xml:id="d31_div12_pp_ars" type="subsection" n="2">
                    <head xml:id="pp_ars">Arsinoite nome</head>
                    <p xml:id="d31_p6">Private protocols in the Arsinoite nome are never used for
                        contracts of lease: they pertain to loans [<ref type="internal" target="14118.xml">14118</ref> III CE; <ref target="https://papyri.info/ddbdp/stud.pal;20;51" type="external">15023</ref> 238 CE] and other agreements relating to legal obligations,
                        such as sales of animals [<ref type="internal" target="13078.xml">13078</ref> III CE]. The time span is later than in the Oxyrhynchite
                        nome, from the late III to the IV CE.</p>
                </div>
            </div>
            <div xml:id="d31_div2_structure" type="section" n="2">
                <head xml:id="structure">Structure</head>
                <p xml:id="d31_p7">The Oxyrhynchite private protocol begins with an immediate
                    objective statement. Contracts of lease are introduced by <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d31_s2">[<term xml:lang="grc">ἐμίσθωσεν</term>]</seg> [<ref type="internal" target="22222.xml" corresp="#d31_s2">22222</ref> II CE], the parties are
                    named and the property described. In other cases the objective statement is a
                    mutual homology, <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d31_s3">[<term xml:lang="grc">ὁμολογοῦσιν ἀλλήλοις</term>]</seg>
                        [<ref type="internal" target="16540.xml" corresp="#d31_s3">16540</ref> III
                    CE]. There is a <seg type="syntax" corresp="../authority_lists.xml#date" xml:id="d31_s1">[date@end]</seg> after the main text, and this may be
                    followed by a subscription [<ref target="15404.xml" corresp="#d31_s1" type="internal">15404</ref> 219 CE].</p>
                <p xml:id="d31_p8">Private protocols from the Arsinoite nome also start with an
                    immediate objective statement, but the wording is not the same: they are
                    introduced by verbs such as <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d31_s4">[<term xml:lang="grc">ὁμολογεῖ</term>]</seg> [<ref target="https://papyri.info/ddbdp/stud.pal;20;51" type="external" corresp="#d31_s4">15023</ref>; <ref type="internal" target="13078.xml" corresp="#d31_s4">13078</ref>] or <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d31_s5">[<term xml:lang="grc">προσεφώνησεν</term>]</seg> [<ref type="internal" target="14118.xml" corresp="#d31_s5">14118</ref>]; unlike the Oxyrhynchite
                    documents, there is no mutual homology.</p>
            </div>
            <div xml:id="d31_div3_format" n="3" type="section">
                <head xml:id="format">Format</head>
                <p xml:id="d31_p9">Private protocols from the Oxyrhynchite nome are often
                    distinctively prepared on elongated and narrow sheets (ranging H. 25 to 39cm, W.
                    6.5 to 12.5cm), with horizontal fibres [<ref type="internal" target="21745.xml">21745</ref> II CE; <ref type="internal" target="15404.xml">15404</ref>;
                        <ref type="internal" target="22222.xml">22222</ref>]. A shorter format, with
                    a reduced contrast between height and width, also occurs, but less frequently
                        [<ref type="internal" target="80724.xml">80724</ref>]. </p>
                <p xml:id="d31_p10">Although the elongated proportions that prevail in the
                    Oxyrhynchite nome also occur in the Arsinoite [<ref type="internal" target="13078.xml">13078</ref>], more often sheets in that nome were cut
                    from rolls with a height of c. 21 cm, the standard <term xml:lang="lat">pagina</term> dimension in this area [<ref type="internal" target="14118.xml">14118</ref>; <ref target="https://papyri.info/ddbdp/stud.pal;20;51" type="external">15023</ref>].</p>
            </div>
            <div xml:id="d31_div4_layout" n="4" type="section">
                <head xml:id="layout">Layout</head>
                <p xml:id="d31_p11">The layout appears to be similar in all nomes. The text is
                    written by the scribe as a single block, in one column, often with few
                    distinguishing features [<ref type="internal" target="18507.xml">18507</ref>;
                        <ref type="internal" target="19308.xml">19308</ref> 200 CE, Oxyrhynchus].
                    Some documents carry an enlarged first letter [<ref type="internal" target="16900.xml">16900</ref>; <ref type="internal" target="14118.xml">14118</ref>], and in some cases also an exaggerated (<term xml:lang="grc">ἔτους</term>) symbol [<ref type="internal" target="14118.xml">14118</ref>;
                        <ref type="internal" target="21745.xml">21745</ref>]. There may be a line
                    drawn between the date and the subscription [<ref type="internal" target="14118.xml">14118</ref>; <ref type="internal" target="21745.xml">21745</ref>], and there is evidence of some use of <term xml:lang="lat">ekthesis</term> [<ref type="internal" target="16540.xml">16540</ref> l.1]
                    and <term xml:lang="lat">eistheis</term> [<ref type="internal" target="14118.xml">14118</ref> l.19]. The left-hand margins usually range
                    between 1-1.5 cm, the top margins range 1-2 cm; there may be a large lower
                    margin, e.g. [<ref type="internal" target="21745.xml">21745</ref>].</p>
                <p xml:id="d31_p12">
                    <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1975"/>
                        <citedRange>353</citedRange>
                    </bibl> concludes that the choice of using a private protocol is not dictated by
                    legal criteria: <quote>which form was to be chosen was merely a question of
                        individual taste or of local tradition or fashion.</quote> This is well
                    illustrated by the case of the Sakaon Archive <bibl>
                        <author>TM Arch 206</author>
                        <ptr target="gramm:geens2013"/>
                    </bibl>, spanning the late III and early IV CE. This archive contains several
                    cases of the use of the private protocol (III CE [<ref target="https://papyri.info/ddbdp/p.sakaon;;94" type="external">13113</ref>;
                        <ref type="internal" target="13078.xml">13078</ref>; <ref target="https://papyri.info/ddbdp/p.sakaon;;58" type="external">13074</ref>
                    all III CE; <ref target="https://papyri.info/ddbdp/p.sakaon;;95" type="external">13114</ref>; <ref target="https://papyri.info/ddbdp/p.sakaon;;62" type="external">13079</ref> all IV CE], alongside other types of contracts
                    for similar transactions: compare the loan agreement drawn up as a private
                    protocol [<ref target="https://papyri.info/ddbdp/p.sakaon;;65" type="external">13082</ref> 328 CE] with that drawn up as a [<ref type="descr" target="#gt_cheiro">cheirographon</ref>] [<ref target="https://papyri.info/ddbdp/p.sakaon;;64" type="external">13081</ref>
                    307 CE], both bearing a label <term xml:lang="grc">χ(ει)ρ(όγραφον)</term> on the
                    back.</p>
            </div>
        </body>
    </text>
</TEI>