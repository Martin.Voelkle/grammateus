xquery version "3.1";

import module namespace xmldb="http://exist-db.org/xquery/xmldb";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare option exist:serialize "method=html media-type=text/html";

let $result :=
for $doc in collection("/db/apps/papyri-hgv-apis/HGV/")
where ($doc//tei:term[contains(text(), 'Kamel')] and $doc//tei:term[contains(text(), 'Deklaration')])
let $tm := $doc//tei:idno[@type eq "TM"]/text()
let $ddb := $doc//tei:idno[@type eq "ddb-hybrid"]/text()
let $apis := doc("/db/apps/papyri-hgv-apis/HGV/")//tei:idno[@type eq "TM"][text() eq $tm]
order by xs:integer($doc//tei:idno[@type eq "TM"]/text()) ascending
return
    <doc>
        <TM>{$doc//tei:idno[@type eq "TM"]/text()}</TM>
        <HGV>{$doc//tei:idno[@type eq "filename"]/text()}</HGV>
        <DDB>{$ddb}</DDB>
        <papyri-info>{concat("https://papyri.info/ddbdp/", $ddb)}</papyri-info>
        <series>{$doc//tei:bibl[@subtype eq "principal"]/tei:title[@type eq "abbreviated"]/text()}</series>
        <volume>{$doc//tei:bibl[@subtype eq "principal"]/tei:biblScope[@type eq "volume"]/text()}</volume>
        <fascicle>{$doc//tei:bibl[@subtype eq "principal"]/tei:biblScope[@type eq "fascicle"]/text()}</fascicle>
        <number>{$doc//tei:bibl[@subtype eq "principal"]/tei:biblScope[@type eq "numbers"]/text()}</number>
        <generic>{$doc//tei:bibl[@subtype eq "principal"]/tei:biblScope[@type eq "generic"]/text()}</generic>
        <!-- @type page? -->
        <text-type>Declaration</text-type>
        <text-subtype>Camel</text-subtype>
        <IIIF></IIIF>
        <image>{
            for $figure in $doc//tei:div[@type eq "figure"]/tei:p/tei:figure
            return 
                $figure/tei:graphic/@url/string()
        }</image>
        <printed-image>{$doc//tei:bibl[@type eq "illustration"]}</printed-image>
        <image-rights></image-rights>
        <fibres></fibres>
        <height>{$apis}</height>
        <width></width>
        <shape></shape>
        <writing-parallel></writing-parallel>
        <margin-top></margin-top>
        <margin-bottom></margin-bottom>
        <margin-left></margin-left>
        <margin-right></margin-right>
        <columns></columns>
        <blank-after></blank-after>
        <blank-height></blank-height>
        <seal></seal>
        <hand-nb></hand-nb>
        <section-nb></section-nb>
        <m1></m1>
        <m2></m2>
        <m3></m3>
        <m4></m4>
        <m5></m5>
        <m6></m6>
        <section1></section1>
        <section2></section2>
        <section3></section3>
    </doc>

return
    <html>
    <table>
        <tr>
            {for $elem in $result[1]/*
                return
                    <th>{$elem/name()}</th>
            }
        </tr>
        {for $r in $result
            return
                <tr>
                    {for $info in $r/*
                        return
                            <td>{$info}</td>
                    }
                </tr>
        }
    </table>
    </html>