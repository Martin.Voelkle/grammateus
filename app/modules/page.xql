xquery version "3.1";

(: fixed variables :)
import module namespace g="http://grammateus.ch/global" at "global.xql";

(: page building blocks :)
import module namespace page="http://grammateus.ch/page-elmts" at "page-elmts.xql";

(: pages modules :)
import module namespace home="http://grammateus.ch/pages/home" at "pages/home.xql";
import module namespace newhome="http://grammateus.ch/pages/newhome" at "pages/home-updated.xql";
import module namespace t="http://grammateus.ch/pages/typology" at "pages/typology.xql";
import module namespace x="http://grammateus.ch/pages/xmlpage" at "pages/xmlpage.xql";
import module namespace papyri="http://grammateus.ch/pages/papyri" at "pages/papyri.xql";
import module namespace browse="http://grammateus.ch/pages/browse" at "pages/browse.xql";
import module namespace biblio="http://grammateus.ch/pages/bibliography" at "pages/bibliography.xql";
import module namespace syntax="http://grammateus.ch/pages/syntax" at "pages/syntax.xql";
import module namespace compare="http://grammateus.ch/pages/compare" at "pages/compare.xql";
import module namespace error="http://grammateus.ch/pages/error" at "pages/error.xql";
import module namespace show="http://grammateus.ch/show" at "show.xql";
import module namespace howtocite="http://grammateus.ch/pages/howtocite" at "pages/howto-cite.xql";
import module namespace team="http://grammateus.ch/pages/team" at "pages/team.xql";
import module namespace rights="http://grammateus.ch/pages/rights" at "pages/rights.xql";
import module namespace tuto="http://grammateus.ch/pages/tutorials" at "pages/tutorials.xql";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare boundary-space preserve;
declare option output:method "html5";
declare option output:media-type "text/html";


(: point of entry for page requests :)


let $pagetype := request:get-parameter("pagetype", "home")
let $resource := request:get-parameter("resource", ())
let $exception := request:get-parameter("exception", ())

(: testing path/resource etc :)
let $exist_path := request:get-parameter("existpath", ())
let $exist_resource := request:get-parameter("existresource", ())
let $exist_controller := request:get-parameter("existcontroller", ())
let $exist_prefix := request:get-parameter("existprefix", ())
let $exist_root := request:get-parameter("existroot", ())

return

<html>
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="grammateus. The Architecture of Documentary Papyri" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="{$g:img}grammateus-favicon-32x32.ico" />
        <title>{page:title($pagetype, $resource)}</title>
        <link rel="stylesheet" href="{$g:fonts}fontawesome/css/all.css" />
        <link
            href="https://fonts.googleapis.com/css?family=Literata|Merriweather|Open+Sans&amp;display=swap"
            rel="stylesheet" />
        {page:css($pagetype, $resource)}
    </head>
    <body>
    {
(:        switch ($pagetype):)
(:        case "newhome":)
(:        return:)
            <nav class="navbar navbar-expand-sm navbar-light nav-position" role="navigation" id="grammateus-navbar">
            <a class="navbar-brand" href="{$g:root}home">
                <img src="{$g:img}logo-grammateus-red.png" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <div class="navbar-nav mr-auto">
                    <li class="dropdown">
                        <a class="nav-item nav-link dropdown-toggle" href="#" id="navbarIntro"
                            role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">Introduction</a>
                        <div class="dropdown-menu" aria-labelledby="navbarIntro">
                            <a class="dropdown-item" href="{$g:root}introduction/general">General</a>
                            <a class="dropdown-item" href="{$g:root}introduction/concepts">Key Concepts</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-item nav-link dropdown-toggle" href="#" id="navbarTypology"
                            role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">Typology</a>
                        <div class="dropdown-menu" aria-labelledby="navbarTypology">
                            <a class="dropdown-item" href="{$g:root}typology">Overview</a>
                            <a class="dropdown-item" href="{$g:root}descriptions">Descriptions</a>
                            <a class="dropdown-item" href="{$g:root}classification">Classification</a>
                            <a class="dropdown-item" href="{$g:root}syntax">Syntax</a>
                        </div>
                    </li>
                    <a id="navbarPapyri" class="nav-item nav-link" href="{$g:root}papyri.html">Papyri</a>
                    <a id="navbarCompare" class="nav-item nav-link" href="{$g:root}compare">Compare</a>
                    <a id="navbarBiblio" class="nav-item nav-link" href="{$g:root}bibliography">Bibliography</a>
                </div>
                <form action="{$g:root}browse.html" method="get"
                    class="form-inline my-2 my-lg-0 form-search">
                    <div class="input-group input-search">
                        <input type="text" name="keyword" aria-label="Search the website"
                            aria-describedby="addon-search" class="form-control text-search"
                            placeholder="" />
                        <div class="input-group-append">
                            <span class="input-group-text logo-search" id="addon-search">
                                <i class="fas fa-search"></i>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </nav>
        }
    
        <div id="content" class="container">
            {
                switch ($pagetype)
                case "home" return home:page()
                case "newhome" return newhome:page()
                case "browse" return browse:page()
                case "biblio" return biblio:page()
                case "introduction"
                case "descriptions" 
                case "classification" return x:page($pagetype, $resource)
                case "typology" return t:page()
                case "papyri" return papyri:page()
                case "doc" return show:page($resource)
                case "syntax" return syntax:page()
                case "compare" return compare:page()
                case "howto-cite" return howtocite:page()
                case "team" return team:page()
                case "rights" return rights:page()
                case "tutorials" return tuto:page()
                case "error" return error:page($resource, $exception)
                default return
                    <p>
                        <h1>Page type: {$pagetype}</h1>
                        <ul>
                            <li><b>exist:path</b>: {$exist_path}</li>
                            <li><b>exist:resource</b>: {$exist_resource}</li>
                            <li><b>exist:controller</b>: {$exist_controller}</li>
                            <li><b>exist:prefix</b>: {$exist_prefix}</li>
                            <li><b>exist:root</b>: {$exist_root}</li>
                        </ul>
                    </p>
            }
        </div>
        {
(:            switch ($pagetype):)
(:            case "newhome":)
(:            return:)
                <footer>
            <div class="container">
                <div class="row text-center">
                    <!--<div class="col">
                        <a href="" class="footer-link">Help/tutorials</a>
                    </div>-->
                    <div class="col">
                        <a href="{$g:root}howto-cite" class="footer-link">How to cite</a>
                    </div>
                    <div class="col">
                        <a href="{$g:root}team" class="footer-link">Team</a>
                    </div>
                    <div class="col">
                        <a href="{$g:root}rights"class="footer-link">Rights</a>
                    </div>
                    <div class="col">
                        <a href="{$g:fns}" target="_blank">
                            <img src="{$g:img}logo-fns.png" alt="SNFS" height="40px" />
                        </a>
                    </div>
                    <div class="col">
                        <a
                            href="https://www.unige.ch/lettres/antic/unites/grec/enseignants/schubert/grammateus/"
                            target="_blank">
                            <img src="{$g:img}logo-UNIGE_blanc_fondTransparent.gif"
                                alt="Unige" height="40px" />
                        </a>
                    </div>
                </div>
            </div>
        </footer>
            }
    </body>
    {page:scripts($pagetype, $resource)}
</html>
