xquery version "3.1";

module namespace gramm="http://grammateus.ch/gramm";

import module namespace g="http://grammateus.ch/global" at "global.xql";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace kwic="http://exist-db.org/xquery/kwic";
import module namespace functx="http://www.functx.com";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

(:: VARIABLES ::)
(: Mininum and Maximum papyrus dimensions :)
declare variable $gramm:width_min := min(collection("/db/apps/grammateus/data/papyri")//tei:width);
declare variable $gramm:width_max := max(collection("/db/apps/grammateus/data/papyri")//tei:width);
declare variable $gramm:height_min := min(collection("/db/apps/grammateus/data/papyri")//tei:height);
declare variable $gramm:height_max := max(collection("/db/apps/grammateus/data/papyri")//tei:height);

 
(:: GENERAL ::)

(:~ returns formatted link to a document
 : 
 : @param uri - string uri
 :)
declare function gramm:get-doc-link ($doc as node()) {
    <a href="document/{functx:substring-after-last(document-uri(root($doc)), "/")=> substring-before(".xml")}">
        {gramm:title($doc)}
    </a> 
 };

(:~ returns TM number
 : 
 : @param doc - XML EpiDoc node
 : @return <idno>
 :)
declare function gramm:tm ($doc as node()) {
    $doc//tei:idno[@type eq "TM"]
 };
 
 (:~ returns HGV number
 : 
 : @param doc - XML EpiDoc node
 : @return <idno>
 :)
 declare function gramm:hgv ($doc as node()) {
    $doc//tei:idno[@type eq "hgv"]
 };
 
  (:~ returns ddb-hybrid identifier
 : 
 : @param doc - XML EpiDoc node
 : @return <idno>
 :)
 declare function gramm:ddb-hybrid ($doc as node()) {
    $doc//tei:idno[@type eq "ddb-hybrid"]
 };
 
(:~ returns first of an space-separated list
 : 
 : @param string - string with one or more item (e.g. identifiers)
 : @return string
 :)
declare function gramm:first-item($string) {
    functx:substring-before-if-contains($string, ' ')
};
 
(:~ returns Title
 : 
 : @param doc - XML EpiDoc node
 : @return string
 :)
 declare function gramm:title ($doc as node()) {
    let $series := $doc//tei:bibl/tei:title/string()
    let $volume := $doc//tei:biblScope[@type eq "volume"]/string()
    let $fascicle := $doc//tei:biblScope[@type eq "fascicle"]/string()
    let $numbers := $doc//tei:biblScope[@type eq "numbers"]/string()
    let $side := $doc//tei:biblScope[@type eq "side"]/string()
    let $generic := if($doc//tei:biblScope[@type eq "generic"]) 
                    then "(" || $doc//tei:biblScope[@type eq "generic"]/string() || ")"
                    else ()
    let $page := $doc//tei:biblScope[@type eq "pages"]/string()
    let $number := $doc//tei:biblScope[@type eq "number"]/string()
    return
        string-join(($series, $volume, $fascicle, $numbers, $side, $generic, $page, $number), " ")
 };
 
(:: TEXT SECTIONS ::)

(:~ returns text section name from the authority list
 : 
 : @param id - @xml:id of a text section
 : @return <label> of a text section
 :)
declare function gramm:get-text-section-name($id) {
    doc($g:authority)//id($id)/tei:label
};

(:~ returns text section number from the authority list
 : for ordering purposes
 : 
 : @param id - @xml:id of a text section
 : @return @n of a text section as string
 :)
declare function gramm:get-text-section-nb($id) {
    doc($g:authority)//id($id)/@n
};

(:~ returns the text section of a locus element/sequence, for the faceted search
 : 
 : @param locus - locus node or sequence of nodes
 : @return string - Text section name of the locus
 :)
declare function gramm:has-s($locus) {
        if($locus)
        then
            let $s := doc($g:authority)//tei:list[@type eq 'layout']/id($locus/substring-after(@ana, '#'))
            return $s/tei:label/string()
        else ()
};
 
(: fa-icons :)
declare function gramm:fa-info() {
    <i class="fas fa-info-circle"/>
};
declare function gramm:fa-shape-vertical() {
    <i class="far fa-file"/>
};
declare function gramm:fa-shape-horizontal() {
    <i class="far fa-file icon-rotate"/>
};
(: <i class="far fa-sticky-note"/> :)
declare function gramm:fa-shape-square() {
    <i class="far fa-square"/>
};
declare function gramm:fa-fibre-vertical() {
    <i class="fas fa-arrow-down"/>
};
declare function gramm:fa-fibre-horizontal() {
    <i class="fas fa-arrow-right"/>
};
declare function gramm:fa-search() {
    <i class="fas fa-search"/>
};
declare function gramm:fa-image() {
    <i class="fas fa-camera fa-2x"/>
};
declare function gramm:fa-question() {
    <i class="fa fa-question-circle"></i>
};
declare function gramm:fa-times() {
    <i class="fa fa-times-circle"></i>
};
declare function gramm:fa-up() {
    <i class="fas fa-caret-up"></i>
};
declare function gramm:fa-down() {
    <i class="fas fa-caret-down"></i>
};
declare function gramm:fa-map() {
    <i class="fas fa-map"></i>
};
declare function gramm:fa-calendar() {
    <i class="far fa-calendar-alt"></i>
};
declare function gramm:fa-next() {
    <i class="fas fa-chevron-right"></i>
};
declare function gramm:fa-previous() {
    <i class="fas fa-chevron-left"></i>
};
declare function gramm:fa-open() {
    <i class="fas fa-external-link-alt"></i>
};

declare function gramm:iiif-logo() {
    <img src="{$g:img}logo_iiif.png" style="height: 20px; margin: 5px;"></img>
};
declare function gramm:figshare-logo() {
    <img src="{$g:img}logo-figshare.jpg" style="height: 20px; margin: 5px;"></img>
};
declare function gramm:roll-logo() {
    <img src="{$g:img}roll.png" style="height: 20px; margin: 5px;"></img>
};
declare function gramm:back-to-top($node as node(), $model as map(*)) {
    <a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button">
        <i class="fas fa-chevron-up"/>
    </a>
};
declare function gramm:back-to-top() {
    <a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button">
        <i class="fas fa-chevron-up"/>
    </a>
};

(:: EXTERNAL DATA ::)

(:~ returns <history> fragment from external HGV file
 : with place/date of origin included.
 : For doc with multiple HGV (such as TM 13), we use the first one.
 : 
 : @param doc - document node
 : @return XML <history> fragment
 :)
declare function gramm:doc-expand-history ($doc as node()){
    
    (: get HGV ID, select first one only :)
    let $hgv := gramm:hgv($doc) => gramm:first-item()
    
    (: link to external HGV document :)
    let $hist_link := $g:papinfohgv||$hgv||'/source'
    
    (: fragment of external HGV document :)
    let $fragment := doc($hist_link)//tei:history
    
    return
        $fragment
 };
 
(:~ returns <textClass> fragment from external HGV file
 : with HGV subjects.
 : For doc with multiple HGV (such as TM 13), we use the first one.
 : 
 : @param doc - document node
 : @return XML <textClass> fragment
 :)
declare function gramm:doc-expand-keywords ($doc as node()){
    
    (: get HGV ID, select first one only :)
    let $hgv := gramm:hgv($doc) => gramm:first-item()
    
    (: link to external HGV document :)
    let $hist_link := $g:papinfohgv||$hgv||'/source'
    
    (: fragment of external HGV document :)
    let $fragment := doc($hist_link)//tei:textClass
    
    return
        $fragment
 };
 
(:~ Get transcription file from papyri.info.
 : 
 : @param doc - document node
 : @return external XML document
 :)
declare function gramm:doc-expand-text ($doc as node()){
    
    (: get ddb-hybrid, select first one only :)
    let $ddb := gramm:ddb-hybrid($doc) => gramm:first-item()
    
    (: link to external transcription document :)
    let $text_link := $g:papinfoddb||$ddb||'/source'
    
    (: papyri.info transcription file :)
    let $externaldoc := if (doc-available($text_link)) 
        then doc($text_link)
        else <div>ERROR: text not available from papyri.info.<br/>{$text_link}</div>
    return
        $externaldoc
};

(:~ Get updated information for reprint-in papyri
 : 
 : @param doc - document node
 : @return updated doc, which is also stored in the database
 :)
declare function gramm:reprint-update($doc) {
    ()
    (: update titleStmt :)
    (: update div @type edition: add subtype 'lb-review-needed' for line numbers may be out of date :)
    ()
};


(:~ Get link to an external image.
 : 
 : @param doc - document node
 : @return link as string
 :)
declare function gramm:get-img-link ($doc as node()){
    
    (: get ID to external image link :)
    let $img_id := $doc//tei:graphic/@corresp/string() => substring-after('#')
    
    (: we assume only one image link, or else it will return the first :)
    let $ext_facs := doc($g:images)
    let $img_link := $ext_facs//id($img_id)/tei:ref[1]/@target/string()
    
    return
        $img_link
};

(:~ Get iiif manifest link.
 : 
 : @param doc - document node
 : @return link as string
 :)
declare function gramm:get-iiif-manifest ($doc as node()){

    (: get ID to external image link :)
    let $img_id := $doc//tei:graphic/@corresp/string() => substring-after('#')
    
    let $ext_facs := doc($g:images)
    let $iiif_link := $ext_facs//id($img_id)/tei:ref[@type eq 'iiif']
    
    return
        $iiif_link
};

(:~ Get iframe link.
 : 
 : @param doc - document node
 : @return link as string
 :)
declare function gramm:get-iframe ($doc as node()){

    (: get ID to external image link :)
    let $img_id := $doc//tei:graphic/@corresp/string() => substring-after('#')
    
    let $ext_facs := doc($g:images)
    let $iframe_link := $ext_facs//id($img_id)/tei:ref[@type eq 'iframe']
    
    return
        $iframe_link
};

declare function gramm:has-iiif($doc) {
    let $img_id := $doc//tei:graphic/@corresp/string() => substring-after('#')
    return
        doc($g:images)//id($img_id)/tei:ref[@type eq 'iiif']
};
declare function gramm:has-iframe($doc) {
    let $img_id := $doc//tei:graphic/@corresp/string() => substring-after('#')
    return
        doc($g:images)//id($img_id)/tei:ref[@type eq 'iframe']
};


(:: PLACE ::)

(:~ returns place of origin as string
 : 
 : @param history - <history> node
 : @return string
 :)
declare function gramm:origPlace($history as node()) {
    
    (: chose placeName of @subtype nome if available :)
    (: it can happen that there is more than 1 placeName @subtype nome (e.g. TM 7883) :)
    if ($history//tei:placeName[@subtype eq 'nome'][not(@cert eq 'low')]) 
    then $history//tei:placeName[@subtype eq 'nome'][not(@cert eq 'low')][1]/string()
    
    (: otherwise take the nome in parenthesis within origPlace :)
    else if (matches($history//tei:origPlace/string(), '\([a-zA-Z\s]+\??\)'))
    then 
        (: find substring starting at the point of the match :)
        substring($history//tei:origPlace/string(), functx:index-of-match-first($history//tei:origPlace/string(), '\([a-zA-Z\s]+\??\)')) 
        (: remove parentheses :)
        => substring-before(')') 
        => substring-after('(')
    
    (: if no nome can be reliably found, then return the content of origPlace :)
    else $history//tei:origPlace/string()
    => functx:if-empty('unknown')
    => replace('unbekannt', 'unknown')
 };
 
(:: SHAPE ::)

(:~ returns shape based on ratio
 : 
 : @param ratio - double number
 :)
declare function gramm:shape ($ratio as xs:double) {
    let $shape := 
        if ($ratio < 0.8) then "Vertical"
        else if ($ratio > 1.2) then "Horizontal"
        else "Squarish"
    return
        $shape 
 };

(:~ returns shape symbol based on ratio
 : 
 : @param shape - string from gramm:fibres direction
 :)
declare function gramm:shape-symbol($shape) {
    let $symbol :=
        if ($shape eq 'Vertical') then gramm:fa-shape-vertical()
        else if ($shape eq 'Horizontal') then gramm:fa-shape-horizontal()
        else gramm:fa-shape-square()
    return
        $symbol
};

(:: FIBRES ::)

(:~ returns fibres direction
 : 
 : @param f - string from @ana of <support>
 :)
declare function gramm:fibres($doc) {
    let $f := $doc//tei:support/@ana
    let $direction :=
        if (functx:substring-after-last($f, '#') eq 'fv') then 'Vertical' 
        else if (functx:substring-after-last($f, '#') eq 'fh') then 'Horizontal'
        else 'Mixed'
    return
        $direction
};
(:~ returns fibres symbol based on direction
 : 
 : @param fibres - string from gramm:fibres direction
 :)
declare function gramm:fibres-symbol($fibres) {
    let $symbol :=
        if ($fibres eq 'Vertical') then gramm:fa-fibre-vertical()
        else if ($fibres eq 'Horizontal') then gramm:fa-fibre-horizontal()
        else <span>{gramm:fa-fibre-vertical()} &amp; {gramm:fa-fibre-horizontal()}</span>
    return
        $symbol
};

(:: DATE functions ::)

(:~ filters a list of papyri for the ones dated within a date range.
 : The date range of a papyrus must overlap with the filter date range.
 : 
 : @param min - string for a year, terminus post quem
 : @param max - string for a year, terminus ante quem
 : @param papyri - set of nodes for papyri
 : @return xs:date
 :)
declare function gramm:date-filter($min, $max, $papyri) {
    (: increase the min to -320 for TM 5836 :)
    let $min-updated := if ($min eq '-300') then "-320" else $min
    let $tpq := xs:date(gramm:get-valid-year($min-updated, "tpq") || "-01-01")
    let $taq := xs:date(gramm:get-valid-year($max, "taq") || "-12-31")
    for $p in $papyri
    let $date := $p//tei:origDate[1]
    return 
        if (gramm:date-in-range($tpq, $taq, $date)) then $p else ()
    
};

(:~ returns a valid year, i.e. a year with four digits,
 : from terminus post/ante quem in the date filter on the papyri page.
 : 
 : @param num - string, 1 or 3-digit year
 : @return 4-digit year string
 :)
declare function gramm:get-valid-year($num, $limit) {
    (: 1-digit year 0 :)
    if ($num eq "0" and $limit eq "tpq") then
        "0001"
    else if ($num eq "0" and $limit eq "taq") then
        (: should be 0000, but not working :)
        "-0001"
    (: 3-digit year :)
    else if (matches($num, "-")) 
    then $num => replace("-", "-0") 
    else "0" || $num
};

(:~ checks if the origDate is within a date range.
 : 
 : @param min - xs:date, terminus post quem
 : @param max - xs:date, terminus ante quem
 : @param origDate - <origDate> node
 : @return returns boolean
 :)
declare function gramm:date-in-range($min, $max, $origDate) {
    if ($origDate/@when) then 
        $min <= gramm:string-to-date($origDate/@when/string()) and gramm:string-to-date($origDate/@when/string()) <= $max
    else if ($origDate[@notBefore] and $origDate[@notAfter]) then 
        let $taq := gramm:string-to-date($origDate/@notBefore/string())
        let $tpq := gramm:string-to-date($origDate/@notAfter/string())
        return
        ($min <= $taq and $tpq <= $max) or
        ($taq < $max and $tpq > $max) or
        ($min < $tpq and $taq < $min)
    else if ($origDate[@notBefore]) then 
        let $taq := gramm:string-to-date($origDate/@notBefore/string())
        let $tpq := 
            if(string(year-from-date($taq)) eq "-0050")
            then $taq + xs:yearMonthDuration("P49Y") (: avoid year 0 :)
            else $taq + xs:yearMonthDuration("P50Y")
        return
        ($min <= $taq and $tpq <= $max) or
        ($taq < $max and $tpq > $max) or
        ($min < $tpq and $taq < $min)
    else if ($origDate[@notAfter]) then
        let $tpq := gramm:string-to-date($origDate/@notAfter/string())
        let $taq := 
            if(string(year-from-date($tpq)) eq "0050")
            then $tpq - xs:yearMonthDuration("P49Y") (: avoid year 0 :)
            else $tpq - xs:yearMonthDuration("P50Y")
        return
        ($min <= $taq and $tpq <= $max) or
        ($taq < $max and $tpq > $max) or
        ($min < $tpq and $taq < $min)
    else ()
};

(:~ returns date. 
 : For string containing only a year, or a year and month, the date is completed with the first day (of January).
 : 
 : @param string - string from @when, @notAfter or @notBefore of <origDate>
 : @return xs:date
 :)
declare function gramm:string-to-date($string) {
    (: yyyy-mm-dd can be converted to a date :)
    if (matches($string, "\d{4}-\d{2}-\d{2}")) 
    then 
        xs:date($string)
    (: yyyy-mm :)
    else if (matches($string, "\d{4}-\d{2}")) 
    then xs:date(concat($string, "-01"))
    (: yyyy :)
    else xs:date(concat($string, "-01-01"))
};


(:: GENERAL SEARCH RESULTS ::)

(:~ returns search result from top-right search form
 :
 : @param hit - node from a hit (either XML or XHTML)
 : @param link - document-uri for the hit node
 :)
declare function gramm:result($hit, $document as xs:string) {
    (: get proper link to files depending on where the file is. Not ideal. :)
    let $link :=
        (: html pages :)
        if ($document => ends-with(".html")) 
        then replace($document, "/db/", "/exist/")
        (: XML files - intro, descr, class 
        (bibliography cannot be searched properly for now as it is generated dynamically :)
        else if ($document => ends-with("intro_overview.xml")) then "overview.html"
        else if ($document => ends-with("intro_concepts.xml")) then "concepts.html"
        else if ($document => matches($g:descr)) then "description.html?d=" || functx:substring-after-last($document, "/")
        else "classification.html?d=" || functx:substring-after-last($document, "/")
    (: page title (needs refactoring?) :)
    let $title :=
        if ($document => ends-with("papyri.html")) then "Papyri"
        else if ($document => ends-with("index.html")) then "Homepage"
        else if ($document => ends-with(".html")) then doc($document)//xhtml:h1/string()
        else doc($document)//tei:body/tei:head/string()
    return
        <div class="result border-bottom">
            <a href="{$link}" target="_blank">{$title}</a>
            {(: get the search term in context (300 characters before/after) :)
            let $config := <config xmlns="" width="300"/>
            for $match in $hit
                for $m in $match//exist:match
                return
                    kwic:get-summary($match, $m, $config)
            }

        </div>
};


(:: TYPOLOGY ::)

(:~ Get document or format category as a string.
 : 
 : @param catRef - <catRef> node
 : @return string
 :)
declare function gramm:get-catDesc($catRef as node()) {
    (: the first category if a list is the most important and the one we want to get:)
     let $cat := $catRef/@target => substring-after("#")
     let $cat1 := if(contains($cat, '#')) then substring-before($cat, " #") else $cat
     return
(:        collection($config:data-root)//id($scheme)//id($cat)/tei:catDesc:)
        doc($g:taxonomy)//id($cat1)/tei:catDesc
 };
 
(:~ Get shorter name of the type.
 : 
 : @param catRef - <catRef> node
 : @return string
 :)
declare function gramm:get-shortType($catRef) {
    let $log1 := console:log($catRef)
    let $type := if (matches($catRef, "#gt_")) then $catRef else $catRef/@target
    
    return
        switch ($type)
        case "Epistolary Exchange" 
        case "#gt_ee" 
        return "EE"
        case "Transmission of Information"
        case "#gt_ti"
        return "TI"
        case "Objective Statement"
        case "#gt_os" 
        return "OS"
        case "Recording of Information"
        case "#gt_ri"
        return "RI"
        default return ()
};
 
(:~ Get link to category description.
 : 
 : @param catRef - <catRef> node
 : @return string
 :)
declare function gramm:get-descr-link($catRef as node()) {
     let $scheme := $catRef/@scheme => substring-after("#")
     let $cat := $catRef/@target => substring-after("#")
     return
        collection($g:data)//id($scheme)//id($cat)/tei:catDesc/tei:ptr[@type eq "descr"]/@target/string() => replace(".xml", "")
 };
 
 (:~ Get link to category classification.
 : 
 : @param catRef - <catRef> node
 : @return string
 :)
declare function gramm:get-class-link($catRef as node()) {
     let $scheme := $catRef/@scheme => substring-after("#")
     let $cat := $catRef/@target => substring-after("#")
     return
        collection($g:data)//id($scheme)//id($cat)/tei:catDesc/tei:ptr[@type eq "class"]/@target/string() => replace(".xml", "")
 };
 
 (:~ returns title of typology/taxonomy pages
 : @param $pagetype - string
 : @param $resource - string
 : @return title string
 :)
declare function gramm:resource-title($pagetype, $resource) {
    let $doc := doc($g:data || $pagetype || "/" || $resource || ".xml")
    return
        $doc//tei:body/tei:head[1]/string()
};
 
 (:~ returns sidebar navigation for typology/taxonomy pages
 : 
 : @param pagetype - string
 : @param $doc_path - path to xml document. in some cases (descriptions empty sets) the path does not lead to an xml
 : @return html <div> sidebar container
 :)
 declare function gramm:sidebar-container($pagetype as xs:string, $doc_path as xs:string) {
     <div class="col-md-3 sidebar-container">
         {
            switch($pagetype)
            case "introduction" return gramm:sidebar-short($pagetype, $doc_path)
            case "descriptions"
            case "classification" return gramm:sidebar($pagetype, $doc_path)
            default 
            return gramm:sidebar($pagetype, $doc_path)
         }
     </div>
 };

(:~ returns sidebar navigation for typology/taxonomy pages
 : 
 : @param pagetype - string
 : @param $doc_path - path to xml document. in some cases (descriptions empty sets) the path does not lead to an xml
 : @return html <div> sidebar container with <nav> sidebar
 :)
declare function gramm:sidebar($pagetype as xs:string, $doc_path as xs:string) {
        <nav class="sidebar {$pagetype}" id="sidebarSupportedContent">
            <div class="sidebar-item title">
                <a class="sidebar-link" href="{$g:root}{$pagetype}">
                    {functx:capitalize-first($pagetype)}
                </a>
            </div>
            {   
                (: because of categories recursion, it is easier to create the sidebar through xslt :) 
                let $params :=
                    <parameters>
                        <param name="nav" value="{$pagetype}"/>
                        <param name="root" value="{$g:root}"/>
                    </parameters>
                let $sidebar := transform:transform(doc($g:taxonomy), doc($g:sidebar), $params)
                return
                $sidebar
            }
        </nav>
};

(:~ returns shorter version of sidebar navigation for intro/about pages
 : for the short sidebar we always have an xml document, we can convert the doc_path to doc
 : 
 : @return html <nav> sidebar
 :)
declare function gramm:sidebar-short($pagetype as xs:string, $doc_path as xs:string) {
    let $doc := doc($doc_path)
    return
        <nav class="sidebar">
            <div class="sidebar-item level0">
                <span class="sidebar-link">{$doc//tei:body/tei:head}</span>
            </div>
            {
                for $section in $doc//tei:div[@type eq "section"]
                return
                    <div class="sidebar-item level1">
                        <a class="sidebar-link" href="#{$section/tei:head/@xml:id}">{$section/tei:head/string()}</a>
                        {
                            for $subsection in $section//tei:div[@type eq "subsection"]
                            return
                                <div class="sidebar-item level2">
                                    <a class="sidebar-link" href="#{$subsection/tei:head/@xml:id}">{$subsection/tei:head/string()}</a>
                                </div>
                        }
                    </div>
            }
        </nav>
};

(:~ transforms XML TEI page into HTML for main text
 : 
 : the text is limited in width with bootstrap class, 
 : so that even on big screen the text is not made of long lines
 : 
 : @param $doc_path - path to xml document. in some cases (descriptions empty sets) the path does not lead to an xml
 : 
 : @return html <main> content
 :)
declare function gramm:main($doc_path) {
    <main class="col-md-7">
        {
            let $html := 
                if(doc-available($doc_path)) 
                then transform:transform(doc($doc_path), doc($g:typologyToHTML), <parameters><param name="id" value="{$doc_path}"/></parameters>)
                else transform:transform(doc($g:taxonomy), doc($g:typologyToTOC), <parameters><param name="id" value="{$doc_path}"/></parameters>)
            
            return
                $html
        }
    </main>
};

(:~ creates meta tags from a resource with DOI
 : 
 :  @param $pagetype - string type of the resource (intro, classification, descriptions...)
 : @param $resource - string name of the resource (calf, warrant, general, concepts... name of file without .xml extension)
 : 
 : @return html <meta> tags
 :)
declare function gramm:meta($pagetype as xs:string?, $resource as xs:string?) {
    (: there is the function x:path for the resource path (esp typology pages? :)
    (: compare with resources/xslt/typologyToHTML.xslt, how to cite section :)
    (: documentation of which meta tags available is patchy. What is the website title? (here using book title) :)
    let $path := "/db/apps/grammateus/data/"
    let $doc := doc($path||$pagetype||"/"||$resource||".xml")
    let $title := $doc//tei:titleStmt/tei:title
    let $doi := $doc//tei:publicationStmt/tei:idno[@type eq 'DOI']
    let $date := if($doi) then $doc//tei:change[text() eq 'Archived on Yareta'][1]/substring(@when, 1, 4) else $doc//tei:change[1]/substring(@when, 1, 4)

    let $author_meta := for $author in  $doc//tei:teiHeader//tei:author | $doc//tei:respStmt
            return 
                <meta name="citation_author" content="{$author//tei:surname}, {$author//tei:forename}"/>
    return 
        
        <span class='Z3988' title='url_ver=Z39.88-2004&amp;ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fzotero.org%3A2&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Adc&amp;rft.type=webpage&amp;rft.title=Webpage%20Description%20of%20Greek%20Documentary%20Papyri%3A%20Circular%20Letter&amp;rft.source=grammateus%20project&amp;rft.rights=Creative%20Commons%20Attribution-NonCommercial%204.0%20International&amp;rft.identifier=https%3A%2F%2Fgrammateus.unige.ch%2Fdescriptions%2Fcircular&amp;rft.aufirst=Lavinia&amp;rft.aulast=Ferretti&amp;rft.au=Lavinia%20Ferretti&amp;rft.au=Susan%20Fogarty&amp;rft.au=Elisa%20Nury&amp;rft.au=Paul%20Schubert&amp;rft.date=2023&amp;rft.language=en'></span>
};

