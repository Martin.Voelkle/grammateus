<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d45">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Warrant</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <author corresp="../authority-lists.xml#RLC">
                    <forename>Ruey-Lin</forename>
                    <surname>Chang</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#GB">
                        <forename>Gianluca</forename>
                        <surname>Bonagura</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName>
                    <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                    <ref target="https://data.snf.ch/grants/grant/212424">Project 212424</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:4bcjmpoy6zavbozlhn46ckrrci</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.
                   Previous version(s) archived on Yareta:
                   <list>
                       <item>
                           Ferretti, L., Fogarty, S., Nury, E., and Schubert, P. 2023. <title>Description of Greek Documentary Papyri: Warrant</title>. grammateus project. DOI: 
                           <idno type="DOI">10.26037/yareta:3qbd6bw2nnbp7pkodwpisk3whq</idno>
                       </item>
                   </list>
                </p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Ancient Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee #gt_ti"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_letterOfficial #gt_ti_warrant"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_ee_warrant"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2024-06-15">Archived on Yareta</change>
            <change who="../authority_lists.xml#RLC ../authority_lists.xml#EN" when="2023">Updated with Byzantine period</change>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2021-04-01">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2020-08-03">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Warrant</head>
            <p xml:id="d45_p10">
                In this article, warrants are placed in the category <q>transmission of information</q> [TI]. In the Ptolemaic period, they display some features that relate them also to <q>epistolary exchange</q> [EE]. In the Byzantine warrants, the final farewell formula, employed strictly as a tool of validation (see below), does not imply that those documents belong to the category [EE]. For an overview of the form of Byzantine letters, see <bibl>
                    <author>Fournet</author>
                    <ptr target="gramm:fournet2009"/>
                    <citedRange>esp. 37ff.</citedRange>
                </bibl>.
                </p>
            <p xml:id="d45_p1">A warrant is an order from an official to have an accused person
                brought before him. It usually follows from an earlier [<ref type="descr" target="#gt_petition">petition</ref>], complaint, or request from an individual
                to a higher authority. For an overview of the different descriptions of these
                documents in the modern period as <q>Orders to Arrest</q> and <q>Summonses</q>, see
                the references in <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2018a"/>
                    <citedRange>253, n.2</citedRange>
                </bibl>. For a list of Ptolemaic warrants see <bibl>
                    <author>P.Paramone</author>
                    <ptr target="gramm:coweyKramer2004"/>
                    <citedRange>no. 9, introduction : 104-106</citedRange>
                </bibl> and add [<ref type="internal" target="4126.xml">4126</ref> 222 BCE, Arsinoite nome]; for a list of Roman and Byzantine
                warrants see <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2018a"/>
                    <citedRange>253, and n.3; 258-262</citedRange>
                </bibl> and add [<ref type="internal" target="316224.xml">316224</ref> mid I-mid II CE, Ptolemais Hormou; <ref type="external" target="https://papyri.info/ddbdp/zpe;205;215">749346</ref> II CE, Bakchias; <ref type="external" target="https://papyri.info/ddbdp/zpe;217;172_1">942810</ref> II CE, Arsinoite nome(?); <ref type="external" target="https://papyri.info/ddbdp/zpe;217;173_2">942811</ref> early III CE, Herakleia (Arsinoite nome); <ref type="external" target="https://papyri.info/ddbdp/zpe;217;175_3">942812</ref> early IV CE, Adaiou (Oxyrhynchite nome)]; see also <bibl>
                    <author>Micucci</author>
                    <ptr target="gramm:micucci2021"/>
                    <citedRange>171</citedRange>
                </bibl>. 
                </p>
            <p xml:id="d45_p11">Roman warrants were usually issued by the <term xml:lang="lat">strategos</term>
                to the <term xml:lang="lat">archephodos</term>, head-policeman of a village, who was tasked with
                locating and ensuring the accused was produced. For a discussion on the changes to
                policing after III CE see <listBibl>
                    <bibl>
                    <author>Gagos and Sijpesteijn</author>
                    <ptr target="gramm:gagosSijpesteijn1996"/>
                    <citedRange>79-80</citedRange>
                </bibl>
                <bibl>
                    <author>Fischer-Bovet and Sänger</author>
                    <ptr target="gramm:fischerBovetSanger2019"/>
                    <citedRange>175-178</citedRange>
                </bibl>
                </listBibl>. By the mid-III CE, the office of archephodos starts receding <bibl>
                    <author>P.Oxy. 74</author>
                    <ptr target="gramm:leith2009"/>
                    <citedRange>no. 5000, 4-5n.</citedRange>
                </bibl> and the recipients of Byzantine warrants are <term xml:lang="lat">komarchai</term>, <gloss>village supervisors</gloss> (III-IV) and <term xml:lang="lat">eirenarchai</term> <gloss>peace supervisors</gloss> (IV-VII), to cite only the better attested officials. As for the senders, they display a variety of official capacities.</p>
            <p xml:id="d45_p2">Warrants are found throughout the Ptolemaic, Roman and
                Byzantine periods, and their format evolved along this timeline. Unsurprisingly most preserved examples are from the Arsinoite and
                Oxyrhynchite nomes, with increased occurrences of Hermopolite provenance from the IV CE onwards. In the Ptolemaic period they are found in two formats: either as an ordinary letter, or as an apostil, i.e. <quote>a short set of instructions added to the bottom of a
                    petition</quote>
                <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2018a"/>
                    <citedRange>266</citedRange>
                </bibl>. In the Roman period these apostils became separate and distinct documents in that they
                were no longer attached to the original petition, and had various methods of
                validation. By the mid-III CE, influence from the [<ref target="#gt_businessNote" type="descr">Business Note</ref>] led to changes in the structure of
                the document, and by the IV CE warrants were again showing elements of the epistolary
                type; for a detailed description of the evolution of the warrant see <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2018a"/>
                </bibl>. That these warrants were mass-produced is evidenced by [<ref target="https://papyri.info/ddbdp/p.harr;2;196" type="external">26510</ref> II-III CE, Oxyrhynchus] and
                    [<ref type="external" target="https://papyri.info/ddbdp/sb;20;15095">34196</ref> IV CE, Hermopolite nome]
                each with two different warrants written on the same sheet. For the later Byzantine period, a clue as to mass-production may be found on the upper right edge of [<ref type="internal" target="38524.xml">38524</ref> VI-VII CE, Hermopolis], where the remains of a hasta from a document (e.g. another warrant) written above is still visible after the individual documents were cut off from the roll (this palaeographical detail is mentioned in <bibl>
                    <author>Sijpesteijn</author>
                    <ptr target="gramm:sijpesteijn1988"/>
                    <citedRange>73</citedRange>
                </bibl>).</p>
            <div xml:id="d45_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d45_p3">Ptolemaic warrants in the form of a letter referred to themselves
                    as <term xml:lang="grc">ἐπιστολή</term> [<ref type="internal" target="8207.xml">8207</ref> 248 BCE, Oxyrhynchus] or <term xml:lang="grc">γράμματα</term>
                        [<ref type="internal" target="8212.xml">8212</ref> 245 BCE, Oxyrhynchus];
                    there was no specific term for this type of document, see <bibl>
                        <author>P.Paramone</author>
                        <ptr target="gramm:coweyKramer2004"/>
                        <citedRange>no. 9, introduction : 106</citedRange>
                    </bibl>. These warrants began with the standard opening address <seg corresp="../authority_lists.xml#introduction" type="syntax" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" xml:id="d45_s1" ana="../authority_lists.xml#ptolemaic">[from <hi rend="italic">name</hi>
                        &lt;nom.&gt;][to <hi rend="italic">name</hi> &lt;dat.&gt;][<term xml:lang="grc">χαίρειν</term>]</seg> e.g. [<ref type="internal" target="4126.xml" corresp="#d45_s1">4126</ref>; <ref target="https://www.papyri.info/ddbdp/p.mil.congr.xiv;;pg5" type="external" corresp="#d45_s1">14424</ref> 6 CE, Arsinoite nome]. There often followed a
                    brief description of the reason for the order [<ref type="external" target="https://www.papyri.info/ddbdp/p.athen;;8">5021</ref> l.2-8, 193-192
                    BCE, Arsinoite nome] or simply the names of the complainant and the accused
                        [<ref type="internal" target="4126.xml">4126</ref> l.2-9]. The main verb was
                    usually an imperative <seg corresp="../authority_lists.xml#main-text" type="syntax" xml:id="d45_s2" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic">[<term xml:lang="grc">ἀπόστειλον</term>]</seg>
                    <gloss corresp="#d45_s2">dispatch</gloss> [<ref type="internal" target="4126.xml" corresp="#d45_s2">4126</ref> l.11-12], <seg corresp="../authority_lists.xml#main-text" type="syntax" xml:id="d45_s3" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic">[<term xml:lang="grc">ἀποκατάστησον</term>]</seg>
                    <gloss corresp="#d45_s3">deliver/hand over</gloss> [<ref type="internal" target="8212.xml" corresp="#d45_s3">8212</ref> l. 13-14], <seg corresp="../authority_lists.xml#main-text" type="syntax" xml:id="d45_s4" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic">[<term xml:lang="grc">ἀνάπεμψον</term>]</seg>
                    <gloss corresp="#d45_s4">send up</gloss> [<ref type="internal" target="8207.xml">8207</ref> l.1, 248 BCE, Oxyrhynchite nome], or simply <seg corresp="../authority_lists.xml#main-text" type="syntax" xml:id="d45_s5" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic">[<term xml:lang="grc">πέμψας</term>]</seg>
                    <gloss corresp="#d45_s5">send</gloss> [<ref type="external" target="https://www.papyri.info/ddbdp/p.hib;1;127" corresp="#d45_s5">8255</ref> l.3, 250 BCE, Oxyrhynchite nome]. There followed a simple
                    closing farewell <seg corresp="../authority_lists.xml#subscription" type="syntax" xml:id="d45_s6" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic">[<term xml:lang="grc">ἔρρωσο</term>]</seg> and, as with all official letters, a <seg corresp="../authority_lists.xml#date" type="syntax" xml:id="d45_s7" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic ">[date@end]</seg> of the document.
                    This type of warrant is last attested in 6 BCE [<ref corresp="#45_s6 #d45_s7" target="https://www.papyri.info/ddbdp/p.mil.congr.xiv;;pg5" type="external">14424</ref>].</p>
                <p xml:id="d45_p4">Warrants are also found as a short note added to the end of the
                    petition to which it pertains, i.e. as an apostil. Some of these apostils are
                    written in the form of a letter with the usual address <seg corresp="../authority_lists.xml#introduction" type="syntax" xml:id="d45_s8" datcat="#gt_ee #gt_letterOfficial #gt_ee_warrant" ana="../authority_lists.xml#ptolemaic">[from <hi rend="italic">name</hi>
                        &lt;nom.&gt;][to <hi rend="italic">name</hi> &lt;dat.&gt;][<term xml:lang="grc">χαίρειν</term>]</seg> and followed by a simple order to
                    have the person produced before the authority, e.g. [<ref type="external" target="https://www.papyri.info/ddbdp/p.tebt;3.2;961" corresp="#d45_s8">5475</ref> l.13-14, 150/139 BCE, Tebtunis] <term xml:lang="grc">σύ[νταξον]
                        [κατ]αστῆσα[ι]</term>
                    <gloss>order (him) to be delivered</gloss>, and no closing farewell, but with
                    the date.</p>
                <p xml:id="d45_p5">Other apostils are even shorter with a simple <seg corresp="../authority_lists.xml#introduction" type="syntax" xml:id="d45_s9" datcat="#gt_ti #gt_ti_warrant" ana="../authority_lists.xml#ptolemaic">[to
                            <hi rend="italic">name</hi> &lt;dat.&gt;]</seg>, followed by a brief
                    order, no farewell, and with the <seg corresp="../authority_lists.xml#date" type="syntax" xml:id="d45_s14" datcat="#gt_ti #gt_ti_warrant" ana="../authority_lists.xml#ptolemaic">[date@end]</seg> [<ref corresp="#d45_s9 #d45_s14" target="https://papyri.info/ddbdp/sb;24;16295" type="external">8810</ref> 199 BCE, Arsinoite nome]. Such apostils continue
                    to be written into the early Roman period e.g. [<ref type="internal" target="697550.xml">697550</ref> l.16-19, 13 CE, Arsinoite nome; <ref type="external" target="https://www.papyri.info/ddbdp/sb;20;15182" corresp="#d45_s9 #d45_t1">14956</ref> l.21-22, 28-30 CE, Euhemeria] <term xml:id="d45_t1" xml:lang="grc" corresp="#d45_s9">ἀρχε̣(φόδῳ)·
                        ἔκπ[ε]μ̣[ψον]</term>
                    <gloss corresp="#d45_t1">To the archephodos. Send them up</gloss>.</p>
                <p xml:id="d45_p6">In the Roman period these succinct apostils were eventually
                    separated from the petition and written as individual warrants. As in the short
                    Ptolemaic apostil, there was no mention of the official who issued the warrant
                    (but see below) – this remained the case until the mid III CE <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018a"/>
                        <citedRange>258-265</citedRange>
                    </bibl>; compare [<ref type="internal" target="26701.xml">26701</ref> II-III CE,
                    Oxyrhynchite nome] and [<ref type="internal" target="16434.xml">16434</ref> 256
                    CE, Oxyrhynchus]. After this time influence from the [<ref type="descr" target="#gt_businessNote">Business Note</ref>] led to a change in the
                    opening address with the official in primary position <seg corresp="../authority_lists.xml#introduction" type="syntax" xml:id="d45_s10" datcat="#gt_ti #gt_ti_warrant" ana="../authority_lists.xml#roman">[from
                            <term xml:lang="grc">παρά</term>
                        <hi rend="italic">official</hi> &lt;gen.&gt;] [<hi rend="italic">official</hi> &lt;dat.&gt;]</seg> [<ref type="internal" corresp="#d45_s10" target="31339.xml">31339</ref> l.1-2, III-IV CE,
                    Oxyrhynchus]. Occasionally the recipient is moved to a closing footer <seg corresp="../authority_lists.xml#subscription" type="syntax" xml:id="d45_s11" datcat="#gt_ti #gt_ti_warrant" ana="../authority_lists.xml#roman">[<hi rend="italic">name</hi> &lt;dat.&gt;]</seg> as is the case in the [<ref target="#gt_businessNote" type="descr">Business Note</ref>]; compare [<ref target="https://www.papyri.info/ddbdp/p.flor;2;118" type="external" corresp="#45_s11">10971</ref> l.9, 260 CE, Theadelphia] and [<ref type="internal" corresp="#d45_s11" target="128316.xml">128316</ref> l.6,
                    III-IV CE, Oxyrhynchite nome] <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018a"/>
                        <citedRange>269-273</citedRange>
                    </bibl>. The main sentence conveys a sense of urgency: <seg type="syntax" datcat="#gt_ti #gt_ti_warrant" corresp="../authority_lists.xml#main-text" xml:id="d45_s12" ana="../authority_lists.xml#roman">[<term xml:lang="grc">ἐξαυτῆς ἀναπέμψατε</term>
                        <hi rend="italic">name</hi> &lt;acc.&gt; <term xml:lang="grc">ἢ ὑμεῖς αὐτοὶ
                            ἀνέλθατε</term>]</seg>
                    <gloss corresp="#d45_s12">send up [the accused] immediately, or else come
                        yourself</gloss> [<ref type="internal" target="16434.xml" corresp="#d45_s12">16434</ref> l.3-5]. Sometimes a date is added [<ref type="internal" target="29824.xml">29824</ref> l.4, II CE, Oxyrhynchus]. For only a few
                    decades from the mid-III to early IV CE, when <term xml:lang="grc">σεσημείωμαι</term>-validation (see below Layout) came into use, the bearer
                    (viz. a soldier) of a warrant was frequently mentioned intended to escort the
                    summoned person <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018a"/>
                        <citedRange>263-264</citedRange>
                    </bibl>.</p>
                <p xml:id="d45_p12">In the Byzantine period, the Roman practice of giving the header
                    position in warrants to the issuing official <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d45_s13" datcat="#gt_ti #gt_ti_warrant" ana="../authority_lists.xml#byzantine">[from
                            <term xml:lang="grc">παρά</term> official &lt;gen.&gt;]</seg> is carried
                    on into VII CE, e.g. [<ref type="external" target="https://papyri.info/ddbdp/bgu;19;2774" corresp="#d45_s13">91684</ref> late V / early VI CE, Hermopolis; <ref type="internal" corresp="#d45_s13" target="38314.xml">38314</ref> VI-VII CE, Hermopolis;
                        <ref type="internal" corresp="#d45_s13" target="38524.xml">38524</ref>],
                    with an exception [<ref type="external" target="https://papyri.info/ddbdp/p.mich;10;591">37321</ref> VI CE,
                    Herakleopolis] where the recipients [official &lt;dat.&gt;] are presented in the
                    header, while the issuer is not indicated. A selection of main verbs used in
                    Byzantine warrants can be found in <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018a"/>
                        <citedRange>264, n.37</citedRange>
                    </bibl>. </p>
            </div>
            <div xml:id="d45_div2_format" n="2" type="section">
                <head xml:id="format">Format</head>
                <p xml:id="d45_p7">Ptolemaic warrants written as letters vary in format from <term xml:lang="lat">transversa charta</term> [<ref type="internal" target="1939.xml">1939</ref> 254 BCE, Arsinoite nome; <ref target="8207.xml" type="internal">8207</ref>], to <term xml:lang="lat">pagina</term> with horizontal fibres [<ref type="internal" target="4126.xml">4126</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.hib;1;59">8209</ref> 245 BCE, Oxyrhynchite nome]. Although for many types of documents in the Roman period the favoured format was <term xml:lang="lat">pagina</term>, warrants from this period retained the horizontal appearance of the apostils at the end of petitions: this means that the scribe cut vertical sheets from the roll, but instead of writing along the fibres he preferred to turn the sheet so that it was horizontally oriented and wrote across the vertical fibres, see [<ref type="internal" target="26779.xml">26779</ref> II CE, Oxyrhynchite nome; <ref type="internal" target="26920.xml">26920</ref> III-IV CE, Philadelphia; <ref type="internal" target="31276.xml">31276</ref> III CE, Ptolemais Euergetis], <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018a"/>
                        <citedRange>266-269</citedRange>
                    </bibl> and <bibl>
                        <author>Ferretti et al.</author>
                        <ptr target="gramm:ferretti2020"/>
                        <citedRange>201-202</citedRange>
                    </bibl>. This Roman way of drafting warrants was carried on into the Byzantine period, so much so that Byzantine warrants often assume the characteristic form of a narrow strip e.g. [<ref type="external" target="https://papyri.info/ddbdp/bgu;19;2774">91684</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.mich;10;591">37321</ref>; <ref type="internal" target="38314.xml">38314</ref>; <ref type="internal" target="38524.xml">38524</ref>]</p>
            </div>
            <div xml:id="d45_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d45_p8">Ptolemaic warrants written in letter format are usually presented as a
                    single block of text with the final farewell and date in <term xml:lang="lat">eisthesis</term> at the end of the document [<ref type="internal" target="1939.xml">1939</ref>; <ref type="internal" target="4126.xml">4126</ref>; <ref type="internal" target="5856.xml">5856</ref> 223-222 BCE, Apollonopolite nome]. Warrants
                    from the Roman period are also written as a block of text, occasionally with the
                    first [<ref type="internal" target="26920.xml">26920</ref>] or the final line
                        [<ref type="internal" target="31339.xml">31339</ref>] in <term xml:lang="lat">eisthesis</term>. There is often a very large space at the
                    end of the document [<ref type="internal" target="28128.xml">28128</ref> II-III CE, Ptolemais Euergetis; <ref type="internal" target="29824.xml">29824</ref>].</p>
                <p xml:id="d45_p9">While in I-II CE warrants there is no reference in the written
                    text to the issuing official, the order is often validated by the presence of a
                    seal inscribed <term xml:lang="grc">ὁ στρατηγός σε καλεῖ</term>, <gloss>the
                            <term xml:lang="lat">strategos</term> orders you</gloss> [<ref type="internal" target="25693.xml">25693</ref> I CE, Ptolemais Euergetis; <ref target="https://www.papyri.info/ddbdp/sb;18;13172" type="external">18272</ref> 88-96 CE, Arsinoite nome]. From the II-III CE the seal, and therefore any mention of the
                    issuing official, disappears, and there is often a series of crosses under the
                    text [<ref type="internal" target="26779.xml">26779</ref>; <ref type="internal" target="31660.xml">31660</ref> III CE, Oxyrhynchus] (and note the more unusual filler in [<ref type="internal" target="31417.xml">31417</ref> III CE, Karanis, <ref type="internal" target="30319.xml">30319</ref> III CE, Arsinoite nome]). This served the dual purpose of ensuring
                    against unauthorised additions, and acting as a form of validation, see <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018a"/>
                        <citedRange>262-265</citedRange>
                    </bibl>. A hybrid example of both seal and filler can be found in [<ref type="internal" target="30430.xml">30430</ref> III CE, Oxyrhynchite nome]. With the changes in the
                    structure of the warrant in the mid-III CE, where the identity of the official
                    is now explicitly mentioned in the opening address, there is often a signature
                    to validate the document <term xml:lang="grc">σεσημείωμαι</term> (diversely abbreviated)
                    <gloss>I have signed</gloss> [<ref type="internal" target="31339.xml">31339</ref> l.6], also found in the [<ref type="descr" target="#gt_businessNote">Business Note</ref>] of the same period, e.g. [<ref type="internal" target="22671.xml">22671</ref> 206/235 CE].</p>
                <p xml:id="d45_p13">In the Byzantine period from the early IV CE to VII CE, the validation takes the form of final farewell as used in Roman letters: <term xml:lang="grc">ἔρρωσο</term>, <term xml:lang="grc">ἔρρωσθε</term> <gloss>be in good health</gloss> [<ref type="internal" target="22179.xml">22179</ref> 346-350 CE, Oxyrhynchus; <ref type="external" target="https://papyri.info/ddbdp/bgu;19;2774">91684</ref> late V / early VI CE, Hermopolis], or <term xml:lang="grc">ἐρρῶσθαι ὑμᾶς εὔχομαι</term> <gloss>I pray for your health</gloss> [<ref type="external" target="https://papyri.info/ddbdp/p.poethke;;5">128330</ref> V(?) CE, Hermopolis]. This form of validation is as a rule written by a hand — presumably the sender’s — other than the one that wrote the main text e.g. [<ref type="internal" target="22179.xml">22179</ref> l.4; <ref type="internal" target="38524.xml">38524</ref> l.3]. In the last cited papyrus, <term xml:lang="grc">ἔρρωσθαι</term> (read <term xml:lang="grc">ἔρρωσθε</term>) † is apparently followed by a paraph of the sender. Elsewhere, it has previously not been pointed out that <term xml:lang="grc">ἐρρῶσθαι ὑμᾶς ε̣ὔ̣(χομαι)</term> [<ref target="https://papyri.info/ddbdp/p.amh;2;146" type="external">35497</ref> V CE, Hermopolis(?), ll.5-6] is actually written by a second hand. In the end, it ought to be stressed that the warrants present a rigorous evolution of formats over time, with the exception of [<ref type="internal" target="30430.xml">30430</ref>], and that as validation, <term xml:lang="grc">σεσημείωμαι</term> and <term xml:lang="grc">ἐρρῶσθαι ὑμᾶς εὔχομαι</term> are never employed simultaneously in the same warrant <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2018a"/>
                    <citedRange>264-265</citedRange>
                </bibl>.</p>
            </div>
        </body>
    </text>
</TEI>