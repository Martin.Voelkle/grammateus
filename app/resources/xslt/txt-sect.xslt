<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:map="http://www.w3.org/2005/xpath-functions/map" exclude-result-prefixes="#all" version="3.0">
    
  <xsl:param name="sections" select="//tei:ab[@type eq 'section']"/>
  
  <xsl:variable name="classes-per-line" as="map(xs:integer, xs:string*)" select="map:merge(for $section in $sections/tei:locus, $line in $section/@from to $section/@to return map { $line : substring-after($section/@ana/string(), '#') }, map { 'duplicates' : 'combine' })"/>

  <xsl:output method="html" indent="yes" html-version="5"/>
  
  <xsl:template match="/">
        <!-- copy contents -->
        <xsl:apply-templates mode="copy"/>
    </xsl:template>
    
    <!-- makes copy of everything, but allows to modify output with templates -->
    <xsl:template match="@*|node()" mode="copy">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="copy"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- finds all elements (<ab>) that contain lines -->
    <xsl:template match="*[(./a/@id) or (./br/@id)]" mode="copy">
        <!-- copy the element itself -->
        <xsl:copy>
            <!-- copy attributes but not descendants -->
            <xsl:apply-templates select="@*" mode="copy"/>
        
            <!-- wrap each line content in a <span> -->
            <!-- this is assuming that all <br> and <a> (and only them) indicate a newline. -->
            <xsl:for-each-group select="node()" group-starting-with="br|a">
                <!-- remove the first empty group -->
                <xsl:choose>
                    <xsl:when test="@id">
                        <!-- copy the a/br element -->
                        <xsl:copy-of select="."/>
                        <!-- extract line number -->
                        <!-- assuming the number always come after letter l, and there is only one letter l -->
                        <xsl:variable name="lb" select="number(substring-after(./@id, 'l'))"/>
                        <!-- add class only to section lines -->
                        <xsl:choose>
                            <!-- if the line has a text section -->
                            <xsl:when test="map:contains($classes-per-line, $lb)">
                                <span class="section {$classes-per-line($lb)}">
                                    <xsl:apply-templates select="tail(current-group())" mode="copy"/>
                                </span>
                            </xsl:when>
                            <!-- otherwise, e.g. verso lines, or documents where not all text is highlighted (TM 19654), lines with seals etc. -->
                            <xsl:otherwise>
                                <span class="no-section">
                                    <xsl:apply-templates select="tail(current-group())" mode="copy"/>
                                </span>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- empty group not displayed -->
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
  
</xsl:stylesheet>