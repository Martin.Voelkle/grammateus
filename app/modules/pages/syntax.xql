xquery version "3.1";

module namespace syntax="http://grammateus.ch/pages/syntax";


import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "../gramm.xql";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare function local:color($section) {
    switch ($section)
    case ("abstract") return "--papyrus-abstract-color"
    case ("addressee") return "--papyrus-addressee-color"
    case ("main-text") return "--papyrus-main-color"
    case ("heading") return "--papyrus-heading-color"
    case ("salutation") return "--papyrus-salutation-color"
    case ("introduction") return "--papyrus-intro-color"
    case ("subscription") return "--papyrus-subscription-color"
    case ("date") return "--papyrus-date-color"
    case ("col-no") return "--papyrus-col-color"
    case ("official-mark") return "--papyrus-mark-color"
    case ("descr-contents") return "--papyrus-descr-cont-color"
    case ("trans-clause") return "--papyrus-clause-color"
    case ("signature") return "--papyrus-signature-color"
    case ("addition") return "--papyrus-addition-color"
    default return ()
};


(:~ Find <q> translation corresponding to a syntax <seg> @xml:id in a description document
 : 
 : @param id - the segment's @xml:id
 : @param doc - the document node 
 : @return sequence of <q> elements
 : 
 :)
declare function syntax:get-q($id, $doc) {
    
let $q_list :=
    for $q in $doc//tei:gloss
    let $corresp := tokenize($q/@corresp/translate(string(), '#', ''), " ")
    where $corresp = $id
    return $q

return $q_list
};

(:~ Find TM numbers corresponding to a syntax <seg> @xml:id in a description document
 : 
 : @param id - the segment's @xml:id
 : @param doc - the document node 
 : @return sequence of <ref> elements
 : 
 :)
declare function syntax:get-tm($id, $doc) {
    
let $tm :=
    for $ref in $doc//tei:ref
    let $corresp := tokenize($ref/@corresp/translate(string(), '#', ''), " ")
    where $corresp = $id
    return $ref

return $tm
};

(:~ Find examples  corresponding to a syntax <seg> @xml:id in a description document
 : with greek term, translation and TM
 : 
 : @param id - the segment's @xml:id
 : @param doc - the document node 
 : @return sequence of ??? elements
 : 
 :)
declare function syntax:get-example($id, $doc) {
    
let $tm :=
    for $term in $doc//tei:term
    let $corresp := tokenize($term/@corresp/translate(string(), '#', ''), " ")
    let $term_id := $term/@xml:id/string()
    where $corresp = $id
    return 
        <example>
            <term>{$term}</term>
            <q>{syntax:get-q($term_id, $doc)}</q>
            <ref>{syntax:get-tm($term_id, $doc)}</ref>
        </example>

return $tm
};

(:~ create table to apply datatable.
 : The width is set to 100% also for the tables in the compare page
 : 
 : @param id - an identifier for the table
 : @param documents - XML TEI descriptions to search for <seg> elements
 : @param categories - xml:id of categories to restrict the table (because some descriptions have formulas for more than one category)
 : 
 : @return HTML <table> within a <div>
 : 
 :)
declare function syntax:table($id, $documents, $category) {
<div class="syntax-table">

    <table id="{$id}" class="display" style="width:100%;">
        <thead>
            <tr>
                <th>Formula</th>
                <th>Text Section</th>
                <th>Translation</th>
                <th>TM</th>
                <th>Type</th>
                <th>Description</th>
                <th>Examples</th>
                <th>ID</th>
            </tr>
        </thead>
        <tbody>
            {
                for $doc in $documents
                (: find <seg> @type syntax, and @datcat of $categories or no @datcat:)
                for $formula in $doc//tei:seg[@type eq 'syntax' and (not($category) or matches(@datcat, $category) or not(@datcat))]
                    let $text := $formula/normalize-space(string())
                    let $id := if ($formula/@xml:id) then $formula/@xml:id/string() else ""
                    let $section := $formula//@corresp[1]/substring-after(string(), "#")
                    let $example := syntax:get-example($id, $doc)
                    let $translation := syntax:get-q($id, $doc)
                    let $tm := syntax:get-tm($id, $doc)
                order by number(replace($id[1], "[sd_]", ""))
                group by $doc, $section, $text
                return
                    <tr>
                        <td style="border-left: 8px solid var({local:color($section[1])})">{transform:transform($formula[1], doc($g:seg), ())}</td>
                        <td data-order="{gramm:get-text-section-nb($section[1])}">
                        {gramm:get-text-section-name($section[1])}</td>
                        
                        <td>{
                            for $t in $translation
                            return transform:transform($t, doc($g:seg), ())
                        }</td>
                        <td>{
                            for $p in functx:distinct-nodes($tm)
                            let $value := $p/string()
                            group by $value
                            (: the last .comma is hidden with css :)
                            return (transform:transform(<root>{$p[1]}</root>, doc($g:ref), ()), <span class="comma">, </span>)
                        }</td>
                        <td>{if ($formula[1]/@datcat) then 
                            let $type := functx:get-matches($formula[1]/@datcat, "#gt_(ee|ti|os|ri)")
                            return gramm:get-shortType($type[1])
                            else gramm:get-shortType($doc//tei:catRef[@n eq 'type'])
                        }</td>
                        <td>{
                            let $descr := base-uri($doc[1]) 
                                => functx:substring-after-last("/")
                                => substring-before(".xml")
                            return
                                <a href="descriptions/{$descr}#{$id[1]}">
                                    {$doc//tei:body/tei:head[1]}
                                </a>
                            
                        }</td>
                        <td>
                            <ul>{for $e in $example
                            let $list := count($example) > 1
                            return
                                <div class="{if ($list) then 'syntax-example-list' else ()}">
                                    <span>{transform:transform($e/term, doc($g:seg), ())}</span>,
                                    {if ($e/q/tei:gloss) then
                                        (<span>{transform:transform($e/q, doc($g:seg), ())}</span>, ", ")
                                        else ()
                                    }
                                    <span>{transform:transform($e/ref[1], doc($g:ref), ())}</span>
                                </div>
                            }</ul>
                        </td>
                        <td data-order="{replace($id[1], "[sd_]", "")}">{$id[1]}</td>
                    </tr>
            }
        </tbody>
    </table>
</div>
};

declare function syntax:page() {
    <div id="content">
        <h1 class="h1">Syntax</h1>
        <h3 class="h3">List of formulas from the descriptions</h3>
        <p>The textual structure of documentary papyri is ruled by a syntax of structural elements (see Key Concepts <a href="{$g:root}introduction/concepts#structure">§55 and following</a>).</p>
        <p>The formulas mentioned in the descriptions are available here for exploration, because they had some significance when establishing the classification of documents. This is <b>not</b> a complete list of all the the existing formulas found in the papyri. The content of the table relies on the text of the descriptions.</p>
        <p>The table is ordered first by description and then by text section. The description link brings you directly to the formula in the text, as  the description is ultimately the reference to understand the formula in context.</p>
        {syntax:table("formulas", collection($g:descr), ())}
    </div>
};

