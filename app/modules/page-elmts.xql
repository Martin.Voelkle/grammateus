xquery version "3.1";

module namespace page="http://grammateus.ch/page-elmts";

import module namespace g="http://grammateus.ch/global" at "global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "gramm.xql";

declare function page:title($pagetype as xs:string?, $resource as xs:string?)
{
	switch ($pagetype)
	case "home"
	case "newhome" return "Home"
	case "about" return "About"
	case "introduction" return $resource || " | Introduction"
	case "typology" return "Typology"
	case "classification" return 
	    if (matches($resource, "classification"))
	    then "Classification"
	    else upper-case($resource) || " | Classification" 
	case "descriptions"
	case "description" return 
	    if (matches($resource, "description")) 
	    then "Descriptions"
	    else if (matches($resource, 'gt_'))
	    then $resource || " | Description"
	    else gramm:resource-title($pagetype, $resource) || " | Description"
	case "papyri" return "Papyri"
	case "doc" return "TM " || $resource
	case "browse" return "Fulltext Search"
	case "biblio" return "Bibliography"
	case "syntax" return "Syntax Elements"
	case "compare" return "Compare"
	case "error" return "Error"
	case "howto-cite" return "How To Cite"
	case "team" return "Team"
	case "rights" return "Rights"
	case "tutorials" return "Tutorials"
	default return ()
};

(: for css/js : see if we can limit mirador to resources that actually have a iiif link? :)

declare function page:css($pagetype as xs:string?, $resource as xs:string?) {
    let $css_all_pages := 
        (
            <link rel="stylesheet" href="{$g:css}bootstrap-4.3.1.min.css"/>,
            <link rel="stylesheet" href="{$g:css}style.css"/>,
            <link rel="stylesheet" href="{$g:css}print.css"/>
        )
    let $css_dataTables :=
        (
            <link rel="stylesheet" href="{$g:css}jquery.dataTables.min.css"/>,
            <link rel="stylesheet" href="{$g:css}responsive.dataTables.min.css"/>
        )
    return
    if ($pagetype = 'doc') then
        (
            <link rel="stylesheet" href="{$g:css}iiif-viewer.css"/>,
            <link rel="stylesheet" href="{$g:css}svg.css"/>,
            $css_all_pages
            
        )
    else if ($pagetype = 'papyri') then
        (
            <link rel="stylesheet" href="{$g:css}nouislider.css"/>,
            $css_all_pages
        )
    else if ($pagetype = 'syntax') then
        (
            $css_dataTables,
            $css_all_pages
        )
    else if ($pagetype = 'compare') then
        (
            $css_dataTables,
            $css_all_pages
        )
    else
        $css_all_pages
};

declare function page:scripts($pagetype as xs:string?, $resource as xs:string?) {
    let $js_all_pages := 
        (
            <script src="{$g:js}jquery-3.4.1.min.js"/>,
            <script src="{$g:js}popper.min.js"/>,
            <script src="{$g:js}bootstrap-4.3.1.min.js"/>,
            <script src="{$g:js}grammateus.js"/>
        )
    let $js_dataTables := 
        (
            <script src="{$g:js}jquery.dataTables.min.js"/>,
            <script src="{$g:js}dataTables.responsive.min.js"/>
        )
    return
    if ($pagetype = 'doc') then
        (
            $js_all_pages,
            <script src="{$g:js}mirador.min.js"></script>,
            <script src="{$g:js}mirador-3-config.js"/>,
            <script src="{$g:js}svg-pan-zoom.min.js"/>,
            <script src="{$g:js}mirador-3-config-navigation.js"/>
        )
    else if ($pagetype = 'papyri') then
        (
            $js_all_pages,
            <script src="{$g:js}papyri-facets-form.js"/>,
            <script src="{$g:js}wNumb.min.js"/>,
            <script src="{$g:js}nouislider.js"/>,
            <script src="{$g:js}papyri-facets-dateSlider.js"/>
        )
    else if ($pagetype = 'syntax') then
        (
            $js_all_pages,
            $js_dataTables,
            <script src="{$g:js}syntax.js"/>
            
        )
    else if ($pagetype = 'compare') then
        (
            <script src="{$g:js}popper.min.js"/>,
            $js_all_pages,
            $js_dataTables,
            <script src="{$g:js}compare.js"/>,
            <script src="{$g:js}tooltips.js"/>
            
        )
    else
        $js_all_pages
};

