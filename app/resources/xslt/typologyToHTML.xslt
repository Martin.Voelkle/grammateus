<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:exist="http://exist-db.org/xquery/kwic" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:request="http://exist-db.org/xquery/request" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0">
    
    <xsl:strip-space elements="tei:bibl tei:listBibl tei:gloss"/>
    
    <!-- ********** -->
    <!-- Parameters -->
    <!-- ********** -->
    
    <xsl:param name="id"/>
    
    <!-- ********** -->
    <!-- Variables  -->
    <!-- ********** -->
    
    <xsl:variable name="descr">description</xsl:variable> 
    <xsl:variable name="class">classification</xsl:variable>
    <xsl:variable name="intro">introduction</xsl:variable>
    <xsl:variable name="taxonomy">../../data/grammateus_taxonomy.xml</xsl:variable>
    <xsl:variable name="authority">../../data/authority_lists.xml</xsl:variable>
    <xsl:variable name="root">https://grammateus.unige.ch/</xsl:variable>
    
    <xsl:variable name="year">
        <xsl:choose>
            <xsl:when test="//tei:publicationStmt/tei:idno[@type eq 'DOI']">
                <xsl:value-of select="//tei:change[text() eq 'Archived on Yareta'][1]/substring(@when, 1, 4)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="//tei:change[1]/substring(@when, 1, 4)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="title" select="//tei:titleStmt/tei:title"/>
    <xsl:variable name="coins-title">
        <xsl:value-of select="replace($title, ' ', '%20')"/>
    </xsl:variable>
    
    <xsl:variable name="url">
        <!-- should there not be the document/base-uri function available?
            e.g. select="document-uri(root(//tei:TEI))" -->
        <xsl:value-of select="$root"/>
        <xsl:value-of select="replace($id, '/db/apps/grammateus/data/', '') =&gt; replace('.xml', '') "/>
    </xsl:variable>
    
    <xsl:variable name="coins-author-list">
        <xsl:call-template name="coins-authors"/>
    </xsl:variable>
    
    <xsl:variable name="hasDOI" select="(//tei:publicationStmt/tei:idno[@type eq 'DOI'] or //tei:sourceDesc//tei:idno[@type eq 'DOI'])"/>
    <xsl:variable name="doi" select="//tei:publicationStmt/tei:idno[@type eq 'DOI']"/>
    <xsl:variable name="coins-doi">
        <xsl:if test="$doi">
            <xsl:text>rft_id=info%3Adoi%2Fhttp%3A%2F%2Fdoi.org%2F</xsl:text>
            <xsl:value-of select="$doi"/>
            <xsl:text>&amp;</xsl:text>
        </xsl:if>
    </xsl:variable>
    <xsl:variable name="hasPastDOI" select="//tei:sourceDesc//tei:idno[@type eq 'DOI']"/>
    
    <!-- ********** -->
    <!-- Functions  -->
    <!-- ********** -->
    
    <!-- Extract the proper link in a <category> element from the <taxonomy>.
        Function use in for ref template, @type "descr" or "class"-->
    <xsl:function name="tei:link">
        <xsl:param name="type"/><!-- either "descr" or "class" -->
        <xsl:param name="category"/><!-- the xml:id of a <category> -->
        <xsl:value-of select="document($taxonomy)//tei:category[@xml:id eq $category]/tei:catDesc/tei:ptr[@type eq $type]/replace(@target, '.xml', '')"/>
    </xsl:function>
    
    <!-- ********** -->
    <!-- Templates  -->
    <!-- ********** -->
    
    <xsl:template match="/">
        <div class="text">
        
            <!-- title/subtitle -->
            <h1 class="h1">
                <xsl:apply-templates select="//tei:body/tei:head[not(@type)]" mode="h1"/>
                <xsl:if test="//tei:body/tei:head[@type = 'subtitle']">
                    <!-- text-secondary is a bootstrap color class -->
                    <p class="subtitle text-secondary">
                        <xsl:apply-templates select="//tei:body/tei:head[@type = 'subtitle']" mode="h2"/>
                    </p>
                </xsl:if>
            </h1>
        
            <!-- contents -->
            <!-- only for descriptions -->
            <xsl:if test="//tei:body/tei:div[@type eq 'section'] and not(//tei:body/tei:head[1] eq 'Key concepts' or //tei:body/tei:head[1] eq 'General Introduction')">
                <h3 class="h3">Contents</h3>
                <ol>
                    <!-- code also used in typologyToTOC -->
                    <xsl:for-each select="//tei:body/tei:div[@type eq 'section']">
                        <li>
                            <a href="#{./tei:head/@xml:id}">
                                <!-- tokenize the text to include only the first part of the heading and no greek text in parenthesis-->
                                <xsl:choose>
                                    <xsl:when test="./tei:head[1]/tei:term">
                                        <xsl:value-of select="./tei:head[1]/tokenize(string(), ' \(')[1]"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./tei:head[1]/string()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </a>
                            <xsl:if test="./tei:div[@type eq 'subsection']">
                                <ul>
                                    <xsl:for-each select="./tei:div[@type eq 'subsection']">
                                        <li>
                                            <a href="#{./tei:head[1]/@xml:id}">
                                                <!-- tokenize the text to include only the first part of the heading and no greek text in parenthesis-->
                                <xsl:choose>
                                    <xsl:when test="./tei:head[1]/tei:term">
                                        <xsl:value-of select="./tei:head[1]/tokenize(string(), ' \(')[1]"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./tei:head[1]/string()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </xsl:if>
                        </li>
                    </xsl:for-each>
                </ol>
            </xsl:if>

            <!-- main text -->
            <xsl:apply-templates/>
            
            <!-- bibliography -->
            <xsl:if test="//tei:bibl">
                <h3 class="h3">Bibliography</h3>
                <ul>
                    <xsl:for-each-group select="//tei:bibl/tei:ptr" group-by="@target">
                        <!-- Author name / TM Arch/Per / edition title -->
                        <xsl:sort select="replace(preceding-sibling::tei:author, '\d+\D*', '')" collation="http://www.w3.org/2013/collation/UCA?lang=de;strength=primary"/>
                        <!-- TM Archive/Person number -->
                        <xsl:sort select="replace(preceding-sibling::tei:author, 'TM (Arch|Per) ', '')" data-type="number"/>
                        <!-- edition volume or number (not including the BGU XXX dddd [exkursus...]) of Poll Tax -->
                        <xsl:sort select="replace(preceding-sibling::tei:author, '\D+', '')" data-type="number"/>
                        <!-- date -->
                        <xsl:sort select="replace(replace(./@target, '\w+:[a-zA-Z]+', ''), '[a-z]', '')" data-type="number"/>
                        <!-- item letter, e.g. 2022c -->
                        <xsl:sort select="replace(./@target, '\w+:[a-zA-Z]+[0-9]+', '')"/>
                        <li>
                            <xsl:apply-templates select="."/>
                            <xsl:text>.</xsl:text>
                        </li>
                    </xsl:for-each-group>
                </ul>
            </xsl:if>
            
            <!-- how to cite (not ideal, maybe separate from the transformation?) -->
            <xsl:if test="$hasDOI">
            <h3 class="h3">How to Cite</h3>
            <div class="pb-4">
                <span>
                    <!-- same select for the coins authors-->
                    <xsl:for-each select="//tei:respStmt | //tei:teiHeader//tei:author">
                        <xsl:sort select=".//tei:surname"/>
                        <xsl:if test="position() = last()">
                            <xsl:text>and </xsl:text>
                        </xsl:if>
                        <xsl:value-of select=".//tei:surname"/>
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="replace(.//tei:forename, '[a-z]+', '')"/>
                            <xsl:choose>
                                <xsl:when test="position() != last()">
                                    <xsl:text>., </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>. </xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                    </xsl:for-each>
                </span>
                <xsl:value-of select="$year"/>
                <xsl:text>. </xsl:text>
                <span class="title">
                    <xsl:value-of select="$title"/>
                    <xsl:text>. </xsl:text>
                </span>
                <span> grammateus project. </span>
                <!-- choose => if no DOI, add url+accessed date-->
                <xsl:choose>
                    <xsl:when test="//tei:publicationStmt/tei:idno[@type eq 'DOI']">
                        <a href="{$url}">
                                <xsl:value-of select="$url"/>
                            </a>. (Accessed: <xsl:value-of select="format-date(current-date(), '[MNn] [D], [Y0001]')"/>).
                        <span>
                            <xsl:text>DOI: </xsl:text>
                            <a href="https://doi.org/{$doi}">
                                <xsl:value-of select="$doi"/>
                            </a>
                        </span>
                    </xsl:when>
                    <xsl:otherwise>
                        <a href="{$url}">
                                <xsl:value-of select="$url"/>
                            </a>. (Accessed: <xsl:value-of select="format-date(current-date(), '[MNn] [D], [Y0001]')"/>).
                    </xsl:otherwise>
                </xsl:choose>
            </div>
            <xsl:if test="$hasPastDOI">
                <div class="pb-4">
                    Previous version(s) of the document:
                    <ul>
                        <xsl:for-each select="//tei:sourceDesc//tei:item">
                            <li>
                                <xsl:value-of select="text()[1]"/>
                                <i>
                                        <xsl:value-of select="tei:title"/>
                                    </i>
                                <xsl:value-of select="text()[2]"/>
                                <a href="https://doi.org/{tei:idno[@type eq 'DOI']}">
                                    <xsl:value-of select="tei:idno[@type eq 'DOI']"/>
                                </a>
                            </li>
                        </xsl:for-each>
                    </ul>
                </div>
            </xsl:if>
            <!-- CoinS for zotero plugin type=webpage? type=dataset? -->
            <span class="Z3988" title="url_ver=Z39.88-2004&amp;ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fzotero.org%3A2&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Adc&amp;rft.type=webpage&amp;{$coins-doi}rft.title={$coins-title}&amp;rft.source=grammateus%20project&amp;rft.identifier={$url}&amp;{$coins-author-list}rft.date={$year}&amp;rft.language=en"/>
            </xsl:if>
        </div>
    </xsl:template>
    
    
    <!-- general TEXT templates -->

    <xsl:template match="tei:head[not(@type)]" mode="h1">
       <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:head[@type = 'subtitle']" mode="h2">
       <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:div[@type = 'section']">
        <h3 class="h3">
            <a class="anchor" id="{tei:head/@xml:id}"/>
            <xsl:value-of select="tei:head[not(@type)]"/>
            <xsl:if test="./tei:head[@type = 'subtitle']">
                <!-- text-secondary is a bootstrap color class -->
                <p class="subtitle text-secondary">
                    <xsl:apply-templates select="./tei:head[@type = 'subtitle']" mode="h2"/>
                </p>
            </xsl:if>
        </h3>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:div[@type = 'subsection']">
        <h5 class="h5">
            <a class="anchor" id="{tei:head/@xml:id}"/>
            <xsl:value-of select="tei:head"/>
        </h5>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:p">
        <div class="paratext">
            <span class="paranumber">
                <xsl:value-of select="count(preceding::tei:p[ancestor::tei:body])+1"/>
            </span>
            <p>
                <xsl:if test="@xml:id">
                    <a class="anchor" id="{@xml:id}"/>
                </xsl:if>
                <xsl:apply-templates/>
            </p>
        </div>
    </xsl:template>
    
    <xsl:template match="tei:list[@type = 'ordered']">
        <ol>
            <xsl:attribute name="type">
                <xsl:value-of select="tei:item[1]/@n"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </ol>
    </xsl:template>
    
    <xsl:template match="tei:list[@type = 'unordered']">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    
    <xsl:template match="tei:title[@rend = 'italic']">
        <cite>
            <xsl:apply-templates/>
        </cite>
    </xsl:template>
    
    <xsl:template match="tei:abbr[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    
    <xsl:template match="tei:hi[@rend = 'bold']">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    
    <xsl:template match="tei:hi[@rend = 'superscript']">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
    
    <xsl:template match="tei:note[@rend = 'alert']">
        <div class="alert alert-primary" role="alert">
            <i class="fas fa-exclamation-circle"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <!-- Quotation Marks -->
    <!--    q = double quote, general
            q @type foreign = highlight of foreign term like giro, single quote
            quote = actual quotation, double quote
            gloss = translation of actual Greek present in document, single quote
    -->
    
    <xsl:template match="tei:quote">
        <xsl:text>“</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>”</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:q[not(parent::tei:q)]">
        <xsl:text>“</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>”</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:q[parent::tei:q or parent::tei:quote]">
        <xsl:text>‘</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>’</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:q[@type eq 'foreign']">
        <xsl:text>‘</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>’</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:q[parent::tei:gloss]">
        <xsl:text>“</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>”</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:gloss">
        <xsl:text>‘</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>’</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:term[@xml:lang = 'grc']">
        <span class="grc">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:term[@xml:lang = 'lat']">
        <span class="lat" style="font-style:italic;">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    
    <!-- POINTERS and LINKS -->
    
    <!-- pointer for cross-references (list items) -->
    <xsl:template match="tei:ptr[@type = 'list']">
        <xsl:variable name="target" select="@target"/>
        <xsl:value-of select="ancestor::tei:div//tei:item[@xml:id = substring-after($target, '#')]/@n"/>)
    </xsl:template>
    
    <!-- links to description page -->
    <xsl:template match="tei:ref[@type = 'descr']">
        <a href="{$root}{$descr}/{tei:link(@type, substring-after(@target, '#'))}{tei:ptr/@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- links to classification page -->
    <xsl:template match="tei:ref[@type = 'class']">
        <a href="{$root}{$class}/{tei:link(@type, substring-after(@target, '#'))}{tei:ptr/@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- links to introduction pages -->
    <xsl:template match="tei:ref[@type = 'intro']">
        <a href="{$root}{$intro}/{substring-before(@target, '.xml')}{tei:ptr/@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- anchor for links in same document -->
    <xsl:template match="tei:ref[@type = 'anchor']">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- links to papyri not in our corpus / in papyri.info -->
    <xsl:template match="tei:ref[@type = 'external']">
        <a class="out-link" href="{@target}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- links to papyri available in our corpus -->
    <xsl:template match="tei:ref[@type = 'internal']">
        <a class="db-link" href="{$root}document/{substring-before(@target, '.xml')}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- website links -->
    <xsl:template match="tei:ref[@type eq 'gramm']">
        <a href="{$root}{@target}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- other links -->
    <xsl:template match="tei:ref[not(@type) or @type eq 'other']">
        <a href="{@target}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    
    <!-- SYNTAX segments templates -->
    <xsl:template match="tei:seg[@type eq 'syntax']">
        <xsl:variable name="section-id" select="substring-after(@corresp, '#')"/>    
        <xsl:variable name="section-name" select="doc($authority)//id($section-id)/tei:label/string()"/>
        <xsl:variable name="section-n" select="doc($authority)//id($section-id)/@n/string()"/>
        <a class="anchor" id="{@xml:id}"/>
        <a href="{$root}papyri.html?facet:s{$section-n}={$section-name}&amp;" class="formula {substring-after(@corresp, '#')}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    
    <!-- BIBLIOGRAPHY templates -->
    
    <!-- Inline citations: links to our own bibliography page -->
    <xsl:template match="tei:bibl">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>]</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:listBibl">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>]</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:bibl[parent::tei:listBibl]">
        <xsl:apply-templates/>
        <xsl:if test="position() != last()">
            <xsl:text>; </xsl:text>  
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="tei:ptr[parent::tei:bibl]">
        <!-- path to bibliographic file from ptr @target -->
        <xsl:variable name="path">
            <xsl:text>../../data/bibl/</xsl:text>
            <xsl:value-of select="substring-after(@target, 'gramm:')"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <!-- get bibliographic file  -->
        <xsl:variable name="bibl-doc" select="document($path)"/>
        <!-- author nb -->
        <xsl:variable name="author" select="count($bibl-doc//tei:text//tei:author)"/>
        <!-- editor nb -->
        <xsl:variable name="editor" select="count($bibl-doc//tei:text//tei:editor)"/>
        <!-- special short title instead of author (for Papyri editions like Paramone) -->
        <xsl:variable name="short-title" select="$bibl-doc//tei:title[@type eq 'short' and @level eq 'm']"/>
        <!-- link to our bibliography -->
        <a href="{$root}bibliography#{substring-after(@target, 'gramm:')}" target="_blank">
            <!-- display author depending on author/editor nb -->
            <xsl:choose>
                <xsl:when test="$short-title">
                    <xsl:value-of select="replace($short-title, '\.$', '')"/>
                </xsl:when>
                <xsl:when test="$author = 1">
                    <xsl:value-of select="$bibl-doc//tei:author/tei:surname/string()"/>
                </xsl:when>
                <xsl:when test="$author = 2">
                    <xsl:value-of select="$bibl-doc//tei:author[1]/tei:surname/string()"/>
                    <xsl:text> and </xsl:text>
                    <xsl:value-of select="$bibl-doc//tei:author[2]/tei:surname/string()"/>
                </xsl:when>
                <xsl:when test="$author gt 2">
                    <xsl:value-of select="$bibl-doc//tei:author[1]/tei:surname/string()"/>
                    <xsl:text> </xsl:text>
                    <i>et al.</i>
                </xsl:when>
                <!-- no author -->
                <xsl:when test="$editor = 1">
                    <xsl:value-of select="$bibl-doc//tei:editor/tei:surname/string()"/>
                </xsl:when>
                <xsl:when test="$editor = 2">
                    <xsl:value-of select="$bibl-doc//tei:editor[1]/tei:surname/string()"/>
                    <xsl:text> and </xsl:text>
                    <xsl:value-of select="$bibl-doc//tei:editor[2]/tei:surname/string()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$bibl-doc//tei:editor[1]/tei:surname/string()"/>
                    <xsl:text> </xsl:text>
                    <i>et al.</i>
                </xsl:otherwise>
            </xsl:choose>
        </a>
        <!-- date -->
        <xsl:variable name="date" select="$bibl-doc//tei:imprint/tei:date/substring(text(), 1, 4)"/>
        <xsl:choose>
            <xsl:when test="$short-title"/>
            <xsl:otherwise>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$date"/>
                <xsl:value-of select="substring-after(@target, $date)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:citedRange">
        <xsl:text> : </xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- CoinS templates -->
    <xsl:template name="coins-authors">
        <xsl:for-each select="//tei:respStmt | //tei:teiHeader//tei:author">
            <xsl:sort select=".//tei:surname"/>
            <xsl:if test="position() = 1">
                <xsl:text>rft.aufirst=</xsl:text>
                <xsl:value-of select=".//tei:forename"/>
                <xsl:text>&amp;rft.aulast=</xsl:text>
                <xsl:value-of select=".//tei:surname"/>
                <xsl:text>&amp;</xsl:text>
            </xsl:if>
            <xsl:text>rft.au=</xsl:text>
            <xsl:value-of select=".//tei:forename"/>
            <xsl:text>%20</xsl:text>
            <xsl:value-of select=".//tei:surname"/>
            <xsl:text>&amp;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    
    <!-- TABLES templates -->
    
    <xsl:template match="tei:table">
        <table>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    
    <xsl:template match="tei:row">
        <tr>
            <xsl:apply-templates/>
        </tr>
    </xsl:template>
    
    <xsl:template match="tei:row[@role eq 'label']">
        <thead>
            <tr>
                <xsl:apply-templates/>
            </tr>
        </thead>
    </xsl:template>
    
    <xsl:template match="tei:cell[parent::tei:row[@role eq 'label']]">
        <th>
            <xsl:apply-templates/>
        </th>
    </xsl:template>
    
    <xsl:template match="tei:cell[@rows]">
        <td rowspan="{@rows}">
            <xsl:apply-templates/>
        </td>
    </xsl:template>
    
    <xsl:template match="tei:cell">
        <td>
            <xsl:apply-templates/>
        </td>
    </xsl:template>
    
    <!-- FIGURES templates -->
    
    <!-- bootstrap figures -->
    <xsl:template match="tei:figure">
        <figure class="figure">
            <img src="../resources/img/{tei:graphic/@url}" alt="{tei:figDesc}" class="figure-img img-fluid"/>
            <figcaption class="figure-caption">
                Fig. <xsl:value-of select="@n"/>: <xsl:value-of select="tei:head"/>
                <a type="button" data-toggle="modal" data-target="#{@xml:id}"> Click to enlarge</a>
            </figcaption>
        </figure>
        
        <div id="{@xml:id}" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <xsl:value-of select="tei:head"/>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="fa fa-xmark"/>
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="../resources/img/{tei:graphic/@url}" alt="{tei:figDesc}" class="figure-img img-fluid"/>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- Other -->
    
    <!-- templates for the expanded full text search results? Not working. -->
    <xsl:template match="exist:match">
        <exist:match>
            <xsl:apply-templates/>
        </exist:match>
    </xsl:template>
    
    
    <!-- ********************** -->
    <!-- elements not processed -->
    <xsl:template match="tei:teiHeader"/>
    <xsl:template match="tei:head"/>
    <xsl:template match="tei:catDesc"/>
    <xsl:template match="tei:author"/>
    <xsl:template match="tei:figDesc"/>
    <xsl:template match="tei:head[parent::tei:figure]"/>
    <xsl:template match="tei:title[parent::tei:bibl]"/>
    
</xsl:stylesheet>