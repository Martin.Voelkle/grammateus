xquery version "3.1";

import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare option exist:serialize "method=html media-type=text/html";

let $tmlist := "2089
708
834
1048
1280
818
1027
1945
905
945
3215
5842
5856
7423
1062
1079
1122
2092
2149
2205
2155
1761
951
1020
1096
1132
1145
2090
2091
2095
2150
12879
12880
12881
12981
14139
14141
31312
22671
31761
30768
128900
78846
30431
31224
47159
31655
26876"



return


    <html>
        <ol>
            {for $tm in functx:lines($tmlist)
            let $hgv := doc(concat("https://papyri.info/hgv/", $tm, "/source"))
                
                return
                <li style="list-style:none;">
                    {string-join($hgv//tei:term, ",")}
                    
                </li>
            }
        </ol>
    </html>