<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d12">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Double Document</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:v23otqafrnbwbh3zq4t4lwaypa</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_syngr"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_doubleDoc"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-03-16">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2021-11-01">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Double document</head>
            <div xml:id="d12_div1_introduction" type="section" n="1">
            <head xml:id="introduction">Introduction</head>
            <p xml:id="d12_p1">A [<ref type="descr" target="#gt_syngr">syngraphe</ref>] (<term xml:lang="grc">συγγραφή</term>) may take the form of a double document, ie. the
                text of the contract or receipt is written out twice on the same sheet of papyrus.
                For an overview of all types of double documents, see <bibl>
                    <author>Wolff</author>
                    <ptr target="gramm:wolff1978a"/>
                    <citedRange>57-80</citedRange>
                </bibl>.</p>
            <div xml:id="d12_div2_six_wit" type="subsection" n="1">
                <head xml:id="six_wit">Six-witness double document</head>
                <p xml:id="d12_p2">The earliest form of double document, appearing in Greek from
                    the late IV BCE, is the six-witness double document (<term xml:lang="grc">συγγραφὴ ἑξαμάρτυρος</term>), a privately drawn up contract with the
                    upper/inner text (<term xml:lang="lat">scriptura interior</term>) rolled up and
                    sealed, and the lower/outer text (<term xml:lang="lat">scriptura
                    exterior</term>) open and available for consultation [<ref type="internal" target="5836.xml">5836</ref> 311-310 BCE; <ref type="internal" target="5837.xml">5837</ref> 285-284 BCE (with seals), both Elephantine].
                    The six witnesses were named at the end of each of the two scripts, one of which
                    was nominated the <term xml:lang="grc">συγγραφοφύλαξ</term> (keeper of the <term xml:lang="lat">syngraphe</term>) and entrusted with the finished document.
                    On probable demotic precedents for this double document consult <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978a"/>
                        <citedRange>59-63</citedRange>
                    </bibl>. These documents record loan contracts [<ref type="internal" target="3097.xml">3097</ref> 110 BCE; <ref type="internal" target="3099.xml">3099</ref> 109 BCE, both Hakoris], marriage agreements [<ref type="internal" target="5836.xml">5836</ref>; <ref type="internal" target="5838.xml">5838</ref> 284-283 BCE, Elephantine], and testaments [<ref type="internal" target="5837.xml">5837</ref>].</p>
                <p xml:id="d12_p3">Although it became possible to publicly register private
                    contracts by the end of the III BCE <bibl>
                        <author>Boswinkel and Pestman</author>
                        <ptr target="gramm:boswinkelPestman1982"/>
                        <citedRange>177</citedRange>
                    </bibl>, the II BCE saw an increase in the number of officials (<term xml:lang="lat">agoranomoi</term>) with the authority to register documents
                    in a <term xml:lang="lat">grapheion</term>; see <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978a"/>
                        <citedRange>9-18</citedRange>
                    </bibl> on the office of the <term xml:lang="lat">agoranomos</term>. This
                    registration led to a gradual degeneration of the six-witness double document,
                    with the inner script eventually shrinking to a one-line summary, compare [<ref type="internal" target="5837.xml">5837</ref>; <ref type="internal" target="2674.xml">2674</ref> 215-214 BCE, Oxyrhynchite nome; <ref type="internal" target="44733.xml">44733</ref> 145 BCE, Herakleopolis; <ref target="https://papyri.info/ddbdp/p.bad;2;2" type="external">8140</ref> 130
                    BCE, Hermonthis; <ref type="internal" target="3099.xml">3099</ref>]. While the
                    requirement for six witnesses and a <term xml:lang="grc">συγγραφοφύλαξ</term>
                    was now superfluous, there were documents which still listed these alongside the
                    note of registration, e.g. [<ref type="internal" target="3097.xml">3097</ref>;
                        <ref type="internal" target="869382.xml">869382</ref> 99 BCE, Herakleopolite
                    nome]; see <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978a"/>
                    </bibl>. Eventually, by the end of the I BCE, even the single line inner script
                    had disappeared, and all that remained of the six-witness double document was the
                    word <term xml:lang="grc">μάρτυρες</term> (witnesses), a remnant of a now
                    obsolete requirement [<ref type="external" target="https://papyri.info/ddbdp/p.berl.moeller;;4">10235</ref> l.27,
                    3 CE, Philadelphia]. </p>
                <p xml:id="d12_p4">The Roman period six-witness document, a <term xml:lang="grc">δάνειον ἑξαμάρτυρον</term>, bears little resemblance to the Ptolemaic
                    six-witness double document. On theories surrounding the origin of this type of
                    document see <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978a"/>
                        <citedRange>72-73, esp. n.85</citedRange>
                    </bibl>.</p>
            </div>
            <div xml:id="d12_div3_agoranomic" type="subsection" n="2">
                <head xml:id="agoranomic">Agoranomic deed</head>
                <p xml:id="d12_p5">Double documents, without six-witnesses or a <term xml:lang="grc">συγγραφοφύλαξ</term>, were drawn up and registered in the
                        <term xml:lang="lat">grapheion</term>. They also have an inner and outer
                    script, are rolled and sealed, and mostly concern house and land sales, e.g.
                        [<ref type="internal" target="56.xml">56</ref>; <ref type="internal" target="91.xml">91</ref>, both 113 BCE, Pathyris]. These documents are often
                    referred to as agoranomic deeds; see <bibl>
                        <author>Pestman</author>
                        <ptr target="gramm:pestman1985"/>
                    </bibl> for such documents from Krokodilopolis and Pathyris.</p>
                <p xml:id="d12_p6">On six-witness double documents, see <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978a"/>
                        <citedRange>57-71</citedRange>
                    </bibl> and <bibl>
                        <author>Pestman</author>
                        <ptr target="gramm:pestman1980"/>
                        <citedRange>9-13</citedRange>
                    </bibl>. On the evolution from private document to agoranomic deed, see <bibl>
                        <author>Boswinkel and Pestman</author>
                        <ptr target="gramm:boswinkelPestman1982"/>
                        <citedRange>176-193</citedRange>
                    </bibl> and <bibl>
                        <author>Yiftach-Firanko</author>
                        <ptr target="gramm:yiftach2008b"/>
                    </bibl>.</p>
            </div>
            <div xml:id="d12_div4_symbolon" type="subsection" n="3">
                <head xml:id="symbolon">Symbolon</head>
                <p xml:id="d12_p7">Another type of double document also found in the Ptolemaic
                    period is the <term xml:lang="lat">symbolon</term>, a contract or receipt
                    written out twice on a single sheet (<term xml:lang="lat">scriptura interior /
                        exterior</term>), and sealed. The inner script is more usually an abstract
                    of the contents of the outer script. Many of the examples extant belong to the
                        <ref type="other" target="https://www.trismegistos.org/arch/detail.php?arch_id=256">Zenon
                        archive</ref>; most of them are receipts for grain [<ref type="internal" target="762.xml">762</ref> 257 BCE, Philadelphia], tax [<ref type="internal" target="2063.xml">2063</ref> 249-248 BCE, Philadelphia (pig tax)], money
                        [<ref type="internal" target="1834.xml">1834</ref> 257 BCE, Philadelphia],
                    or wages [<ref type="internal" target="1837.xml">1837</ref> 257 BCE,
                    Philadelphia] - the last two examples have a demotic copy after the two Greek
                    scripts. A contract for a loan of wheat was explicitly drawn up as a <term xml:lang="lat">symbolon</term>: <term xml:lang="grc">ἀποδώσω κατὰ τὸ σύμβολον τ[οῦτο...</term>
                    <gloss>I will give according to this <term xml:lang="lat">symbolon</term>
                        </gloss> [<ref target="https://www.papyri.info/ddbdp/p.sorb;1;17dupl" type="external">3132</ref> Frag.B, l.19, 22, 257 BCE, Oxyrhynchite nome]. See <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978a"/>
                        <citedRange>75-77</citedRange>
                    </bibl>.</p>
                <p xml:id="d12_p8">Other <term xml:lang="lat">symbolon</term> receipts were issued
                    by <term xml:lang="lat">naukleroi</term> (ship-owners) to various <term xml:lang="lat">sitologoi</term> (grain collectors) upon receipt of grain
                    from the granaries given to them for shipment, e.g. [<ref type="internal" target="3066.xml">3066</ref> 197 BCE, Arsinoite nome]. See <bibl>
                        <author>Pfeiffer</author>
                        <ptr target="gramm:pfeiffer2003"/>
                        <citedRange>25-26</citedRange>
                    </bibl> for a full bibliography on these documents.</p>
            </div>
                </div>
            <div xml:id="d12_div5_structure" type="section" n="2">
                <head xml:id="structure">Structure</head>
                <p xml:id="d12_p9">The Ptolemaic six-witness double document begins with the <seg xml:id="d12_s1" type="syntax" corresp="../authority-lists.xml#date">[date@start]</seg>, usually the long regnal format for both inner and
                    outer scripts, e.g. [<ref type="internal" target="2946.xml" corresp="#d12_s1">2946</ref> 171 BCE, Arsinoite nome; <ref type="internal" target="44733.xml" corresp="#d12_s1">44733</ref>], or a short date for the inner script and a
                    full regnal date for the outer script, e.g. [<ref type="internal" target="3741.xml" corresp="#d12_s1">3741</ref> 103 BCE, Arsinoite nome], or
                    a short date at the beginning of both parts, e.g. [<ref type="internal" target="764.xml" corresp="#d12_s1">764</ref> 257 BCE; <ref type="internal" target="767.xml" corresp="#d12_s1">767</ref> 256 BCE, both Arsinoite
                    nome].</p>
                <p xml:id="d12_p10">The main text begins directly with the verb describing the
                    action in an objective statement, e.g. <seg xml:id="d12_s2" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ἐδάνεισεν</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                        &lt;dat.&gt;]</seg>, <gloss corresp="#d12_s2">
                        <hi rend="italic">N</hi> has
                        loaned to <hi rend="italic">N</hi>
                    </gloss>, [<ref type="internal" target="2674.xml" corresp="#d12_s2">2674</ref>; <ref type="internal" target="2650.xml" corresp="#d12_s2">2650</ref> 173 BCE, Arsinoite nome], or
                        <seg xml:id="d12_s3" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ἐμίσθωσεν</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                        &lt;dat.&gt;]</seg>, <gloss corresp="#d12_s3">
                        <hi rend="italic">N</hi> has
                        leased to <hi rend="italic">N</hi>
                    </gloss>, [<ref type="internal" target="2946.xml" corresp="#d12_s3">2946</ref> 171 BCE, Tebtunis]. At the
                    end of the main text there is a validation statement: <term xml:lang="grc">ἡ
                        συγγραφὴ ἥδε κυρία ἔστω</term> [<ref type="internal" target="5836.xml">5836</ref> l.13-14; <ref type="internal" target="869382.xml">869382</ref>
                    l.21-22]. A list of witnesses follows, introduced by <seg xml:id="d12_s4" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">μάρτυρες</term>]</seg> e.g. [<ref type="internal" target="44733.xml" corresp="#d12_s4">44733</ref> l.42; <ref type="internal" target="869382.xml" corresp="#d12_s4">869382</ref> l.22], after which the
                        <seg xml:id="d12_s5" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">συγγραφοφύλαξ</term>]</seg> is named [<ref type="internal" target="2650.xml" corresp="#d12_s5">2650</ref> l.18]. A document settling a
                    dispute is drawn up with a <seg xml:id="d12_s6" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ὁμολογεῖ</term> + infinitive]</seg> construction, in this case as a
                    mutual <term xml:lang="lat">homologia</term>: <term xml:lang="grc" corresp="#d12_s6" xml:id="d12_t1">ὁμολογοῦσιν διαλελύσθαι πρὸς
                        ἀλλήλους</term>
                    <gloss corresp="#d12_t1">we agree together that we have settled</gloss> [<ref target="https://papyri.info/ddbdp/p.hib;1;96" type="external" corresp="#d12_s6 #d12_t1">8060</ref> 260 BCE, Hibeh].</p>
                <p xml:id="d12_p11">Double documents registered in the <term xml:lang="lat">grapheion</term> have an inner script which is a summary or abstract of the
                    main text, beginning with a short <seg xml:id="d12_s7" type="syntax" corresp="../authority-lists.xml#abstract">[date]</seg>. After the <seg xml:id="d12_s8" type="syntax" corresp="../authority-lists.xml#abstract">[date]</seg> in the outer script, the <seg xml:id="d12_s9" type="syntax" corresp="../authority-lists.xml#abstract">[place]</seg> and name of the
                        <term xml:lang="lat">agoranomos</term> follows [<ref type="internal" target="78.xml" corresp="#d12_s7 #d12_s8 #d12_s9">78</ref> l.18-19 99 BCE,
                    Pathyris]. The main verb begins the objective statement <seg xml:id="d12_s10" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ἀπέδοτο</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;]</seg>
                    <gloss corresp="#d12_s10">
                        <hi rend="italic">N</hi> sold</gloss> followed by a
                    description of the property, and another objective statement introduces the
                    buyer <seg xml:id="d12_s11" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ἐπρίατο</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;]</seg>
                    <gloss corresp="#d12_s11">
                        <hi rend="italic">N</hi> bought</gloss>, e.g. [<ref type="internal" target="84.xml" corresp="#d12_s10 #d12_s11">84</ref> l.17,
                    l.20, 123 BCE, Pathyris]. A registration docket is placed after the main text
                        <seg xml:id="d12_s12" type="syntax" corresp="../authority-lists.xml#subscription">[<hi rend="italic">name</hi>
                        &lt;nom.&gt; <term xml:lang="grc">κεχρημάτικα</term>]</seg> [<ref type="internal" target="56.xml" corresp="#d12_s12">56</ref>]. This can be
                    followed in some instances by a [<ref type="descr" target="#gt_diagraphe">bank diagraphe</ref>] [<ref type="internal" target="91.xml">91</ref>].</p>
                <p xml:id="d12_p12">Receipts usually comprise a short <seg xml:id="d12_s13" type="syntax" corresp="../authority-lists.xml#date">[date@start]</seg> followed by
                    the main text, beginning directly with the verb, in both the inner and outer
                    script, e.g. <seg xml:id="d12_s14" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ἔχει</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;]</seg>, [<ref type="internal" target="764.xml" corresp="#d12_s14">764</ref>; <ref type="internal" target="767.xml" corresp="#d12_s14">767</ref>]. <term xml:lang="lat">Symbola</term> display a <seg xml:id="d12_s15" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ὁμολογεῖ</term> + infinitive]</seg> construction, e.g. <seg xml:id="d12_s16" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ὁμολογεῖ</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt; <term xml:lang="grc">ἔχειν</term>]</seg> [<ref type="internal" target="1765.xml" corresp="#d12_s16">1765</ref> 252 BCE, Philadelphia], or <seg xml:id="d12_s17" type="syntax" corresp="../authority-lists.xml#main-text">
                            [<term xml:lang="grc">ὁμολογεῖ</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt; <term xml:lang="grc">μεμετρῆσθαι</term>]</seg> [<ref type="internal" target="1832.xml" corresp="#d12_s17">1832</ref> 257 BCE Philadelphia]. Two Oxyrhynchite
                    receipts with a subjective construction refer to themselves as <term xml:lang="lat">symbola</term>: <term xml:lang="grc">ὁμολογῶ
                        ἠγορακέναι</term>
                    <gloss>I agree that I have bought</gloss>, [<ref target="https://papyri.info/ddbdp/p.hamb;2;185" type="external">4339</ref> l.8-9,
                    245 BCE; <ref target="https://papyri.info/ddbdp/p.hamb;2;186" type="external">4340</ref> l.12, III BCE <term xml:lang="grc">κατὰ τὸ σύμβολον τοῦτο</term> <gloss>according to this <term xml:lang="lat">symbolon</term>
                    </gloss>].</p>
                <p xml:id="d12_p13">Receipts issued by <term xml:lang="lat">naukleroi</term>  have a
                    very brief inner script comprising a short date and the amount of grain
                    transferred, whereas the outer script may have a longer (but not full regnal)
                    date, and the construction <seg xml:id="d12_s18" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">ὁμολογεῖ</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt; (<term xml:lang="grc">ναύκληρος</term>) <term xml:lang="grc">ἐμβεβλῆσθαι</term>]</seg>
                    <gloss corresp="#d12_s18">I <hi rend="italic">N</hi>
                        <term xml:lang="lat">naukleros</term> agree (that x amount of grain) has
                        been loaded</gloss> [<ref type="internal" target="3066.xml" corresp="#d12_s18">3066</ref>; <ref target="https://papyri.info/ddbdp/p.erasm;2;36" type="external" corresp="#d12_s18">5071</ref> 152 BCE, both Arsinoite
                    nome].</p>
            </div>
            <div xml:id="d12_div6_format" type="section" n="3">
                <head xml:id="format">Format</head>
                <p xml:id="d12_p14">Double documents are found in a <term xml:lang="lat">transversa
                        charta</term> format, with the writing against vertical fibres, e.g. [<ref type="internal" target="5838.xml">5838</ref>; <ref target="https://papyri.info/ddbdp/p.giss;1;2" type="external">2796</ref> 173
                    BCE, Krokodilopolis] and in this orientation with the writing along horizontal
                    fibres, e.g. [<ref type="internal" target="56.xml">56</ref>; <ref type="internal" target="78.xml">78</ref>]. They appear in <term xml:lang="lat">pagina</term> format with vertical fibres [<ref type="internal" target="2674.xml">2674</ref>; <ref target="https://papyri.info/ddbdp/bgu;6;1264tripl" type="external">2666</ref> 215-214 BCE, Oxyrhynchus], and with horizontal fibres [<ref type="internal" target="2946.xml">2946</ref>; <ref type="internal" target="3097.xml">3097</ref>]. Some documents can be quite wide, e.g. [<ref type="internal" target="95.xml">95</ref>: H.15.2 x W.58.4cm; <ref type="internal" target="84.xml">84</ref>: H.14.6 x W.63.5cm; <ref type="internal" target="78.xml">78</ref>: H.15 x W.63.5cm], and some quite
                    long, e.g. [<ref type="internal" target="2946.xml">2946</ref>: H.32.3 x W.10cm;
                        <ref type="internal" target="5837.xml">5837</ref>: H.51 x W.40.5cm]. Some
                    receipts are long and narrow, typical of the demotic format [<ref type="internal" target="762.xml">762</ref>: H.30 x W.7cm; <ref type="internal" target="1832.xml">1832</ref>: H.33 x W.9cm (with demotic
                    subscription), both 257 BCE, Philadelphia], see <bibl>
                        <author>Sarri</author>
                        <ptr target="gramm:sarri2018"/>
                        <citedRange>95-97</citedRange>
                    </bibl>.</p>
                <p xml:id="d12_p15">
                    <ref type="other" target="https://www.trismegistos.org/seals/ov_lists/sealslist_1.pdf">Seals</ref> have been preserved on some documents [<ref type="internal" target="5838.xml">5838</ref>; <ref type="internal" target="78.xml">78</ref>;
                        <ref target="91.xml" type="internal">91</ref>; <ref type="internal" target="93.xml">93</ref>; <ref type="internal" target="95.xml">95</ref>;
                        <ref target="https://papyri.info/ddbdp/p.dion;;17" type="external">3100</ref> 108 BCE, Hakoris].</p>
            </div>
            <div xml:id="d12_div7_layout" type="section" n="4">
                <head xml:id="layout">Layout</head>
                <p xml:id="d12_p16">Double documents are written out twice on the same sheet with a
                    space between the inner and outer script to facilitate rolling from the top
                        [<ref target="https://papyri.info/ddbdp/p.dion;;17" type="external">3100</ref>; <ref target="https://papyri.info/ddbdp/sb;6;9405" type="external">5768</ref> 75 BCE, Arsinoite nome], or from the left hand
                    side [<ref type="internal" target="5838.xml">5838</ref>], enclosing and sealing
                    the inner script. There can be another space left between the end of the text in
                    the outer script and any subscriptions [<ref type="internal" target="2946.xml">2946</ref>]. As the outer script may have been written before the inner
                    script, the latter can be cramped into the space allowed [<ref type="internal" target="2946.xml">2946</ref>; <ref type="internal" target="44733.xml">44733</ref>]. The whole document can be presented in a single column [<ref type="internal" target="44733.xml">44733</ref>; <ref type="internal" target="2063.xml">2063</ref>] or two [<ref type="internal" target="78.xml">78</ref>] or more [<ref type="external" target="https://papyri.info/ddbdp/p.adl;;G13">13</ref>; <ref type="internal" target="56.xml">56</ref> (three columns); <ref type="internal" target="131.xml">131</ref> 109 BCE, Pathyris (four
                    columns)].</p>
            </div>
        </body>
    </text>
</TEI>