<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs xi tei" version="2.0">

    <!-- Creates the navigation sidebar for the Description/Classiification pages -->
    
    <!-- nav param: either "descriptions" or "classification" -->
    <xsl:param name="nav"/>
    
    <!-- root param: root of exist app for links -->
    <xsl:param name="root"/>
    
    <!-- ********** -->
    <!-- Templates  -->
    <!-- ********** -->

    <xsl:template match="/">
        <xsl:apply-templates select="tei:taxonomy"/>
    </xsl:template>
    
    <xsl:template match="tei:taxonomy">
            <xsl:apply-templates select="tei:category">
<!--                <xsl:sort select="tei:catDesc" order="ascending"/>-->
            </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="tei:category">
        <!-- each <category> element has pointers to both the description and classification pages -->
        <!-- the correct link is selected through the @type of <ptr>, which is either "descr" or "class" -->
        <xsl:variable name="file" select="replace(tei:catDesc/tei:ptr[@type eq substring($nav, 1, 5)]/@target/string(), '.xml', '')"/>
        <xsl:variable name="link" select="concat($nav, '/', $file)"/>
        <div>
            <!-- add class @attr for level in menu -->
            <xsl:attribute name="class">
                sidebar-item level<xsl:value-of select="count(ancestor::tei:category)"/>
            </xsl:attribute>
        
            <!-- add link if there is a pointer, or link for content page -->
            <xsl:choose>
                <xsl:when test="tei:catDesc/tei:ptr[@type eq substring($nav, 1, 5)]">
                    <a class="sidebar-link" href="{$root}{$link}">
                        <xsl:value-of select="tei:catDesc"/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <a class="sidebar-link" href="{$root}{$nav}/{./@xml:id}">
                        <xsl:value-of select="tei:catDesc"/>
                    </a>
                </xsl:otherwise>
            </xsl:choose>
            
            <xsl:if test="child::tei:category">
                <div>
                    <xsl:apply-templates select="tei:category"/>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
        
    <xsl:template match="tei:catDesc"/>
    
</xsl:stylesheet>