xquery version "3.1";

module namespace browse="http://grammateus.ch/pages/browse";

import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "../gramm.xql";
import module namespace home="http://grammateus.ch/pages/home" at "home.xql";
import module namespace rights="http://grammateus.ch/pages/rights" at "rights.xql";
import module namespace kwic="http://exist-db.org/xquery/kwic";
import module namespace functx = "http://www.functx.com";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare variable $browse:keyword := request:get-parameter("keyword", ());

declare function browse:page() {
    <div class="col">
        <h1 class="h1">Search Results</h1>
        <p>
            From the grammateus website. Search <a href="papyri.html">here</a> for papyri in our database.
        </p>
        <div class="search browse">
        {
            (: if :)
            if ($browse:keyword = '' or not($browse:keyword))
            then browse:term-empty()
            else
                let $hits := browse:search($browse:keyword)
                return
                (
                    browse:term($browse:keyword),
                    browse:count($hits),
                    browse:display($hits)
                )
        }
    
        </div>
    </div>
};

declare function browse:search($keyword as xs:string){
    
    (: search both TEI and HTML files. :)
    let $collections := 
        collection($g:db)//tei:p[ft:query(., $keyword)] |
        collection($g:db)//tei:head[ft:query(., $keyword)] |
        collection($g:db)//tei:p[descendant::tei:ptr[matches(@target, $keyword)]] |
        collection($g:editorial)//h1[ft:query(., $keyword)] |
        collection($g:editorial)//h3[ft:query(., $keyword)] |
        collection($g:editorial)//li[ft:query(., $keyword)] |
        collection($g:editorial)//p[ft:query(., $keyword)]
                        
    for $match in $collections
        let $document := document-uri(root($match))
        let $expanded := util:expand($match, "expand-xincludes=no")
        return 
            <hit>
                <match>{$expanded}</match>
                <document>{$document}</document>
            </hit>

};

declare function browse:term-empty() {
    <p>
        <span class="badge remove-filters">No search term!</span>
        <div class="alert alert-dark" role="alert" style="width:max-content;">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
            Use the search box in the top right corner to search through the website.
        </div>
    </p>
};

declare function browse:term($keyword) {
    <p><span class="badge badge-secondary">Search term: {$keyword}</span></p>
};

declare function browse:count($hits) {
    let $n := count($hits//exist:match)
    let $result := if ($n > 1) then "Results" else "Result"
    return
        <h3 class="h3">{$n || " " || $result}</h3>
};

declare function browse:display($hits) {
    (: the $hit/match is a node, where the xml is not transformed into HTML
        there is some styling missing e.g. for bibl entries or greek terms. :)
    for $h in $hits
        let $expanded := $h/match
        let $doc := $h/document
        let $pagetype := substring-after($doc, $g:db)
        => functx:substring-before-last("/")
        => functx:substring-after-last("/")
        => replace("editorial", "0")
        => replace("introduction", "1")
        => replace("classification", "2")
        => replace("description", "3")
        let $log := console:log($pagetype)
        (: group by page where hits appear :)
        group by $doc
        order by $pagetype[1]
    return 
        browse:result($expanded, $doc)
};

(: 
:~ returns search result from top-right search form
 :
 : @param expanded - node from a hit (either XML or HTML)
 : @param doc - document-uri for the hit node
 :)
declare function browse:result($expanded, $doc) {
    (: Find the title of the resource either from TEI or HTML :)
    (: TODO: make it a function? :)
    let $title := if(doc($doc)//tei:TEI) then doc($doc)//tei:body/tei:head[1] else doc($doc)//h1/string()
    (:resource:)
    let $resource := functx:substring-after-last($doc, "/")
        => functx:substring-before-last(".")
    (: the pagetype comes after /data, but before /name-of-resource
        it can be introduction, classification, descriptions, for the XML. 
        remove editorial if the results comes from an html node :)
    let $pagetype := substring-after($doc, $g:db)
        => functx:substring-before-last("/")
        => functx:substring-after-last("/")
        => replace("editorial", "")
    return
    <div class="result border-bottom">
        <a href="{$pagetype}{if($pagetype) then "/" else ()}{$resource}" target="_blank">{$title}</a>
        {(: get the search term in context (300 characters before/after) :)
            let $config := <config xmlns="" width="200"/>
            for $match in $expanded
                (: each match is separated, it would be possible to group by paragraph :)
                for $m in $match//exist:match
                let $para := $match/tei:p/@xml:id/string()
                let $summary := kwic:get-summary($match, $m, $config)
                return
                        element {node-name($summary)} { 
                <span class="browse-para"><a href="{$pagetype}{if($pagetype) then "/" else ()}{$resource}#{$para}">{gramm:fa-open()}</a>{" "}</span>,
                $summary/*
            }
        }

    </div>
    
};