<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d46">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Bank Receipt</title>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:vldudaboebhzzab67s66uey6ii</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_diagraphe"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2020-05-25">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2020-05-13">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Roman Bank Receipt</head>
            <head type="subtitle">
                <term xml:lang="grc">διαγραφή</term>
            </head>
            <p xml:id="d46_p1">The word <term xml:lang="lat">diagraphe</term> (<term xml:lang="grc">διαγραφή</term>) applies to a payment made through a bank, and by extension to
                the receipt that constitutes proof of the payment. Such payments are recorded in
                registers, and abstracts can be produced to confirm the existence of the payment.
                This description concerns Roman period <term xml:lang="lat">diagraphai</term>; for
                Ptolemaic bank documents, such as receipts, and instructions to accept a payment
                (both referred to as <term xml:lang="lat">diagraphai</term>), cf. the Erbstreit
                Archive <bibl>
                    <author>TM Arch 81</author>
                    <ptr target="gramm:vandorpe2011"/>
                </bibl>, the Archive of the Theban Bank (section 3) <bibl>
                    <author>TM Arch 205</author>
                    <ptr target="gramm:dogaerVandorpe2022"/>
                </bibl>, and see <bibl>
                    <author>Vandorpe and Vleeming</author>
                    <ptr target="gramm:vandorpeVleeming2017"/>
                    <citedRange>no. 1, with commentary</citedRange>
                </bibl> and <bibl>
                    <author>UPZ I</author>
                    <ptr target="gramm:wilcken1927"/>
                    <citedRange>no. 114, with commentary</citedRange>
                </bibl>.</p>
            <p xml:id="d46_p2">
                <term xml:lang="lat">Diagraphai</term> are sometimes mentioned within
                the text of private contracts drafted in the form of a letter [<ref type="descr" target="#gt_cheiro">cheirographon</ref>] in Oxyrhynchus. They provide an
                indirect trace of the use of a <term xml:lang="lat">diagraphe</term> for making
                payments through a bank; they go back to the Ptolemaic era [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;14;1639">5257</ref> 73 or 44 BCE,
                Oxyrhynchus] and continue well into the Roman era [<ref type="internal" target="15644.xml">15644</ref>, 65 CE; <ref type="internal" target="22521.xml">22521</ref> 144
                CE, both Oxyrhynchus]. Direct attestations of receipts issued by banks (i.e. not
                mere mentions within a contract) appear at the very beginning of the Augustan age
                    [<ref type="external" target="https://papyri.info/ddbdp/bgu;4;1194">18644</ref>
                27 BCE, Herakleopolis; <ref type="external" target="https://papyri.info/ddbdp/p.lond;3;890">22701</ref>, 6 BCE (copy of a
                    <term xml:lang="lat">diagraphe</term>)].</p>
            <p xml:id="d46_p3">In the early Roman period, transactions such as loans or sales are
                recorded – and are recognized as legally binding – through the use of a bank
                receipt, which can be produced as evidence before a court in order to support the
                claim of the existence of a contractual relation.</p>
            <p xml:id="d46_p4">Bank receipts are divided by modern specialists in two broad
                categories: a) those that are not considered as self-contained, i.e. they refer to
                another document produced by a separate authority to make the transaction legally
                binding; b) those that are considered as self-contained, i.e. they include all the
                clauses that make the transaction legally binding <bibl>
                    <author>Wolff</author>
                    <ptr target="gramm:wolff1978b"/>
                    <citedRange>96</citedRange>
                </bibl>.</p>
            <p xml:id="d46_p5">It seems that no official legislation was introduced to actively
                support such a development towards self-contained, legally binding bank receipts;
                apparently bankers themselves gradually gained acceptance as a body that could issue
                documents to be produced before a court <bibl>
                    <author>Wolff</author>
                    <ptr target="gramm:wolff1978b"/>
                    <citedRange>100-103</citedRange>
                </bibl>.</p>
            <div xml:id="d46_div1_bankreceipt_I" type="section" n="1">
                <head xml:id="bankreceipt_I">First century CE</head>
                <p xml:id="d46_p6">In the I CE, bank receipts are abstracts from bank registers
                            <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1978b"/>
                        <citedRange>97-98</citedRange>
                    </bibl> where a transaction was recorded. From a legal point of view, their
                    contents correspond to the receipts that are not self-contained, i.e. they refer
                    to another document produced by a separate authority to make the transaction
                    legally binding.</p>
                <div type="subsection" n="1" xml:id="d46_div11_structure_I">
                    <head xml:id="structure_I">Structure</head>
                    <p xml:id="d46_p7">The praescript is characterized by the following elements:
                            <list type="unordered">
                            <item>
                                <seg xml:id="d46_s1" type="syntax" corresp="../authority_lists.xml#descr-contents">
                                    <term xml:lang="grc">ἀντίγραφον διαγραφῆς</term> [through <term xml:lang="grc">διά</term>
                                    <hi rend="italic">bank</hi> &lt;gen.&gt;]</seg>
                            </item>
                            <item>
                                <seg xml:id="d46_s2" type="syntax" corresp="../authority_lists.xml#date">[date@start]</seg>
                            </item>
                            <item>
                                <seg xml:id="d46_s3" type="syntax" corresp="../authority_lists.xml#introduction">[from <hi rend="italic">creditor</hi> &lt;nom.&gt;] [to <hi rend="italic">debtor</hi> &lt;dat.&gt;]</seg>
                            </item>
                        </list>
                    </p>
                    <p xml:id="d46_p8">The implied verb, which is not explicitly provided on the
                        document, is <term xml:lang="grc">διέγραψε</term>, e.g. [<ref type="external" target="https://papyri.info/ddbdp/p.grenf;2;43">11326</ref> 92 CE, Soknopaiou Nesos]. In the subsequent part of the
                        document, however, the payment is expressed from the point of view of the
                        person receiving the payment, usually a debtor, with the infinitive <term xml:lang="grc">ἀπέχειν</term>, but without a conjugated verb introducing
                        the infinitive [<ref type="internal" target="21035.xml">21035</ref> 57 CE,
                        Alexandria; <ref type="internal" target="9448.xml">9448</ref> I CE,
                        Arsinoite nome; <ref type="internal" target="8736.xml">8736</ref> II CE,
                        Herakleopolite nome]. This is explained by the fact that those documents are
                        abstracts from registers.</p>
                </div>
                <div type="subsection" n="2" xml:id="d46_div12_format_I">
                    <head xml:id="format_I">Format</head>
                    <p xml:id="d46_p9">A distinction can be made between documents from Alexandria and the Arsinoite and
                            Herakleopolite nomes on the one hand, and documents from Hermopolis on the other.</p>
                    <p xml:id="d46_p10">In the first area, the format is quite unremarkable: a
                        vertical sheet in <term xml:lang="lat">pagina</term> format, with a height
                        of ca. 22 cm and a width of ca. 8 cm, with the writing along the fibres
                            [<ref type="internal" target="8736.xml">8736</ref> (Herakleopolite
                        nome); <ref type="internal" target="9448.xml">9448</ref> (Arsinoite nome)].
                        In a receipt from Alexandria [<ref type="internal" target="21035.xml">21035</ref>], the height is 36.5 cm.</p>
                    <p xml:id="d46_p11">In the Hermopolite nome, the format
                        of bank receipts can be more elaborate. In [<ref type="external" target="https://papyri.info/ddbdp/p.lond;3;1168">22825</ref> 44 CE] the
                        scribe writes on a horizontally oriented sheet (H. 24.8 x W. 108.6 cm) in
                        wide columns following the direction of fibres; the relevant contract is
                        drawn up and a copy of the corresponding bank payment is added to the
                        document by another hand.</p>
                </div>
                <div type="subsection" n="3" xml:id="d46_div13_layout_I">
                    <head xml:id="layout_I">Layout</head>
                    <p xml:id="d46_p12">The layout of these I CE documents do not seem to follow a
                        strict pattern. In [<ref type="internal" target="8736.xml">8736</ref>],
                        apart from a slight <term xml:lang="lat">ekthesis</term> in line 1
                        highlighting the caption <term xml:lang="grc">ἀντίγραφον</term>, the text is
                        written by a single hand in a single block. In [<ref type="internal" target="9448.xml">9448</ref>], the enlarged initial <term xml:lang="lat">alpha</term> of <term xml:lang="grc">ἀντίγραφον</term> serves the same
                        purpose of highlighting the nature of the document; in line 3, the date is
                        also marked by the symbol for (<term xml:lang="grc">ἔτους</term>) placed in
                            <term xml:lang="lat">ekthesis</term>; moreover, the debtor has confirmed
                        the transaction in his own hand, below the main text.</p>
                    <p xml:id="d46_p13">It appears that apart from the wording of the praescript,
                        there is not much consistency in the way scribes prepare such receipts on
                        behalf of banks. This may be explained by the fact that, in the first
                        century CE, bank receipts are not yet considered valid self-contained
                        contracts. The degree of formality increases in the subsequent period, as
                        bank receipts gain acceptance as self-contained, legally binding
                        contracts.</p>
                </div>
            </div>
            <div xml:id="d46_div2_bankreceipt_II_III" type="section" n="2">
                <head xml:id="bankreceipt_II_III">Second-third century CE</head>
                <p xml:id="d46_p14">In the early second century CE, bank receipts become
                    self-contained: instead of relying on a transaction supervised by another
                    authority, they become <hi rend="italic">per se</hi> legally binding contracts.
                    This change is reflected in the structure of those receipts: most conspicuously,
                    a new praescript appears, with a certain degreee of consistency between the
                    Hermopolite and Arsinoite nomes.</p>
                <div type="subsection" n="1" xml:id="d46_div21_structure_II">
                    <head xml:id="structure_II">Structure</head>
                    <p xml:id="d46_p15">The absence of a mention of a copy (<term xml:lang="grc">ἀντίγραφον</term>) suggests that those receipts are not abstracts from
                        registers anymore, but receipts in their own right. The praescript follows a
                        new pattern:<list type="unordered">
                            <item>
                                <seg xml:id="d46_s4" type="syntax" corresp="../authority_lists.xml#date">[date@start]</seg>
                            </item>
                            <item>
                                <seg xml:id="d46_s5" type="syntax" corresp="../authority_lists.xml#descr-contents">(<term xml:lang="grc">διαγραφή</term>) [through <term xml:lang="grc">διά</term>
                                    <hi rend="italic">bank</hi> &lt;gen.&gt;]</seg>
                            </item>
                            <item>
                                <seg xml:id="d46_s6" type="syntax" corresp="../authority_lists.xml#introduction">[from <hi rend="italic">creditor</hi> &lt;nom.&gt;] [to <hi rend="italic">debtor</hi> &lt;dat.&gt;]</seg>
                            </item>
                        </list>
                    </p>
                    <p xml:id="d46_p16">This is followed by a verb in the infinitive, e.g. <seg xml:id="d46_s7" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἔχειν</term>]</seg> [<ref type="internal" target="9302.xml" corresp="#d46_s7">9302</ref> II CE, Arsinoite nome],
                            <seg xml:id="d46_s8" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἐσχηκέναι</term>]</seg> [<ref type="external" target="https://papyri.info/ddbdp/p.lond;3;932" corresp="#d46_s8">22718</ref> 211 CE, Hermopolis], <seg xml:id="d46_s9" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἀπέχειν</term>]</seg> [<ref type="internal" target="9194.xml" corresp="#d46_s9">9194</ref> II CE, Arsinoite nome], <seg xml:id="d46_s10" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">πεπρακέναι</term>]</seg> [<ref type="internal" target="9164.xml" corresp="#d46_s10">9164</ref>II CE, Soknopaiou Nesos; <ref type="internal" target="22324.xml" corresp="#d46_s10">22324</ref> III
                        CE; <ref type="external" target="https://papyri.info/ddbdp/p.lond;3;1158" corresp="#d46_s10">22810</ref> 226 CE, both Hermopolis], with no
                        conjugated verb to introduce it; the infinitive may also be missing [<ref type="external" target="https://papyri.info/ddbdp/p.giss;;32">19432</ref>
                        153 CE; <ref type="external" target="https://papyri.info/ddbdp/p.flor;1;1">23525</ref> 188 CE, both Hermopolis]. This wording apparently continues
                        that of the first century, although now the receipts are no longer abstracts
                        from registers: they function as full-fledged contracts of loan, sale etc.,
                        as shown in the detailed description of the transaction in e.g. the bank
                        receipt for a camel sale [<ref type="internal" target="9164.xml">9164</ref>], which contains a guarantee clause (lines 20-23). In a bank
                        receipt rent of an oil-press [<ref type="internal" target="10938.xml">10938</ref> II CE, Theadelphia], there is a clause confirming the
                        validity of the transaction (lines 18-20). In another bank receipt for a
                        camel sale [<ref type="internal" target="9194.xml">9194</ref>], the buyer
                        pledges to declare his purchase in his declaration of property (lines
                        18-21).</p>
                    <p xml:id="d46_p17">A particular loan document is identified by
                                <bibl>
                            <author>Wolff</author>
                            <ptr target="gramm:wolff1978b"/>
                            <citedRange>103</citedRange>
                        </bibl> as a fictitious <term xml:lang="lat">diagraphe</term> [<ref type="internal" target="20157.xml">20157</ref> 102 CE]: in the guise of
                        a copy from a bank register, its purpose is to confirm the repayment of a
                        loan that was already mentioned in another document, also preserved [<ref type="external" target="https://papyri.info/ddbdp/bgu;1;44">9091</ref>
                        102 CE, Arsinoite nome], written in the form of a <term xml:lang="lat">cheirographon</term> - here the creditor acknowledges the repayment of
                        the loan and promises to confirm it with a bank receipt.</p>
                </div>
                <div type="subsection" n="2" xml:id="d46_div21_format_II">
                    <head xml:id="format_II">Format</head>
                    <p xml:id="d46_p18">Bank receipts from the Arsinoite nome
                        retain the previous <term xml:lang="lat">pagina</term> format, and the
                        writing along horizontal fibres. The distinctive format found in the
                        Hermopolite nome in the I CE remains in use in the two subsequent centuries,
                        with a very wide horizontal sheet cut from a roll with a height of c. 24 cm
                        and a width of approximately 60 to 100 cm. The text is written along the
                        fibres, in two columns: one column contains the bank receipt itself, while
                        the other records the agreement underlying the transaction; in most cases
                        this happens on the same day. The bank receipt can be placed first [<ref type="internal" target="22324.xml">22324</ref>] or second [<ref type="internal" target="19654.xml">19654</ref> I CE, Hermopolis].</p>
                </div>
                <div type="subsection" n="3" xml:id="d46_div21_layout_II">
                    <head xml:id="layout_II">Layout</head>
                    <p xml:id="d46_p19">The text is written in a single block, but the date stands
                        out because of the large epsilon of <term xml:lang="grc">ἔτους</term>
                            [<ref type="internal" target="9194.xml">9194</ref>; <ref type="internal" target="9302.xml">9302</ref>; <ref type="external" target="https://papyri.info/trismegistos/13751">13751</ref> 151 CE,
                        Ptolemais Euergetis; <ref type="internal" target="9164.xml">9164</ref>; <ref type="internal" target="9825.xml">9825</ref> II CE, Soknopaiou Nesos].
                        The presence of a date at the beginning of the document lends a
                        quasi-official character to such bank receipts.</p>
                    <p xml:id="d46_p20">Two documents concerning a loan are pasted together in [<ref type="external" target="https://papyri.info/ddbdp/bgu;2;472_2">9195/9196</ref> 139 CE, Karanis]: col. ii contains a loan contract
                        dating from 139 CE, drafted by the notarial office in Karanis. When the loan
                        was repaid in 141 CE, a bank recorded the payment with a receipt (col. i),
                        which was pasted on the left side of the contract. The contract itself was
                        crossed out and thus cancelled. This shows that two distinct types of
                        documents (i.e. contract drafted by a notary, and bank receipt) were
                        considered as appropriate to validate the two stages of the process, i.e.
                        loan and repayment. Presumably, the copy of the loan contract belonged to
                        the creditor; once his loan was paid back, he handed the contract to the
                        banker, who crossed it out; then both the contract and the bank receipt were
                        handed to the debtor, as proof that his loan had been repaid.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>