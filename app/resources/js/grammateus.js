$(document).ready(function(){
  
  
  // ********************
  // SHOW Page
  // ********************
  
  // set SVG size
  var svg_width = $("#svg").width();
  var container_width = $("#text-viz").width();
  
  if (svg_width > container_width){
      $("#svg").width("100%");
      $("#svg").height("auto");
      $("#svg").toggle();
  }else{
      $("#svg").toggle();
  }
  
  // display SVG zoom control only for papyri width > 5 and height > 5 ?
  var controls = true;
  
  // initialize pan and zoom for SVG
  // only on pages with svg for papyri (otherwise messes up with back-to-top button)
  if($('#svg').length){
    var panZoomTiger = svgPanZoom('#svg', {
      controlIconsEnabled: true
    });  
  }

  
  // ********************
  // Recto/Verso
  // There is no fully consistent encoding in papyri.info
  // which creates some problems and prevent automatic processing
  // ******************** 
  
  // TM 219272: display columns that were hidden
  if ( $('idno').text() == '219272'){
      $('span[id*="abv"] + .textpart').show();
  }
  
  // TM 11695: hide the recto, we display the verso this time (removed from data)
  if ( $('idno').text() == '11965'){
      $('span[class="textpartnumber r"]').nextAll('.textpart').hide();
  }
  
  // TM 11697: hide the verso
  if ( $('idno').text() == '11967'){
      $('span[class="textpartnumber v"]').nextAll('.textpart').hide();
  }
  
  // TM 9196: hide the verso
  if ( $('idno').text() == '9196'){
      $('span[class="textpartnumber v"]').nextAll('.textpart').hide();
  }
  
  // TM 28932: we have both recto (28932b) and verso (28932b) as separate document
  // For 28932b, we need to display verso, hide recto
  // also for TM 31761, 31055, 25449, 36198
  if ( $('#filename').text() == '28932b' || $('idno').text() == '31761' || $('idno').text() == '25449' || $('idno').text() == '36198'){
      $('span[class="textpartnumber r"]').next('.textpart').hide();
      $('span[class="textpartnumber v"]').next('.textpart').show();
  }
  //same for TM 31055, with additional adjustments (font-size, margin)
  if ($('idno').text() == '31055'){
      // hide recto, show verso
      $('span[class="textpartnumber r"]').next('.textpart').hide();
      $('span[class="textpartnumber v,ctr"]').next('.textpart').show();
      $('span[class="textpartnumber v,m"]').next('.textpart').show();
      // font size
      $('foreignObject').attr("font-size", "6.5");
      // margin text see below
  }
  
  
  
  
  // ********************
  // margin text in vertical papyri
  // only known example so far are TM 905, 8621, 14917, 18507
  // ********************
  
  // style margin text to the left and to 25% width
  $("#abms, #abr\\,ms").next("div").css({
    "order": "-1", //place first
    "break-after": "right", // make separate columns (firefox)
    "width": "25%", 
    "word-wrap": "break-word"});
    
  // make separate columns (chrome) => messing up with firefox (cf. TM8621)
  //if (userAgentString.indexOf("Chrome") > -1) {
    //$("#abms, #abr\\,ms").closest("#edition").css({"flex-flow": "inherit"});
  //}  
   
  
  // style margin text to the right and to 25% width
  // TM 14917. This will probably not work well in multi-culumn documents
  // TM 18507
  $("#abmd, #abr\\,md, #abr\\,minf").next("div").css({"order": "2", "page-break-before": "right", "width": "25%", "word-wrap": "break-word"});
  
  // style center text to 75% width
  //TEMPORARY: .ctr as class
  $(".ctr, .r\\,ctr").next().css({"width": "75%"});
  
  // style edition ancestor
  // TM905 needs 2 columns ms/ctr, but this is NOT WORKING IN CHROME
  if ( $('idno').text() == '905'){
    $("#abms").closest("#edition").css({"column-count": "2", "flex-flow": "column wrap"});
  }else{
    $("#abms").closest("#edition").css({}); 
  }
  
  // --------
  // add empty lines before the margin text
  // the number of lines depend on where in the papyrus the ms text begins
  // TM 905: starts at line 13, which is found in <a id="ms-l13">
  
  //ABMS
  // find the first element with an id in the margin text and select id value
  var line_attr = $("#abms").next().find("[id]:first").attr("id");
  if (typeof line_attr !== 'undefined'){
    var line_nb = line_attr.substr(line_attr.lastIndexOf("l") + 1);
  
    // add empty lines
    for (i = 0; i < line_nb-1; i++) {
      $("#edition").next("div").children("span").prepend("<br/>");
    }
  }
  
  // ABR,MINF - TM 18507, ABV,M = TM 31055
  var line_attr_2 = $("#abr\\,minf, #abv\\,m").next().find("[id]:first").attr("id");
  console.log(line_attr_2);
  if (typeof line_attr_2 !== 'undefined'){
    var line_nb = line_attr_2.substr(line_attr_2.lastIndexOf("l") + 1);
    console.log(line_nb);
    // add empty lines
    for (i = 0; i < line_nb-1; i++) {
      $("#abr\\,minf, #abv\\,m").next("div").children("span").prepend("<br/>");
    }
  }
  // set font size for TM 18507
  //if ($('idno').text() == '18507'){
    //$("#edition").css({"font-size":"3.8px"})
  //}
  
  // ABR,MS - TM 8621 special case.
  // margin text is not an annotation but the continuation of main text (scribe ran out of paper)
  // emtpy lines set by EN to 27, so that ctr and ms text are aligned at the bottom
  if ( $('idno').text() == '8621'){
    var line_nb_8621 = 27;
  
    // add empty lines
    for (i = 0; i < line_nb_8621-1; i++) {
      $("#abr\\,ms").next("div").children("span").prepend("<br/>");
    }
  }
  
  
  // ********************
  // double documents 
  // prevent ext/int text from being separated in columns
  // 
  // ********************
  
  // prevent column display
  $("#abr\\,int, #abint").closest("#edition").css({"display":"block"});
  
  // move interior script first
  $("#abr\\,int, #abint").next("div").prependTo("#edition");
  
  //int/ext are fragm A/B
  if ( $('idno').text() == '2063'){
      $('#abFrA').closest("#edition").css({"display":"block"});
      $('foreignObject').attr("font-size", "7.0");
  }
  
  
  // ********************
  // Special cases
  // ********************
  
  //Tm 2092: hide fragment B textpart, it is a different document.
  //adapt font because the calculations are offset 
  if ( $('idno').text() == '2092'){
      $('span[class="textpartnumber FrB"]').nextAll('.textpart').hide();
      $('foreignObject').attr("font-size", "13.5");
      $('foreignObject').css("line-height", "1.75");
  }
  
  //Tm 16447: hide fragment B textpart, it is actually the verso.
  //correct second column as margin inferior text, below and perpendicular to main text
  //adapt line-height because the calculations are offset 
  if ( $('idno').text() == '16447'){
      $('span[class="textpartnumber B"]').nextAll('.textpart').hide();
      console.log("test");
      $('#edition').css('display', 'inherit');
      //$('foreignObject').attr("font-size", "13.5");
      $('foreignObject').css("line-height", "1.75");
  }
  
  //TM 15650, 15651: single column instead of 2
  if ( $('idno').text() == '15650' || $('idno').text() == '15651'){
      $('#edition').css('display', 'inherit');
  }
  
  // TM 17627: perpendicular text (eventually, font-size to 6.5 and line-height to 1.29...)
  if ( $('idno').text() == '17627'){
      // remove width
      $('#abr\\,ctr, #abr\\,md').next('.textpart').removeAttr('style');
      // rotate -90
      $('#abr\\,md').next('.textpart').css({'transform':'rotate(-90deg)', 'position': 'relative', 'left': '150px'});
  }
  
  // TM 31141: perpendicular text (eventually, font-size to xxx and line-height to xxx)
  if ( $('idno').text() == '31141'){
      // rotate -90
      $('#abr\\,ms').next('.textpart').css({'transform':'rotate(90deg)', 'transform-origin': 'top', 'position': 'relative', 'width': '100%', 'top': '75px'});
  }
  
  // TM 31107: perpendicular text (eventually, font-size to xxx and line-height to xxx)
  if ( $('idno').text() == '31107'){
      console.log("31107");
      // rotate -90
      $('#abr\\,ms').next('.textpart').css({'transform':'rotate(90deg)', 'transform-origin': 'top', 'position': 'relative', 'width': '100%', 'top': '75px'});
  }
  
  // TM 30321: perpendicular text not separated in a different textpart. ignoring it for now.
  
  // TM 21704: the middle section is in 3 columns.
  // for the flex display, I am adding a wrapper around the three textparts
  
  if ( $('idno').text() == '21704'){
    //add wrapper
    $('#ab1, #ab2, #ab3').next('.textpart').wrapAll('<div class="wrapper"/>');
    //remove the ctr 75% width meant for margin /ctr text
    $('.textpart').removeAttr('style');
    //fix flex direction
    $('#edition').css({'flex-direction':'column'});
    //adapt font size which cannot be calculated properly
    $('foreignObject').attr("font-size", "7.845");
  }
     
 // ********************
 // BROWSE Page
 // PAPYRI Page
 // ********************
 
 // toggle facet header caret
 $(".facet-header > button").on('click', function(event) {
    $(this).children("i").toggleClass("fa-caret-up");
    $(this).children("i").toggleClass("fa-caret-down");
 });
 
 // toggle search tip card header caret
 $(".card-header > button").on('click', function(event) {
    $(this).children("i").toggleClass("fa-caret-up");
    $(this).children("i").toggleClass("fa-caret-down");
 });
 
 $('.go-to-help').on('click', function(event){
    $('body,html').animate({
		scrollTop: 0
	}, 400);
    console.log(this.type);
	$("#"+this.type).parent().find("i").toggleClass("fa-caret-up");
    $("#"+this.type).parent().find("i").toggleClass("fa-caret-down");
});
 
 // back-to-top button
 $(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});
});