<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d4">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Cheirographon</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:rebksy2ipbhmjh4a55arx7j5bq</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_cheiro"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2021-03-25">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2021-01-30">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head rend="bold">Cheirographon</head>
            <p xml:id="d4_p1">The <term xml:lang="lat">cheirographon</term> (literally
                    <gloss>handwritten document</gloss>) was a document written in the form of a letter, and
                was often used for a formal acknowledgement of a transaction, or to establish a
                contract between two parties; in the latter case some legal clauses could be added
                to ensure the contract was binding, see <bibl>
                    <author>Keenan et al.</author>
                    <ptr target="gramm:keenan2014"/>
                    <citedRange>44</citedRange>
                </bibl>. The contract could be easily produced without the need to employ a notary
                or enlist witnesses. This ease of production meant that it remained popular from its
                first attestation in the Ptolemaic period well into the Byzantine period <bibl>
                    <author>Yiftach-Firanko</author>
                    <ptr target="gramm:yiftach2008a"/>
                    <citedRange>325 n.3, 327, 328</citedRange>
                </bibl>.</p>
            <p xml:id="d4_p2">Most examples come from the Arsinoite and Oxyrhynchite nomes, but
                there are others from the Thebaid [<ref type="internal" target="60.xml">60</ref> 136
                BCE], Herakleopolite nome [<ref type="internal" target="18652.xml">18652</ref> 18
                BCE], Hermopolis, e.g. [<ref type="internal" target="3116.xml">3116</ref> 111 BCE;
                    <ref type="internal" target="17025.xml">17025</ref> 128 CE; <ref type="internal" target="18722.xml">18722</ref> 271 CE], and Oasis Magna [<ref type="internal" target="22625.xml">22625</ref> 247 CE]. They range from a simple receipt, e.g.
                    [<ref type="internal" target="16534.xml">16534</ref> 62 CE, Oxyrhynchus], to
                long, formal contracts, e.g. [<ref type="internal" target="16594.xml">16594</ref>
                154 CE, Oxyrhynchus] and were concerned with common transactions such as loans and
                sales, see <bibl>
                    <author>Yiftach-Firanko</author>
                    <ptr target="gramm:yiftach2008a"/>
                    <citedRange>327, 329</citedRange>
                </bibl>. </p>
            <p xml:id="d4_p3">The <term xml:lang="lat">cheirographon</term> has an epistolary
                origin, clearly displaying the structure, format, and layout of a letter; it is
                subjectively formulated, and has the same opening address and closing salutation as
                that of a letter, e.g. [<ref type="internal" target="3116.xml">3116</ref> 111 BCE;
                    <ref type="internal" target="3746.xml">3746</ref> 92/59 BCE, Tebtunis]. As it
                evolved, the closing salutation was eventually dropped, e.g. [<ref type="internal" target="18652.xml">18652</ref> 18 BCE; <ref type="internal" target="12147.xml">12147</ref> 45 CE, Tebtunis]. Within the document, the contract could be
                referred to as <term xml:lang="grc">ἡ χείρ</term>, e.g. [<ref type="internal" target="3116.xml">3116</ref> l.24-25; <ref type="internal" target="3746.xml">3746</ref> l.13], or <term xml:lang="lat">cheirographon</term>, e.g. [<ref type="internal" target="11408.xml">11408</ref> l.24, 144/5 CE, Arsinoite nome;
            <ref type="external" target="https://papyri.info/ddbdp/p.lips;1;12">31906</ref> (verso), III-IV CE,
                Hermopolis].</p>
            <p xml:id="d4_p4">The feature distinguishing this contract from any other type is that
                it was ostensibly written in the hand of the initiating party (<term xml:lang="grc">ἡ χείρ</term> = <gloss>hand</gloss>); as such, it did not necessarily carry a
                signature, e.g. [<ref type="internal" target="12054.xml">12054</ref> 119 CE,
                Karanis; <ref type="internal" target="13698.xml">13698</ref> 158 CE, Soknopaiou
                Nesos; <ref type="internal" target="18240.xml">18240</ref> 217 CE, Soknopaiou
                Nesos]. The fact that it was an autograph document was often stated within the
                contract, e.g. <term xml:lang="grc">ἡ ἰδιόγραφός μου χείρ</term>
                <gloss>written by my own hand</gloss> [<ref type="internal" target="17415.xml">17415</ref>
                l.9, 186 CE, Oxyrhynchus], or <term xml:lang="grc">τὸ δὲ χειρόγραφον τοῦτό ἐστιν
                    ἐμὸν ἰδιόγραφον</term>
                <gloss>the <term xml:lang="lat">cheirographon</term> is completely written by me</gloss>
                    [<ref type="internal" target="11408.xml">11408</ref> l.24-25]. Theon and his
                brother Petaus, the barely literate village scribe of Ptolemais Hormou, were parties
                to a loan agreement where Theon stated: <term xml:lang="grc">Θέων ἔγραψα καὶ ὑπὲρ
                    τοῦ Πεταῦτος τὰ πλεῖστα</term>
                <gloss>I Theon have written most of it and also for Petaus</gloss> [<ref target="https://papyri.info/ddbdp/p.petaus;;31" type="external">8850</ref>
                l.13-14, 183-184 CE]; often in cases of illiteracy another named person wrote the
                whole document on behalf of the issuer, e.g. [<ref type="internal" target="60.xml">60</ref>; <ref type="internal" target="9159.xml">9159</ref> 276-277,
                Philadelphia].</p>
            <p xml:id="d4_p5">Sometimes, however, a scribe wrote the main text and the issuer added
                a summary in his own hand (<term xml:lang="grc">ὑπογραφή</term>), e.g. [<ref type="internal" target="15361.xml">15361</ref> 220 CE, Oxyrhynchus; <ref type="internal" target="16901.xml">16901</ref> 289 CE, Antinoopolis]. Some
                documents were written by a scribe and signed by a third party at the request of the
                main party [<ref type="internal" target="8908.xml">8908</ref> 289 CE, Arsinoite
                nome]. Occasionally witnesses are noted [<ref type="internal" target="7332.xml">7332</ref>]. The subscription (<term xml:lang="grc">ὑπογραφή</term>) of a
                contract for the division of property [<ref type="internal" target="10315.xml">10315</ref> 297 CE, Karanis] is written on behalf of four of the parties by two
                named persons, while the fifth party was able to write for himself. </p>
            <p xml:id="d4_p6">As some of the documents were not written by professional scribes the
                papyri often display mistakes and corrections [<ref type="internal" target="12054.xml">12054</ref>; <ref type="internal" target="16447.xml">16447</ref> (verso) 247 CE, Oxyrhynchus], as well as orthographic errors [<ref type="internal" target="19956.xml">19956</ref> 105 CE]. If a more competent hand
                wrote the main contract, the summary written by the issuer can display orthographic
                errors [<ref target="https://papyri.info/ddbdp/p.genova;2;62" type="external">15534</ref> 98 CE, Oxyrhynchus]. Sometimes the hand is clearly that of someone
                who found it difficult to write, see for example the laboured script of [<ref type="internal" target="19956.xml">19956</ref>], and the signature of the weaver
                in [<ref type="internal" target="20987.xml">20987</ref>]. Some documents are
                particularly well prepared and beautifully written, e.g. the chancery hand of [<ref type="internal" target="8908.xml">8908</ref>]. Two versions of a contract for
                the deposit of grain are written on either side of the same sheet: one is written in
                a good hand, while the other (later written) copy is in a hastier, more careless
                hand [<ref type="internal" target="16447.xml">16447</ref> 247 CE, Oxyrhynchus] (see
                P. Oxy XLII 3049, introduction).</p>
            <p xml:id="d4_p7">A number of Roman <term xml:lang="lat">cheirographa</term> from
                Oxyrhynchus record the involvement of a bank in the transaction documented in the
                contract and appear to have been drawn up by professional scribes, e.g. [<ref type="internal" target="15644.xml">15644</ref> 65 CE; <ref type="internal" target="22521.xml">22521</ref> 144 CE], both loan repayments with a [<ref type="descr" target="#gt_diagraphe">bank diagraphe</ref>] recorded at the end of
                the document. These always carried a subscription (<term xml:lang="grc">ὑπογραφή</term>) in the hand of one or more of the parties, and may point to
                professional scribes who, while being privately hired, also worked closely with a
                bank; on this see <bibl>
                    <author>Yiftach-Firanko</author>
                    <ptr target="gramm:yiftach2008a"/>
                </bibl>.</p>
            <p xml:id="d4_p8">The subjective formulation and the handwritten nature of these
                documents meant that they were legally binding even though they were not produced by
                a notary and did not usually record witnesses; the legality was unaffected when a
                document was written by third parties because of illiteracy, see <bibl>
                    <author>Wolff</author>
                    <ptr target="gramm:wolff1978b"/>
                    <citedRange>107-108</citedRange>
                </bibl>. Querying this last point, see <bibl>
                    <author>Yiftach-Firanko</author>
                    <ptr target="gramm:yiftach2008a"/>
                    <citedRange>326-327</citedRange>
                </bibl>.</p>
            <p xml:id="d4_p9">It was not required for a <term xml:lang="lat">cheirographon</term> to
                be registered in the public archive, but particularly in the case of loan contracts,
                public registration (<term xml:lang="grc">δημοσίωσις</term>) could facilitate the
                future recovery of the debt should there be a default, see for example these
                registration documents with the <term xml:lang="lat">cheirographa</term> embedded
                    [<ref target="https://papyri.info/ddbdp/p.oxy;17;2134r" type="external">17514</ref> l.8-35, 170 CE; <ref target="https://papyri.info/ddbdp/p.oxy;4;719" type="external">20418</ref> l.10-29, 193 CE, both Oxyrhynchus]; on the public
                registration of privately drawn up contracts, see <bibl>
                    <author>Wolff</author>
                    <ptr target="gramm:wolff1978b"/>
                    <citedRange>111-112</citedRange>
                </bibl> and <bibl>
                    <author>Benaissa</author>
                    <ptr target="gramm:benaissa2007"/>
                </bibl>.</p>
            <div xml:id="d4_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d4_p10">The <term xml:lang="lat">cheirographon</term> opens with an
                    address from the issuer to the other party which is the same as that for a
                    letter: <seg xml:id="d4_s1" type="syntax" corresp="../authority_lists.xml#introduction">[from <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                            &lt;dat.&gt;][<term xml:lang="grc">χαίρειν</term>]</seg>. Occasionally a
                    very detailed description of the parties can be added, e.g. [<ref target="https://papyri.info/ddbdp/p.lips;1;12" type="external">31906</ref>
                    l.1-12].</p>
                <p xml:id="d4_p11">This is followed by a subjective statement beginning with <seg xml:id="d4_s2" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ὁμολογῶ</term>]</seg> where the issuer agrees
                    that a certain transaction has taken place: e.g. [<ref type="internal" target="16594.xml" corresp="#d4_s2 #d4_t1">16594</ref>] <term xml:lang="grc" xml:id="d4_t1" corresp="#d4_s2">ὁμολογῶ ἔχειν παρὰ σοῦ... ἀργυρίο̣[υ]
                        σεβαστοῦ νομίσματος δραχμὰς ἑακοσίας</term>
                    <gloss corresp="#d4_t1">I agree that I have had from you 600 imperial silver
                        drachmae</gloss>.</p>
                <p xml:id="d4_p12">Some <term xml:lang="lat">cheirographa</term> display a modified
                    structure and follow the opening address with a subordinate clause beginning
                    with <seg xml:id="d4_s3" type="syntax" corresp="../authority_lists.xml#introduction">[<term xml:lang="grc">ἐπεί</term> / <term xml:lang="grc">ἐπειδή</term>]</seg> and the main
                    clause with <seg xml:id="d4_s4" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ὁμολογῶ</term>]</seg> follows later in the text. The purpose of the
                    subordinate clause is to provide information relevant to the new agreement [<ref type="internal" target="11408.xml" corresp="#d4_s3 #d4_s4">11408</ref> 145
                    CE, Arsinoite nome], or to refer to a previous agreement [<ref type="internal" target="16534.xml" corresp="#d4_s3 #d4_s4">16534</ref> 62 CE, Oxyrhynchus].
                    For a list of documents with this structure see <bibl>
                        <author>Hagedorn</author>
                        <ptr target="gramm:hagedorn1997"/>
                        <citedRange>185</citedRange>
                    </bibl>.</p>
                <p xml:id="d4_p13">A smaller group of <term xml:lang="lat">cheirographa</term>
                    display a variation to the opening address with both parties to the agreement in
                    the nominative, and the addition of <term xml:lang="grc">ἀλλήλοις</term>: <seg xml:id="d4_s5" type="syntax" corresp="../authority_lists.xml#introduction">[<hi rend="italic">name</hi> &lt;nom.&gt;] <term xml:lang="grc">καὶ</term> [<hi rend="italic">name</hi> &lt;nom.&gt;][<term xml:lang="grc">ἀλλήλοις χαίρειν</term>]</seg>, <gloss corresp="#d4_s5">N and
                        N greet one another.</gloss> With this form of address the parties mutually agree to the
                    contents of the document; see <bibl>
                        <author>Jördens</author>
                        <ptr target="gramm:jordens2013"/>
                    </bibl> for an investigation into such <term xml:lang="lat">cheirographa</term>.
                    Contracts with this reciprocal address are used for transactions where the
                    parties are placed on an equal footing, and are mostly found in contracts for
                    the division of property [<ref type="internal" target="10315.xml">10315</ref>
                    297 CE, Karanis], contracts of exchange [<ref target="http://www.papyri.info/ddbdp/p.flor;1;47a" type="external">23560</ref> 217 CE, Hermopolis], and some liturgical [<ref target="http://www.papyri.info/ddbdp/p.col;10;281" type="external">10564</ref>] and partnership agreements [<ref target="http://www.papyri.info/ddbdp/p.koeln;2;101" type="external">21205</ref>] <bibl>
                        <author>Jördens</author>
                        <ptr target="gramm:jordens2013"/>
                        <citedRange>188-189; 190-192</citedRange>
                    </bibl>.</p>
                <p xml:id="d4_p14">In <term xml:lang="lat">cheirographa</term> from the Ptolemaic
                    period the closing salutation <seg xml:id="d4_s6" type="syntax" corresp="../authority_lists.xml#subscription">[<term xml:lang="grc">ἔρρωσο</term>]</seg> was written after the main text, conforming to the
                    structure of a letter, e.g. [<ref type="internal" target="3116.xml" corresp="#d4_s6">3116</ref>; <ref type="internal" target="3746.xml" corresp="#d4_s6">3746</ref>]; this was followed by the date. When the
                    closing salutation eventually disappeared, the date alone served to divide the
                    main text from the subscription (<term xml:lang="grc">ὑπογραφή</term>), e.g.
                        [<ref type="internal" target="15644.xml">15644</ref>]. Occasionally the date
                    appeared both at the beginning and end of the document [<ref type="internal" target="60.xml">60</ref> 136 BCE].</p>
                <p xml:id="d4_p15">The contract ended with a subscription (<term xml:lang="grc">ὑπογραφή</term>), ostensibly written in the hand of the debtor, which
                    provided an acknowledgement or confirmation of the terms of the agreement.</p>
            </div>
            <div xml:id="d4_div2_format" n="2" type="section">
                <head xml:id="format">Format</head>
                <p xml:id="d4_p16">The <term xml:lang="lat">pagina</term> format with horizontal
                    fibres appears to be the favoured orientation for <term xml:lang="lat">cheirographa</term>, e.g. [<ref type="internal" target="7332.xml">7332</ref>; <ref type="internal" target="3116.xml">3116</ref>]. There are
                    some examples <term xml:lang="lat">transversa charta</term> [<ref type="internal" target="20987.xml">20987</ref>] or in this orientation with
                    horizontal fibres [<ref type="internal" target="17030.xml">17030</ref> 105-106
                    CE, Hermopolis]. A roll from Hermopolis with a contract for the sale of a house
                    written out three times [<ref type="internal" target="18722.xml">18722</ref> 271
                    CE] measures H.20 x W.104cm, has several <term xml:lang="lat">kolleseis</term>,
                    and mixed fibre directions. Half of the text of a loan repayment is written on
                    the <term xml:lang="lat">protokollon</term> of a roll [<ref type="internal" target="19956.xml">19956</ref>], the sheet is in <term xml:lang="lat">pagina</term> format. A more squarish sheet records a loan of wheat on a
                    papyrus also partly made with the <term xml:lang="lat">protokollon</term> [<ref type="internal" target="3746.xml">3746</ref>].</p>
                <p xml:id="d4_p17">Some <term xml:lang="lat">cheirographa</term> from Oxyrhynchus
                    have long, narrow formats similar to those used for the [<ref type="descr" target="#gt_pp">private protocol</ref>] type of contract also favoured in
                    Oxyrhynchus during the Roman period, e.g. [<ref target="https://papyri.info/ddbdp/sb;1;5806" type="external">23263</ref> 235
                    CE (H.35 x W.6 cm); <ref type="internal" target="22521.xml">22521</ref> (H.34.5
                    W. 10cm); <ref type="internal" target="15644.xml">15644</ref> (H.35.9 x
                    W.14cm)]. [<ref type="internal" target="140175.xml">140175</ref> 161-162 CE] is
                    also particularly long and narrow (H.36.6 x W.8.5cm) but only the second of (at
                    least) 2 columns survives. Other narrow examples come from Tebtunis [<ref type="internal" target="12147.xml">12147</ref> 45 CE (H.28 x W.8cm)] and
                    Philadelphia [<ref target="https://papyri.info/ddbdp/bgu;7;1649" type="external">9530</ref> 264 CE (H.24 x W.7cm)]. Two copies of a loan, each written in
                    duplicate and on the same day, have the same dimensions [<ref type="internal" target="15650.xml">15650</ref>, <ref type="internal" target="15651.xml">15651</ref> 175 CE, Oxyrhynchus (H.33.9 x 20cm)] suggesting both sheets
                    were cut from the same roll.</p>
            </div>
            <div xml:id="d4_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d4_p18">
                    <term xml:lang="lat">Cheirographa</term> are usually written as a
                    single block of text with little use of <term xml:lang="lat">ekthesis</term> or
                        <term xml:lang="lat">eisthesis</term> [<ref type="internal" target="8875.xml">8875</ref> 159 CE, Arsinoite nome; <ref type="internal" target="9159.xml">9159</ref> ; <ref type="internal" target="17415.xml">17415</ref> 186 CE Oxyrhynchus]. The scribe of [<ref type="internal" target="8908.xml">8908</ref>] writes in a chancery hand and separates <term xml:lang="grc">χαίρειν</term> at the end of the opening address. Sometimes
                    the date can be indented or centred between the main content and the <term xml:lang="grc">ὑπογραφή</term> [<ref type="internal" target="7332.xml">7332</ref>; <ref type="internal" target="22625.xml">22625</ref>], or at the
                    end of the document [<ref type="internal" target="60.xml">60</ref>]. A line can
                    also separate the <term xml:lang="grc">ὑπογραφή</term> from the main content
                        [<ref type="internal" target="60.xml">60</ref>; <ref type="internal" target="15644.xml">15644</ref>]. Wide margins are the distinguishing feature
                    of a contract for the delivery of wheat to a temple [<ref type="internal" target="18652.xml">18652</ref> 18 BCE, Herakleopolite nome], but generally
                    the margins are evenly maintained [<ref type="internal" target="7332.xml">7332</ref>; <ref type="internal" target="15644.xml">15644</ref>]. </p>
                <p xml:id="d4_p19">Often the contract was produced in duplicate, presumably one for
                    each party; this fact could also be pointed out within the contract itself, e.g.
                        [<ref type="internal" target="11408.xml">11408</ref> l.25-26] <term xml:lang="grc">γεγραμμένον δισσόν</term>
                    <gloss>having been written twice</gloss>, [<ref type="internal" target="15361.xml">15361</ref> l.29] <term xml:lang="grc">δισσὴ γραφεῖσα</term>
                    <gloss>written in two (copies)</gloss>. Two copies of a dowry repayment in two different
                    hands are found on two separate sheets, each in a single column, only one
                    complete with the <term xml:lang="grc">ὑπογραφή</term> [<ref type="internal" target="19960.xml">19960</ref> 145 CE]. The duplicate loan agreements [<ref type="internal" target="15650.xml">15650</ref>, <ref type="internal" target="15651.xml">15651</ref>] are written twice on each sheet in a single
                    column, separated by a space. Other copies of contracts are written side by side
                    in two columns on the same sheet [<ref type="internal" target="20335.xml">20335</ref> 225 CE; <ref type="internal" target="17412.xml">17412</ref> 265
                    CE, both Oxyrhynchus]; another papyrus carries three copies of a long contract
                    on a roll [<ref type="internal" target="18722.xml">18722</ref>]. </p>
                <p xml:id="d4_p20">Sometimes there are two different types of document on the same
                    sheet: [<ref type="internal" target="9184.xml">9184</ref> 154 CE, Soknopaiou
                    Nesos] is a <term xml:lang="lat">cheirographon</term> for the sale of a camel -
                    a toll receipt for the same camel is added underneath in a different hand; a
                    copy of an agreement for a loan of money is followed in a second column by a
                    brief letter urging the recipient to chase up the debt [<ref type="internal" target="20540.xml">20540</ref> 57 CE, Oxyrhynchus].</p>
            </div>
        </body>
    </text>
</TEI>