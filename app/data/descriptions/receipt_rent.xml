<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d48">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Rent Receipt (Epistolary Exchange
                    and Objective Statement)</title>
                <author corresp="../authority_lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority_lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority_lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority_lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:bzaqlnvrm5ck7jwuxacc2itrae</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee #gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_ee_receipt #gt_os_receipt"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_ee_rent #gt_os_rent"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority_lists.xml#EN" when="2022-11-24">TEI file created</change>
            <change who="../authority_lists.xml#PS ../authority_lists.xml#SF" when="2022-09-28">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Rent Receipt</head>
            <p xml:id="d48_p1">A rent receipt is evidence of the payment of rent on a previously
                signed lease agreement. This variation of receipt in most cases concerns the rent
                due on land [<ref type="internal" target="10318.xml">10318</ref> 280-281 CE,
                Karanis], or garden land (orchards and allotments) [<ref type="internal" target="10209.xml">10209</ref> 169 CE, Theadelphia], but there are also receipts
                for rent payments on property such as houses [<ref type="internal" target="30255.xml">30255</ref> III-IV CE, Oxyrhynchus], rooms [<ref type="internal" target="9050.xml">9050</ref> II-III CE, Arsinoite nome],
                machinery [<ref target="https://papyri.info/ddbdp/p.laur;1;9" type="external">21238</ref> 167-168 CE] (a water-wheel), and equipment [<ref type="internal" target="9262.xml">9262</ref> 57 CE, Arsinoite nome] (an oil-press). Rent
                receipts for the lease of vineyards are not usually found as these agreements were
                generally based on sharecropping or a wage payment <bibl>
                    <author>Rowlandson</author>
                    <ptr target="gramm:rowlandson1996"/>
                    <citedRange>229, 234</citedRange>
                </bibl>.</p>
            <p xml:id="d48_p2">Different types of rental arrangements are reflected in the variable
                terminology used for the rent payments. The basic term for rent <seg xml:id="d48_s1" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ὁ
                        φόρος</term>]</seg> usually meant an amount paid in money and this was
                generally the medium of payment for fodder crops or perishable goods, e.g. [<ref type="internal" target="10209.xml" corresp="#d48_s1">10209</ref>]. Another term
                    <seg xml:id="d48_s2" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">τὸ ἐκφόριον</term>]</seg> indicated a rent payment
                in-kind – this was usually a predetermined amount of wheat for cereal crops, e.g.
                    [<ref type="internal" target="10318.xml" corresp="#d48_s2">10318</ref>] <bibl>
                    <author>Herrmann</author>
                    <ptr target="gramm:herrmann1958"/>
                    <citedRange>99</citedRange>
                </bibl>, <bibl>
                    <author>Rowlandson</author>
                    <ptr target="gramm:rowlandson1996"/>
                    <citedRange>240-243</citedRange>
                </bibl>. A rent payment could also be a combination of both depending on the crops
                produced, or where the land leased was used for mixed cultivation, e.g. [<ref type="internal" target="9437.xml" corresp="#d48_s1 #d48_s2">9437</ref> II CE,
                Theadelphia], a mix of grain and fodder, with rent paid in grain and cash. The term
                for the rent paid on a house or room is <term xml:lang="grc">τὸ ἐνοίκιον</term>.
                Also found is <seg xml:id="d48_s3" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἀπότακτον</term>]</seg> signifying a fixed rent in money [<ref type="internal" target="11882.xml" corresp="#d48_s3">11882</ref> 182-183 CE,
                Arsinoite nome].</p>
            <p xml:id="d48_p3">Some rent receipts indicate payment by instalments <term xml:lang="grc">ὑπὲρ φόρου ἐπὶ λόγου</term> [<ref type="internal" target="12279.xml">12279</ref> 177 CE, Arsinoite nome], <term xml:lang="grc">ὑπὲρ ἐκφορίου ἐπὶ λόγου</term> [<ref type="internal" target="20965.xml">20965</ref> 156 CE], lit. a payment <gloss>on account</gloss>. This corresponds
                to the stipulations in some lease agreements, e.g. [<ref target="https://papyri.info/ddbdp/p.hamb;1;5" type="external">11396</ref> 89 CE,
                Philadelphia] <term xml:lang="grc">ἐν ἀναφοραῖς δυσὶ μηνὶ Φαῶφι</term>, rent
                    <gloss>in two instalments in the month of Phaophi</gloss>.</p>
            <p xml:id="d48_p4">Two part-payments were made by two people on behalf of a village, for
                pasture land, formerly crown land (<term xml:lang="lat">ousiac</term> land), in
                    [<ref type="internal" target="9698.xml">9698</ref> 208 CE, Soknopaiou Nesos],
                and also [<ref type="internal" target="15047.xml">15047</ref> 194 CE, Soknopaiou
                Nesos]. </p>
            <p xml:id="d48_p5">A payment of rent made in advance is variously expressed as [<term xml:lang="grc">ἀναφόριον</term>] [<ref type="internal" target="11437.xml">11437</ref> 107 CE, Tebtunis] or <term xml:lang="grc">ἐν προδόματι τὸν
                    φόρον</term>
                <gloss>the rent in advance</gloss> [<ref target="https://papyri.info/ddbdp/p.soter;;7" type="external">13148</ref> 91 CE,
                Theadelphia].</p>
            <p xml:id="d48_p6">Rent receipts often refer to payments for the period of one year
                    [<ref target="https://papyri.info/ddbdp/bgu;11;2040" type="external">9574</ref>
                223 CE, Theadelphia] but are also found for longer periods [<ref type="internal" target="9437.xml">9437</ref>], a payment for two years; [<ref target="9035.xml" type="internal">9035</ref> 147-151 CE, Arsinoite nome] lists payments over a
                period of 5 years and part of a 6<hi rend="superscript">th</hi> year, three years of
                which show payments by monthly instalments. Five separate rent receipts were issued
                by the landowner Haryotes to the same tenant Horos, in almost consecutive years,
                    [<ref type="internal" target="11993.xml">11993</ref>, <ref type="internal" target="11994.xml">11994</ref>, <ref type="external" target="https://papyri.info/ddbdp/p.mich;3;197">11995</ref>, <ref type="internal" target="11996.xml">11996</ref>, <ref type="internal" target="11997.xml">11997</ref> 121-126 CE, Bacchias], and
                pertain to the same lease agreement [<ref type="internal" target="11987.xml">11987</ref> II CE, Bacchias].</p>
            <p xml:id="d48_p7">Rent receipts are categorised under both Epistolary Exchange and
                Objective Statement; the former tend to concern private lease agreements, while the
                latter usually concern the rent payments due on the use of public land. The payment
                of rent in-kind for private agreements usually took place at the local threshing
                floor <bibl>
                    <author>Herrmann</author>
                    <ptr target="gramm:herrmann1958"/>
                    <citedRange>109-111</citedRange>
                </bibl>, <bibl>
                    <author>Rowlandson</author>
                    <ptr target="gramm:rowlandson1996"/>
                    <citedRange>219-220</citedRange>
                </bibl>. Rent payments due on public land were facilitated through the granaries,
                e.g. [<ref type="internal" target="13525.xml">13525</ref> 265 CE, Tebtunis] records
                the rent paid in kind by a representative of the village of Tebtunis, through the
                granary at Tebtunis, to the official in charge of the tenancy, for the use of public
                land. Rent receipts for the use of public land could also be issued by a sitologos
                    [<ref target="https://papyri.info/ddbdp/sb;6;9433" type="external">14231</ref>
                163 CE, Karanis].</p>
            <p xml:id="d48_p8">While a variation of receipt specifically concerning rent payments is
                easily discernible, rent receipts are also found which have been constructed as more
                formal documents. A rent receipt from Oxyrhynchus is written as a [<ref type="descr" target="#gt_cheiro">cheirographon</ref>], [<ref target="https://papyri.info/ddbdp/sb;1;5806" type="external">23263</ref> 235 CE]
                on a long and narrow sheet rather like the [<ref target="#gt_pp" type="descr">private protocol</ref>] contracts from the same area. Rent payments are also
                recorded as a [<ref type="descr" target="#gt_syngr">syngraphe</ref>] in the form of
                a [<ref target="#gt_doubleDoc" type="descr">double document</ref>] e.g. [<ref type="internal" target="2063.xml">2063</ref> 249-248 BCE; <ref type="internal" target="2065.xml">2065</ref> 248-247 BCE; <ref type="internal" target="1765.xml">1765</ref> 252 BCE, all Philadelphia], or as an [<ref type="descr" target="#gt_syngr">objective homologia<ptr target="#d42_p14"/>
                </ref>], the
                receipts from Haryotes to Horos. Some rent receipts can be issued through a bank,
                e.g. [<ref type="internal" target="10938.xml">10938</ref> 143 CE, Theadelphia]
                indicates that payment of rent on an oil-press was paid in kind with 5 measures of
                oil.</p>
            <p xml:id="d48_p9">Studies on lease agreements invariably include references to types of
                rent payments and passing references to receipts, cf. <bibl>
                    <author>Herrmann</author>
                    <ptr target="gramm:herrmann1958"/>
                    <citedRange>98-114</citedRange>
                </bibl> and more specifically for tenancy in the Oxyrhynchite nome <bibl>
                    <author>Rowlandson</author>
                    <ptr target="gramm:rowlandson1996"/>
                    <citedRange>202-279, particularly 236-252</citedRange>
                </bibl>.</p>
            <div xml:id="d48_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d48_p10">Rent receipts under Epistolary Exchange have the standard
                    opening address <seg xml:id="d48_s4" type="syntax" corresp="../authority_lists.xml#introduction" datcat="#gt_ee #gt_ee_receipt #gt_ee_rent">[from <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                            &lt;dat.&gt;][<term xml:lang="grc">χαίρειν</term>]</seg>, the receipt
                    being issued by the landlord to the tenant.</p>
                <p xml:id="d48_p11">An unusual opening address appears in [<ref type="internal" target="9050.xml" corresp="#d48_s5">9050</ref>] with a simple <seg xml:id="d48_s5" type="syntax" corresp="../authority_lists.xml#introduction" datcat="#gt_ee #gt_ee_receipt #gt_ee_rent">[from <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                        &lt;dat.&gt;]</seg> and no [<term xml:lang="grc">χαίρειν</term>].</p>
                <p xml:id="d48_p12">The main statement is based around <term xml:lang="grc">ἔχω</term>, usually <seg xml:id="d48_s6" type="syntax" corresp="../authority_lists.xml#main-text" datcat="#gt_ee #gt_ee_receipt #gt_ee_rent">[<term xml:lang="grc">ἔχω παρά
                            σου</term>…]</seg>
                    <gloss corresp="#d48_s6">I receive from you…</gloss> [<ref type="internal" target="4311.xml" corresp="#d48_s6">4311</ref> 165 BCE, Arsinoite nome],
                        <term xml:lang="grc" xml:id="d48_t1">ἔσχον</term> [<ref type="internal" target="9437.xml" corresp="#d48_t1 #d48_s6">9437</ref>], <term xml:lang="grc" xml:id="d48_t2">ἀπέχω</term> [<ref type="internal" target="9262.xml" corresp="#d48_t2 #d48_s6">9262</ref>], and follows with
                    the type of rent, in money (<term xml:lang="grc">ὁ φόρος</term>) or in-kind
                        (<term xml:lang="grc">τὸ ἐκφόριον</term>), the facility being rented, and
                    the amount being paid. An amount may not always be mentioned [<ref type="internal" target="9262.xml">9262</ref>].</p>
                <p xml:id="d48_p13">Some private rent receipts also use the verb <seg xml:id="d48_s7" type="syntax" corresp="../authority_lists.xml#main-text" datcat="#gt_ee #gt_ee_receipt #gt_ee_rent">[<term xml:lang="grc">μεμέτρημαι</term>]</seg>, e.g. [<ref type="internal" target="17053.xml" corresp="#d48_s7 #d48_t3">17053</ref> 125 CE, Hermopolite nome] <term xml:lang="grc" xml:id="d48_t3" corresp="#d48_s7">μεμέτρημαι παρὰ σοῦ</term>
                    <gloss corresp="#d48_t3">I have had measured by you</gloss> – this is the same
                    verb used in [<ref type="descr" target="#gt_sitologos">sitologos receipts<ptr target="#d40_p6"/>
                    </ref>], but these private rent receipts
                    are not issued by the <term xml:lang="lat">sitologoi</term>.</p>
                <p xml:id="d48_p14">The <seg xml:id="d48_s8" type="syntax" corresp="../authority_lists.xml#date" datcat="#gt_ee #gt_ee_receipt #gt_ee_rent">[date@end]</seg> may follow but
                    is not always present. A <seg xml:id="d48_s9" type="syntax" corresp="../authority_lists.xml#subscription" datcat="#gt_ee #gt_ee_receipt #gt_ee_rent">closing salutation</seg> may be
                    added [<ref type="internal" target="2074.xml" corresp="#d48_s9">2074</ref>
                    243-242 BCE, Philadelphia; <ref type="internal" target="4311.xml" corresp="#d48_s9 #d48_s8">4311</ref> 165 BCE, Arsinoite nome; <ref type="internal" target="8082.xml" corresp="#d48_s9 #d48_s8">8082</ref> 165
                    BCE Soknopaiou Nesos].</p>
                <p xml:id="d48_p15">Rent receipts in the form of a <term xml:lang="lat">cheirographon</term> follow the typical construction [<term xml:lang="grc">ὁμολογῶ ἔχειν</term>] and may also carry a closing salutation [<ref type="internal" target="8082.xml">8082</ref> 165 BCE, Soknopaiou Nesos].</p>
                <p xml:id="d48_p16">Rent receipts under Objective Statement, as they are issued by
                    an official, are usually objectively styled and have a <seg xml:id="d48_s10" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#date">[date@start]</seg>. The main statement
                    can begin with <seg xml:id="d48_s11" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἔχει</term>]</seg> [<ref type="internal" target="8236.xml" corresp="#d48_s11 #d48_s10">8236</ref> 261-260, Hibeh], where the official
                    states that he receives the rent payment, or with <seg xml:id="d48_s12" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">παρέσχον</term>]</seg> e.g. [<ref type="internal" target="9698.xml" corresp="#d48_s12 #d48_t4">9698</ref> 208 CE, Soknopaiou Nesos], <term xml:lang="grc" xml:id="d48_t4" corresp="#d48_s12">παρέσχον
                        πρ̣εσβύτεροι</term>, where <gloss corresp="#d48_t4">the village elders have
                        provided</gloss> the payment. The rent payment through a bank has <seg xml:id="d48_s13" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἀπέχειν</term>]</seg> [<ref type="internal" target="10938.xml" corresp="#d48_s13">10938</ref>].</p>
                <p xml:id="d48_p17">Also found is the construction <seg xml:id="d48_s14" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">διέγραψεν</term> to <hi rend="italic">name</hi> &lt;dat.&gt;] [by <hi rend="italic">name</hi> &lt;nom.&gt;]</seg>
                    <gloss corresp="#d48_s14">paid to <hi rend="italic">N</hi>, by <hi rend="italic">N</hi>
                    </gloss> [<ref type="internal" target="11882.xml" corresp="#d48_s14">11882</ref> 183 CE, Arsinoite nome].</p>
                <p xml:id="d48_p18">Some rent receipts under Objective Statement also use the verb
                        <seg xml:id="d48_s15" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">μεμέτρημαι</term>]</seg>, but again these are not issued by a sitologos
                        [<ref type="internal" target="13525.xml" corresp="#d48_s15">13525</ref> 265
                    CE, Tebtunis].</p>
                <p xml:id="d48_p19">The main statement includes the type of rent (in money or
                    in-kind), the facility being rented, and the amount being paid. In most cases
                    there is nothing further added, but occasionally there may be a <seg xml:id="d48_s16" type="syntax" datcat="#gt_os #gt_os_receipt #gt_os_rent" corresp="../authority_lists.xml#subscription">[subscription]</seg> e.g.
                        [<ref type="internal" target="13525.xml" corresp="#d48_s16">13525</ref>].</p>
            </div>
            <div xml:id="d48_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d48_p20">All shapes of papyri are found: <term xml:lang="lat">pagina</term> format with horizontal fibres [<ref type="internal" target="4311.xml">4311</ref>; <ref type="internal" target="11413.xml">11413</ref>], and vertical fibres [<ref type="internal" target="13126.xml">13126</ref> 83 CE, Theadelphia; <ref type="internal" target="20078.xml">20078</ref> 286 CE]; <term xml:lang="lat">transversa charta</term> [<ref type="internal" target="2074.xml">2074</ref>, <ref type="internal" target="9437.xml">9437</ref>], and this orientation with
                    horizontal fibres [<ref type="internal" target="17046.xml">17046</ref> 90 CE,
                    Hermopolite nome]; and squarish with horizontal fibres [<ref type="internal" target="15944.xml">15944</ref> 250 CE, Oxyrhynchus] and vertical fibres [<ref type="internal" target="14287.xml">14287</ref> 209 Euhemeria]. In one example a list of rent payments is
                    written in a single column on horizontal fibres to which is added on the right a
                    sheet with vertical fibres [<ref type="internal" target="9035.xml">9035</ref>] </p>
                <p xml:id="d48_p21">Many rent receipts are found on small slips of papyrus [<ref type="internal" target="30255.xml">30255</ref> (H. 6 x W. 8 cm), <ref type="internal" target="14287.xml">14287</ref> (H. 7.8 x W. 7.3 cm), <ref type="internal" target="13796.xml">13796</ref> 152 CE, Theadelphia (H. 10.2
                    x W. 8.5 cm)]. Some are quite long [<ref type="internal" target="4311.xml">4311</ref> (H. 30.1 x W. 10.3 cm)], or long and narrow [<ref type="internal" target="13125.xml">13125</ref> (H. 24 x W. 6.7 cm). The
                    double document [<ref type="internal" target="2063.xml">2063</ref>] still
                    retains the seal.</p>
            </div>
            <div xml:id="d48_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d48_p22">The receipt is usually written as a single block of text [<ref type="internal" target="13126.xml">13126</ref>; <ref type="internal" target="14287.xml">14287</ref>] sometimes with an enlarged first letter
                        [<ref type="internal" target="9262.xml">9262</ref>; <ref type="internal" target="9698.xml">9698</ref>], and occasionally with the final line indented
                        [<ref type="internal" target="11275.xml">11275</ref> 90 CE, Arsinoite nome].
                    The greeting may be indented [<ref type="internal" target="9437.xml">9437</ref>]
                    - this same receipt has a line drawn between the two payments. There can be
                    large top and bottom margins [<ref type="internal" target="14363.xml">14363</ref> 151 CE, Theadelphia; <ref type="internal" target="20078.xml">20078</ref>]. Another receipt carries some line end fillers [<ref type="internal" target="12279.xml">12279</ref>], while [<ref type="internal" target="17053.xml">17053</ref>] ends the text with a series of crosses. The
                    text can be written uniformly [<ref type="internal" target="17046.xml">17046</ref>], or can be more cursive [<ref type="internal" target="10442.xml">10442</ref> 53 CE, Arsinoite nome]; the second hands in
                        [<ref type="internal" target="13132.xml">13132</ref> 127-128 CE,
                Theadelphia] and [<ref type="external" target="https://papyri.info/ddbdp/p.soter;;21">13136</ref> 133 CE,
                    Theadelphia] are clearly those of a slow writer.</p>
                <p xml:id="d48_p23">Some papyri carry more than one receipt, e.g. [<ref type="internal" target="20078.xml">20078</ref>] has two receipts for
                    different payments made to two different recipients, with a <term xml:lang="lat">vacat</term> between the two. Another receipt with two payments has the
                    start of the second payment written in <term xml:lang="lat">ekthesis</term> to
                    the first [<ref type="internal" target="9698.xml">9698</ref>].</p>
            <p xml:id="d48_p24">One document [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;62;4336">21634</ref> 169-171 CE, Oxyrhynchus] carries a collection of receipts
                    regarding the land of Julius Theon, a former office holder in Alexandria. The
                    text is written out on a single sheet over three columns with <term xml:lang="lat">vacat</term>s between each entry. </p>
                <p xml:id="d48_p25">Another rent receipt is to be found added to the end of a five
                    column list of rent payments [<ref target="https://papyri.info/ddbdp/p.sarap;;38" type="external">17055</ref>
                    128 CE, Hermopolite nome].</p>
                <p xml:id="d48_p26">A text written in red ink concerning a rent payment is not
                    formulated as a typical receipt and may in fact be a simple note of payment
                        [<ref target="https://papyri.info/ddbdp/sb;20;14237" type="external">29480</ref> II-III CE, Karanis].</p>
            </div>
        </body>
    </text>
</TEI>