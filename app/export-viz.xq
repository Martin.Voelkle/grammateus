xquery version "3.1";

import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace show="http://grammateus.ch/show" at "modules/show.xql";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace functx="http://www.functx.com";


declare namespace tei = "http://www.tei-c.org/ns/1.0";

(: Export the papyri visualizations progressively as they are checked :)

(: output folder :)
let $out := "/db/apps/grammateus-archive/visualizations/"

let $css := '
/*------------------------------
* Generic styles
* ------------------------------ 
*/
/* color scheme */
:root {
    --main-bg-color: cadetblue; /* header/footer */ /*d7e8cb e4e6e1 current:a5ae9d*/
    --light-bg-color: #edeeeb; /* card bg, advanced search*/
    --gramm-logo-color: #c00000; /* grammateus red */
    --text-title-color: rgba(0, 0, 0, 0.65); /* same as sidebar links. Before: dark green #52574e*/
    --papyri-info: #162a5c; /* papyri-info links? dark blue */
    --fibres-dark-color: rgba(225, 207, 183, 0.5); /* darker beige #e1cfb7 */
    --fibres-light-color: rgba(234, 222, 204, 0.5); /* lighter beige #eadecc */
    --papyrus-abstract-color: rgba(255, 215, 0, 0.75); /* gold */
    --papyrus-addressee-color: rgba(255, 167, 0, 0.75); /* orange */
    --papyrus-salutation-color: rgba(246, 207, 113, 0.75); /* yellowish (plotly pastel) */
    --papyrus-heading-color: rgba(135, 233, 250, 0.75); /* LightSkyBlue towards Cyan */
    --papyrus-mark-color: rgba(135, 233, 250, 0.75); /* LightSkyBlue towards Cyan */
    --papyrus-intro-color: rgba(255, 99, 71, 0.75); /* red/tomato */
    --papyrus-main-color: rgba(211, 211, 211, 0.75); /* light gray */
    --papyrus-subscription-color: rgba(147,112,219, 0.75); /* medium purple */
    --papyrus-addition-color: rgba(255, 167, 0, 0.75); /* orange */
    --papyrus-date-color: rgba(102, 205, 170, 0.75); /* medium aquamarine */
    --papyrus-descr-cont-color: rgba(136, 204, 238, 0.75);/* blue (plotly safe) */
    --papyrus-clause-color: rgba(229, 196, 148, 0.75);/* brown (plotly set2) */
    --papyrus-col-color: rgba(252, 205, 229, 0.75);/* pink (plotly set3) */
    --papyrus-signature-color: rgba(219, 183, 112, 0.75);/* ~beige */
    --search-color: #cd5c5c; /* red/cherry */
}
body {
    font-family: Merriweather;
    /* sticky footer */
    display: flex;
    min-height: 100vh;
    flex-direction: column;
}
.h1 {
    padding-top: 16px; /* Bootstrap 4 has removed space btw header and content */
    padding-bottom: 16px;
    color: var(--text-title-color);
    font-family: Open Sans;
}
/*---------- SHOW Page ----------*/
/* general */
#text-viz, #image-viz {
    margin-bottom: 20px;
}
/* legend for text section and search parameters from browse page */
.legend {
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    padding: 0px;
}
.legend li {
    display: flex;
    align-items: center;
    margin-right: 15px;
    /*margin: 0 10px;*/
}
.legend span {
    float: left;
    width: 20px;
    height: 20px;
    margin: 5px;
}
/* SVG display of papyri */
/* remove display none from svg */
#svg {
    display: inherit!important;
}
/* ------------------- */
/* background */
.fv {
  color: black;
  background: repeating-linear-gradient(
    90deg,
    var(--fibres-dark-color),
    var(--fibres-dark-color) 5px,
    var(--fibres-light-color) 5px,
    var(--fibres-light-color) 10px
  );
}
.fh {
  color: black;
  background: repeating-linear-gradient(
    0deg,
    var(--fibres-dark-color),
    var(--fibres-dark-color) 5px,
    var(--fibres-light-color) 5px,
    var(--fibres-light-color) 10px
  );
}
.no-fibres {
  color: black;
  background: #eadecc;
}

/* ------------------- */
/* styling epidoc html */
#edition {
    display: flex; /* display textparts as columns */
}
#text-content h1 {
    display: none; /* hide title */
}
#text-content h2 {
    display: none; /* hide date/provenance */
}
#commentary {
    display: none; /* hide commentary */
}
.textpartnumber {
    display: none; /* hide column nb */
}
.linenumber {
    display: none; /* hide line numbers */
}
/* hide verso (textpart following a span that has 'abv' in its @id. See also TM7769 */
#edition span[id*='abv'] + .textpart {
    display: none; /* causes problems for 219272, since there are columns named abv, abvi, abvii... */
}
#abv + .textpart {
    display: none; /* hide verso */
}
#abv\,ctr + .textpart {
    display: none; /* hide verso, center text */
}
#abv\,m + .textpart {
    display: none; /* hide verso, margin text */
}
#abv\,md + .textpart {
    display: none; /* hide verso, right margin text */
}
#abv\,1 + .textpart {
    display: none; /* hide verso, textpart 1 */
}
#abv\,2 + .textpart {
    display: none; /* hide verso, textpart 2 */
}
#textpart-abv-i + .textpart {
    display: none; /* hide verso, col i, tm 4126 */
}
#textpart-abv-ii + .textpart {
    display: none; /* hide verso, col ii, tm 4126 */
}
#license {
    display: none; /* hide license */
}

/* ------------------- */
/* text sections */
#text-content {
    /*display: flex;*/ /* prevents SVG from overflowing. not necessary if using js to resize svg */
    font-family: Literata, serif; /* prevent from using sans-serif (bigger) font 
    /*white-space: nowrap;  prevent automatic linebreak. PROBLEM: lines become too long */
    /*line-height: 1.5;*/
}

/* makes line fully highlighted with color (and not only the text) */
#edition span.section {
    display: inline-block; /* causes problem when lb is inside <span> (e.g. if it's a number, cf 28272 line 6) */
}
.abstract {
    background-color: var(--papyrus-abstract-color);
}
.addressee {
    background-color: var(--papyrus-addressee-color);
}
.salutation {
    background-color: var(--papyrus-salutation-color);
}
.heading{
    background-color: var(--papyrus-heading-color);
}
.official-mark{
    background-color: var(--papyrus-mark-color);
}
.introduction {
    background-color: var(--papyrus-intro-color);
}
.main-text {
    background-color: var(--papyrus-main-color);
}
.subscription {
    background-color: var(--papyrus-subscription-color);
}
.date {
    background-color: var(--papyrus-date-color);
}
.descr-contents {
    background-color: var(--papyrus-descr-cont-color);
}
.trans-clause {
    background-color: var(--papyrus-clause-color);
}
.col-no {
    background-color: var(--papyrus-col-color);
}
.signature {
    background-color: var(--papyrus-signature-color);
}
.addition {
    background-color: var(--papyrus-addition-color);
}
/* two sections */
.date.introduction {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-intro-color));
}
.date.trans-clause {
     background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-clause-color));
}
.descr-contents.trans-clause {
    background: linear-gradient(to right, var(--papyrus-descr-cont-color), var(--papyrus-clause-color));
}
.introduction.main-text {
    background: linear-gradient(to right, var(--papyrus-intro-color), var(--papyrus-main-color));
}
.main-text.subscription {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-subscription-color));
}
.salutation.date {
    background: linear-gradient(to right, var(--papyrus-salutation-color), var(--papyrus-date-color));
}
.salutation.subscription {
    background: linear-gradient(to right, var(--papyrus-salutation-color), var(--papyrus-subscription-color));
}
.heading.introduction {
    background: linear-gradient(to right, var(--papyrus-heading-color), var(--papyrus-intro-color));
}
.heading.main-text {
    background: linear-gradient(to right, var(--papyrus-heading-color), var(--papyrus-main-color));
}
.heading.date {
    background: linear-gradient(to right, var(--papyrus-heading-color), var(--papyrus-date-color));
}
.trans-clause.main-text {
     background: linear-gradient(to right, var(--papyrus-clause-color), var(--papyrus-main-color));
}
span.descr-contents ~ .descr-contents.main-text {
    background: linear-gradient(to right, var(--papyrus-descr-cont-color), var(--papyrus-main-color));
}
span.main-text ~ .main-text.descr-contents {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-descr-cont-color));
}
span.date ~ .date.introduction {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-intro-color));
}
span.introduction ~ .introduction.date {
    background: linear-gradient(to right, var(--papyrus-intro-color), var(--papyrus-date-color));
}
span[class*="official-mark introduction"] {
    background: linear-gradient(to right, var(--papyrus-mark-color), var(--papyrus-intro-color));
}
span[class*="descr-contents introduction"] {
    background: linear-gradient(to right, var(--papyrus-descr-cont-color), var(--papyrus-intro-color));
}
span[class*="introduction descr-contents"] {
    background: linear-gradient(to right, var(--papyrus-intro-color), var(--papyrus-descr-cont-color));
}
span[class*="descr-contents date"] {
    background: linear-gradient(to right, var(--papyrus-descr-cont-color), var(--papyrus-date-color));
}
span[class*="date descr-contents"] {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-descr-cont-color));
}
span[class*="date addressee"] {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-addressee-color));
}
span[class*="date main-text"] {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-main-color));
}
span[class*="main-text date"] {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-date-color));
}
span[class*="date subscription"] {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-subscription-color));
}
span[class*="subscription date"] {
    background: linear-gradient(to right, var(--papyrus-subscription-color), var(--papyrus-date-color));
}
span.date ~ .date.heading {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-heading-color));
}
span.main-text ~ .main-text.salutation {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-salutation-color));
}
span[class*="addition date"] {
    background: linear-gradient(to right, var(--papyrus-addition-color), var(--papyrus-date-color));
}
span[class*="date addition"] {
    background: linear-gradient(to right, var(--papyrus-date-color), var(--papyrus-addition-color));
}
span[class*="main-text addition"] {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-addition-color));
}
span[class*="addition main-text"] {
    background: linear-gradient(to right, var(--papyrus-addition-color), var(--papyrus-main-color));
}
span[class*="subscription addition"] {
    background: linear-gradient(to right, var(--papyrus-subscription-color), var(--papyrus-addition-color));
}
span[class*="addition subscription"] {
    background: linear-gradient(to right, var(--papyrus-addition-color), var(--papyrus-subscription-color));
}
span[class*="addition descr-content"] {
    background: linear-gradient(to right, var(--papyrus-addition-color), var(--papyrus-descr-cont-color));
}
span[class*="descr-content addition"] {
    background: linear-gradient(to right, var(--papyrus-descr-cont-color), var(--papyrus-addition-color));
}
span[class*="introduction addition"] {
    background: linear-gradient(to right, var(--papyrus-introduction-color), var(--papyrus-addition-color));
}
span[class*="addition introduction"] {
    background: linear-gradient(to right, var(--papyrus-addition-color), var(--papyrus-introduction-color));
}

/* three sections */
span[class*="maint-text salutation date"] {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-salutation-color), var(--papyrus-date-color));
}
span[class*="maint-text addition date"] {
    background: linear-gradient(to right, var(--papyrus-main-color), var(--papyrus-addition-color), var(--papyrus-date-color));
}
span[class*="heading date introduction"] {
    background: linear-gradient(to right, var(--papyrus-heading-color), var(--papyrus-date-color), var(--papyrus-intro-color)) !important;
}


/* subscription inside date (TM 15112)? does not seem possible in css */

/* TODO: special case - date embedded in subscription (TM14355)? */
/* vacat */
#edition br.vacat + span {
    display: none; /* hide text and line color background */
}'


(: first batch :)
let $export_1 := collection($g:papyri)//tei:TEI[descendant::tei:catRef[@target eq '#gt_rec']] |
                collection($g:papyri)//tei:TEI[descendant::tei:catRef[@target eq '#gt_condolence']]
                
let $export_2 := collection($g:papyri)//tei:TEI[descendant::tei:catRef[@target eq '#gt_letterBusiness']]|
                collection($g:papyri)//tei:TEI[descendant::tei:catRef[@target eq '#gt_calf']] |
                collection($g:papyri)//tei:TEI[descendant::tei:catRef[@target eq '#gt_telos']]

for $p in $export_2

let $tm := gramm:tm($p)

let $title := gramm:title($p)

(: fibres :)
    let $fibres := functx:substring-after-last($p//tei:support/@ana/string(), '#')

    (: shape :)
    let $w := $p//tei:width
    let $h := $p//tei:height

(: text content :)
    let $text_xml := gramm:doc-expand-text($p) (: papyri.info xml :)
    let $text_html := show:epidoc($text_xml) (: epidoc html :)
    let $sections := $p//tei:ab[@type eq 'section']
    let $text_sections := show:text-content($sections, $text_html) (: text sections bg color :)
    
(: text container :)
    (: column number problem: texpart is not always a column, e.g. double documents
        But column nb is also not always exact, e.g. margin text has to be counted as a column for the display :)
    let $columns_real := number($p//tei:layout/@columns/string())
    let $columns := count($text_sections//div[@id eq 'edition']/div[@class eq "textpart"][not(preceding-sibling::span[matches(@id, 'v')])])
    (: +1 => 1st line is <a> instead of <br>
        we do not count lines on the verso 
        original=> count($text_sections//div[@id eq 'edition']//br[not(parent::div[preceding-sibling::span[contains(@id, 'v')]])])+1 
        now trying to count only the text that is displayed and has a text section:)
    let $lines := count($text_sections//span[@class eq "section"])
    
    (: max nb of lines for each column, from html:)
    (:TODO: use column_real, and a new column variable to add in matrices, to know how many actual text column need to be dislpayed, that include possible margin text etc. problem = if some textpart are part of same column?:)
    let $max_lines_textpart :=
        for $textpart in $text_sections//div[@id eq 'edition']//div[@class eq 'textpart']
        let $l := count($textpart//br[not(parent::div[preceding-sibling::span[matches(@id, 'v')]])])+1
(:        let $l := count($textpart//span[contains(@class, "section")]):)
        order by $l descending
        return $l
    
    (:ADD MARGIN AS COLUMNS:)
    let $max_lines := 
        if($columns_real eq 1) 
        then sum($max_lines_textpart) 
(:        else if (count($max_lines_textpart) ne $columns_real):)
(:        then console:log("problem with column number and textpart"):)
        else $max_lines_textpart

let $html := 
    <html>
        <head>
            <meta charset="utf-8"/>
            <meta name="description" content="grammateus. The Architecture of Documentary Papyri"/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <title>TM {$tm/string()} | {$title}</title>
            <style>{$css}</style>
        </head>
        <body>
            <div class="col">
                <div class="page-header">
                    <h1 class="h1">
                        {$title}
                    </h1>
                <hr/>
                </div>
                <div class="row">
                    <div class="col">
                        <ul class="legend">
                        <!-- list of text sections -->
                        {
                            let $auth := doc($g:authority)
                            for $section in distinct-values($sections/tei:locus/@ana/string())
                            let $id := functx:substring-after-last($section, '#')
                            let $name := $auth//id($id)/tei:label/string()
                            return
                                <li><span class="{$id}"></span> {$name}</li>
                        }
                        </ul>
                    </div>
                </div>
                <div class="row" id="papyrus-viz">
                    <div class="col" id="text-viz">
                        <div id="text-container">
                            <div id="text-content">
                                {show:svg($w, $h, $text_sections/div, $fibres, $max_lines, $columns)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </html>


return xmldb:store($out, $tm||'.html', $html)