xquery version "3.1";

module namespace error="http://grammateus.ch/pages/error";


import module namespace home="http://grammateus.ch/pages/home" at "home.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "../gramm.xql";
import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace console = "http://exist-db.org/xquery/console";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace functx = "http://www.functx.com";

declare function error:page($resource, $exception) {
<div>
    {
        switch ($exception)
        case "nodoc" return error:nodoc($resource)
        case "nodescr" return error:nodescr($resource)
        case "noclass" return error:noclass($resource)
        default return error:nopage()
    }
    {home:page()}
</div>
};

declare function error:nodoc($tm) {
    <div class="alert alert-danger" id="alert-home" role="alert">
        <i class="fas fa-exclamation-circle"/>
        The document "{$tm}" is not available in the database.<br/>
        Please search the list of existing <a href="{$g:root}papyri.html">papyri</a>.
        </div>
};

declare function error:nodescr($doc) {
    <div class="alert alert-danger" id="alert-home" role="alert">
        <i class="fas fa-exclamation-circle"/>
        The document "{$doc}" is not available in the descriptions.<br/>
        Check the list of existing <a href="{$g:root}descriptions">descriptions</a>.
        </div>
};

declare function error:noclass($doc) {
    <div class="alert alert-danger" id="alert-home" role="alert">
        <i class="fas fa-exclamation-circle"/>
        The document "{$doc}" is not available in the classification.<br/>
        Check the list of existing <a href="{$g:root}classification">classification</a> pages.
        </div>
};

declare function error:nopage() {
    <div class="alert alert-danger" id="alert-home" role="alert">
        <i class="fas fa-exclamation-circle"/>
        The page you have requested does not exist.<br/>
        You have been redirected to the home page.
        </div>
};