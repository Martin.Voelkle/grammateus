#!/usr/bin/env python
# coding: utf-8

# import modules
import csv # work with tab separated text matrices
import re # regular expressions
from datetime import date
import sys # get arguments
import os # manipulate file paths
import glob

# ## Variables

# tsv matrices (local)
matrices_txt_from = "../matrices/textfiles"
# folder to save the collections
collections_to = "../data/iiif/"
defaultfolder = matrices_txt_from
# list of manifests? or dict?
manifests = []

# ID urn for iiif collection



# ## Functions

# take as argument a path to a folder with matrices
def get_folder_path():
    try:
        folder_path = os.path.normpath(sys.argv[1])
        return folder_path
    except (TypeError, IndexError):
        print("No folder path has been included. Using the default folder: "+defaultfolder)
        return defaultfolder

# get list of files in the folder as absolute paths
def get_files_path(folder_path):
    try:
        files_path = [os.path.join(folder_path, x) for x in os.listdir(folder_path)]
        # check if the folder is empty
        if len(files_path) == 0:
            sys.exit("There are no matrices to convert to epidoc.")
        return files_path
    except FileNotFoundError:
        sys.exit("The specified folder does not exist.")

# get papyri list from matrix
def create_papyri_list(file):
    # empty papyri list
    list_papyri = []
    # read file
    with open(file, newline = '', encoding="utf-8") as matrix:
        matrix_reader = csv.DictReader(matrix, delimiter='\t')
        for papyrus in matrix_reader:
            # create a dictionary from zipped header-cell value
            list_papyri.append(dict(papyrus))
    # returns list of papyri
    return list_papyri

def create_collection(arg):
    pass

# ----------------------
# ## Main Transformation

# get path to matrices folder
folder_path = get_folder_path()

# get list of paths to the matrices files in folder
files = get_files_path(folder_path)

print('===============')

# get list of iiif manifests along with type/subtype/variation IDs

# get grammateus taxonomy files

#for each <category> create collection (papyri by type? check wellcome collection model )
# 1. top level collection has four subcollection
# 2. subtype collections
# 3. variation collections


# for each matrix
for file in files:
    # create a list of papyri from each row of the matrix
    paplist = create_papyri_list(file)

    # select ones with iiif?
