<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0">
    
    <!-- Converts the file grammateus_taxonomy.xml into an HTML visualisation -->
    
    <xsl:output method="html"/>

    <xsl:strip-space elements="tei:category tei:catDesc"/>
    
    <!-- ********** -->
    <!-- Variables  -->
    <!-- ********** -->

    <xsl:variable name="href_type">
        <xsl:text>papyri.html?facet:f-type=</xsl:text>
    </xsl:variable>

    <xsl:variable name="subtype">
        <xsl:text>facet:f-subtype=</xsl:text>
    </xsl:variable>

    <xsl:variable name="variation">
        <xsl:text>facet:f-variation=</xsl:text>
    </xsl:variable>
    
    <xsl:variable name="authority">../../data/authority_lists.xml</xsl:variable>
    
    <!-- ********** -->
    <!-- Templates  -->
    <!-- ********** -->

    <xsl:template match="/">
        <div class="typology-overview">
            <xsl:apply-templates mode="typology"/>
        </div>
        
        <!-- how to cite (not ideal, maybe separate from the transformation?) -->
            <xsl:if test="//tei:idno[@type eq 'DOI']">
                <h3 class="h3">How to Cite</h3>
                <div class="pb-4">
                    <xsl:apply-templates select="//tei:desc" mode="cite"/>
                </div>
            </xsl:if>
    </xsl:template>
    
    <xsl:template match="tei:desc" mode="cite">
        <xsl:apply-templates mode="cite"/>
    </xsl:template>
    
    <xsl:template match="tei:desc" mode="typology"/>
    
    <xsl:template match="tei:title" mode="cite">
        <span class="title"> <xsl:apply-templates mode="cite"/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:idno[@type eq 'DOI']" mode="cite">
        <a href="https://doi.org/{.}">
            <xsl:value-of select="."/>
        </a>
    </xsl:template>

    <!-- category without sub-elements -->
    <xsl:template match="tei:category[not(child::tei:category)]" mode="typology">
        <li class="list-group-item level{count(ancestor::tei:category)}">
            <xsl:value-of select="normalize-space(tei:catDesc)"/>
            <!-- link to papyri depending on the level (type/subtype/variation) -->
            <xsl:variable name="link">
                <xsl:choose>
                    <!-- type -->
                    <xsl:when test="not(parent::tei:category)">
                        <xsl:value-of select="$href_type"/>
                        <xsl:value-of select="normalize-space(tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                    </xsl:when>
                    <!-- subtype -->
                    <xsl:when test="parent::tei:category[not(parent::tei:category)]">
                        <xsl:value-of select="$href_type"/>
                        <xsl:value-of select="normalize-space(parent::tei:category/tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                        <xsl:value-of select="$subtype"/>
                        <xsl:value-of select="normalize-space(tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                    </xsl:when>
                    <!-- variation -->
                    <xsl:otherwise>
                        <xsl:value-of select="$href_type"/>
                        <xsl:value-of select="normalize-space(parent::tei:category/parent::tei:category/tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                        <xsl:value-of select="$subtype"/>
                        <xsl:value-of select="normalize-space(parent::tei:category/tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                        <xsl:value-of select="$variation"/>
                        <xsl:value-of select="normalize-space(tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <!-- badges: C, D, P -->
            <xsl:apply-templates select="tei:catDesc/tei:ptr[@type eq 'class']" mode="typology"/>
            <xsl:apply-templates select="tei:catDesc/tei:ptr[@type eq 'descr']" mode="typology"/>
            <a href="{$link}" class="badge badge-secondary badge-link" target="_blank">P</a>
        </li>
    </xsl:template>

    <!-- category with sub-element -->
    <xsl:template match="tei:category[child::tei:category]" mode="typology">
        <!-- category header that can be toggled -->
        <div class="card-header level{count(ancestor::tei:category)}">
            <a data-toggle="collapse" href="#{./@xml:id}" role="button" aria-expanded="false" aria-controls="collapse">
                <i class="fas fa-caret-right"/>
                <span> </span>
                <xsl:value-of select="normalize-space(tei:catDesc)"/>
            </a>
            <!-- link to papyri depending on the level (type/subtype/variation) -->
            <xsl:variable name="link">
                <xsl:choose>
                    <!-- type -->
                    <xsl:when test="not(parent::tei:category)">
                        <xsl:value-of select="$href_type"/>
                        <xsl:value-of select="normalize-space(tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                    </xsl:when>
                    <!-- subtype -->
                    <xsl:when test="parent::tei:category[not(parent::tei:category)]">
                        <xsl:value-of select="$href_type"/>
                        <xsl:value-of select="normalize-space(parent::tei:category/tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                        <xsl:value-of select="$subtype"/>
                        <xsl:value-of select="normalize-space(tei:catDesc)"/>
                        <xsl:text>&amp;</xsl:text>
                    </xsl:when>
                    <!-- variation - does not have sub-elements -->
                    <xsl:otherwise/>
                </xsl:choose>
            </xsl:variable>
            <!-- badges: C, D, P -->
            <xsl:apply-templates select="tei:catDesc/tei:ptr[@type eq 'class']" mode="typology"/>
            <xsl:apply-templates select="tei:catDesc/tei:ptr[@type eq 'descr']" mode="typology"/>
            <a href="{$link}" class="badge badge-secondary badge-link" target="_blank">P</a>
        </div>
        <!-- sub-elements are listed in collapsible div -->
        <div class="collapse" id="{./@xml:id}">
            <div class="card card-body">
                <xsl:apply-templates select="tei:category" mode="typology"/>
            </div>
        </div>
    </xsl:template>
    
    <!-- Link to Description -->
    <xsl:template match="tei:ptr[@type eq 'descr']" mode="typology">
        <a href="descriptions/{replace(@target, '.xml', '')}" class="badge badge-secondary badge-link" target="_blank">D</a>
    </xsl:template>
    
    <!-- Link to Classification -->
    <xsl:template match="tei:ptr[@type eq 'class']" mode="typology">
        <a href="classification/{replace(@target, '.xml', '')}" class="badge badge-secondary badge-link" target="_blank">C</a>
    </xsl:template>

    <!-- WHITESPACE -->
    <!-- https://stackoverflow.com/questions/5737862/xslt-output-formatting-removing-line-breaks-and-blank-output-lines-from-remove -->
    <xsl:template match="*/text()[normalize-space()]" mode="typology">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>
    <xsl:template match="*/text()[not(normalize-space())]"/>

</xsl:stylesheet>