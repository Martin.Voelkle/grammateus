xquery version "3.1";

module namespace zt="http://grammateus.ch/zotero";

import module namespace g="http://grammateus.ch/global" at "global.xql";
import module namespace http="http://expath.org/ns/http-client";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";


(: variable - custom 
 : -----------------:)
(: group ID :)
declare variable $zt:group := "2288077";
(: query parameters for zotero api :)
declare variable $zt:params := "?v=3&amp;format=json&amp;include=bib,data,coins,citation&amp;style=chicago-fullnote-bibliography";
(: uri for grammateus bibliographic records :)
declare variable $zt:base-uri := "https://grammateus.unige.ch/data/bibl/";
(: directory where records are saved :)
declare variable $zt:data-dir := "/db/apps/grammateus/data/bibl";
(: metadata for TEI header of bibliographic record :)
declare variable $zt:authority := 
    <authority xmlns="http://www.tei-c.org/ns/1.0">Grammateus Project</authority>;
declare variable $zt:funder := 
    <funder xmlns="http://www.tei-c.org/ns/1.0">
        <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://p3.snf.ch/Project-182205">Project 182205</ref>
    </funder>;
declare variable $zt:license :=
    <licence xmlns="http://www.tei-c.org/ns/1.0" target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>;

(: variable - fixed 
 : ----------------:)
(: zotero api :)
declare variable $zt:api := "https://api.zotero.org/";
(: group items - all :)
declare variable $zt:items := concat($zt:api, "groups/", $zt:group, "/items");


(:~
 : Zotero http GET request
 : 
 : @param $url - string, valid zotero api url
 : @return http response header
 : @return zotero items as array of maps
:)
declare function zt:request($url) {
   http:send-request(<http:request http-version="1.1" href="{xs:anyURI($url)}" method="get"/>)
};

(:~
 : Get JSON record for one zotero item
 : 
 : @param $key - string, zotero item key
 : @return JSON zotero record as map
:)
declare function zt:item($key as xs:string) {
    json-doc($zt:items || "/" || $key || $zt:params)
};


(:~
 : Build new TEI record from JSON
 : @param $rec - map, JSON record from Zotero
 : @param $local-id - string, local record id (zotero key)
 : @return formatted bibliographic reference as a full TEI document
 : From zotero2bibl app
 : 
 : Problem: some tei namespace are missing from the result, adding it to every element
:)
declare function zt:build-new-record-json($rec as item()*, $local-id) {
    (:citation key:)
let $idNumber := tokenize($rec?data?extra,': ')[last()]
let $itemType := $rec?data?itemType
let $recordType := 	
    if($itemType = 'book' and $rec?data?series[. != '']) then 'monograph'
    else if($itemType = ('journalArticle','bookSection','magazineArticle','newspaperArticle','conferencePaper') or $rec?data?series != '') then 'analytic' 
    else 'monograph' 
(: Main titles from zotero record:)
let $analytic-title := for $t in $rec?data?title
                       return
                            element { fn:QName("http://www.tei-c.org/ns/1.0", "title") } {
                             if($recordType = 'analytic') then attribute level { "a" }
                             else if($recordType = 'monograph') then (attribute level { "m" }, attribute type {"main"})
                             else (), $t}
let $bookTitle :=        for $title in $rec?data?bookTitle[. != ''] 
                         return 
                            <title level="m" type="main" xmlns="http://www.tei-c.org/ns/1.0">{$title}</title>
let $shortBookTitle :=  for $title in $rec?data?shortTitle[. != ''] 
                         return 
                            <title level="m" type="short" xmlns="http://www.tei-c.org/ns/1.0">{$title}</title>
let $series-titles :=  (for $series in $rec?data?series[. != ''] 
                        return 
                            element { fn:QName("http://www.tei-c.org/ns/1.0", "title") } {
                            if($recordType = 'monograph') then attribute level { "s" }
                            else (), $series},
                        for $series in $rec?data?seriesTitle[. != ''] 
                        return 
                            element { fn:QName("http://www.tei-c.org/ns/1.0", "title") } {attribute level { "s" }, $series})
let $journal-titles :=  for $journal in $rec?data?publicationTitle[. != '']
                        return <title level="j" xmlns="http://www.tei-c.org/ns/1.0">{$journal}</title>
(: add journal short title :)
let $journal-titles-short :=  for $journal in $rec?data?journalAbbreviation[. != '']
                        return <title level="j" type="short" xmlns="http://www.tei-c.org/ns/1.0">{$journal}</title>
(: add proceedings title :)
let $proceedings-titles := for $proc in $rec?data?proceedingsTitle[. != '']
                        return <title level="m" xmlns="http://www.tei-c.org/ns/1.0">{$proc}</title>                      
let $titles-all := ($analytic-title,$series-titles,$journal-titles)
(: Local ID and URI :)
let $local-uri := <idno type="URI" xmlns="http://www.tei-c.org/ns/1.0">{$local-id}</idno>   
(:    Uses the Zotero ID (manually numbered tag) to add an idno with @type='zotero':)
let $zotero-idno := <idno type="URI" xmlns="http://www.tei-c.org/ns/1.0">{$rec?links?alternate?href}</idno>  
(:  Equals the biblStruct/@corresp URI to idno with @type='URI' :)
let $zotero-idno-uri := <idno type="URI" xmlns="http://www.tei-c.org/ns/1.0">{replace($rec?links?self?href,'api.zotero.org','www.zotero.org')}</idno>
(:  Grabs URI in tags prefixed by 'Subject: '. :)
let $subject-uri := $rec?data?tags?*?tag[matches(.,'^\s*Subject:\s*')]
(:  Not sure here if extra is always the worldcat-ID and if so, 
if or how more than one ID are structured, however: converted to worldcat-URI :)
let $extra := 
                for $extra in tokenize($rec?data?extra,'\n')
                return 
                    if(matches($extra,'^OCLC:\s*')) then 
                        <idno type="URI" subtype="OCLC" xmlns="http://www.tei-c.org/ns/1.0">{concat("http://www.worldcat.org/oclc/",normalize-space(substring-after($extra,'OCLC: ')))}</idno>
                    else if(matches($extra,'^CTS-URN:\s*')) then 
                        (<idno type="CTS-URN" xmlns="http://www.tei-c.org/ns/1.0">{normalize-space(substring-after($extra,'CTS-URN: '))}</idno>(:,
                         <ref type="URL" target="{concat("https://scaife.perseus.org/library/",normalize-space(substring-after($extra,'CTS-URN: ')))}"/>:)
                        )
                    else if(matches($extra,'^xmlFile:\s*')) then 
                        <ref type="URL" subtype="xmlFile" target="{normalize-space(substring-after($extra,'xmlFile: '))}" xmlns="http://www.tei-c.org/ns/1.0"/>                        
                    else if(matches($extra,'^DOI:\s*')) then
                        let $doi := normalize-space(substring-after($extra,'DOI: '))
                        let $doiLink := if(starts-with($doi,'http')) then $doi else concat('http://dx.doi.org/',$doi)
                        return <idno type="URI" subtype="DOI" xmlns="http://www.tei-c.org/ns/1.0">{$doiLink}</idno>
                    else if(matches($extra,'^([\d]\s*)')) then 
                        <idno type="URI" xmlns="http://www.tei-c.org/ns/1.0">{"http://www.worldcat.org/oclc/" || $extra}</idno>
                    else ()                    
let $refs := for $ref in $rec?data?url[. != '']
             return <ref target="{$ref}" xmlns="http://www.tei-c.org/ns/1.0"/> 
(: OA_link and doc_link :)
let $json := json-doc($rec?links?self?href||"/children")
let $oa_link := for $url in $json?*?data[?title = "OA_link"]?url
                return <idno type="URI" subtype="OA" xmlns="http://www.tei-c.org/ns/1.0">{$url}</idno>
let $doc_link := for $url in $json?*?data[?title = "doc_link"]?url
                return <idno type="URI" subtype="doc" xmlns="http://www.tei-c.org/ns/1.0">{$url}</idno>
let $all-idnos := ($local-uri,$zotero-idno,$zotero-idno-uri,$extra,$refs,$oa_link,$doc_link)
(: Add language See: https://github.com/biblia-arabica/zotero2bibl/issues/16:)
let $lang := if($rec?data?language) then
                element {fn:QName("http://www.tei-c.org/ns/1.0", "textLang")} { 
                    attribute mainLang {tokenize($rec?data?language,',')[1]},
                    if(count(tokenize($rec?data?language,',')) gt 1) then
                      attribute otherLangs {
                        normalize-space(string-join(
                            tokenize($rec?data?language,',')[position() gt 1],' ' 
                            ))}  
                    else ()
                }  
             else ()             
(: organizing creators by type and name :)
let $creator := for $creators in $rec?data?creators?*
                return 
                    if($creators?firstName) then
                        element {fn:QName("http://www.tei-c.org/ns/1.0",$creators?creatorType)}{
                            element {fn:QName("http://www.tei-c.org/ns/1.0","forename")}{$creators?firstName},
                            element {fn:QName("http://www.tei-c.org/ns/1.0","surname")}{$creators?lastName}
                        }
                    else element {fn:QName("http://www.tei-c.org/ns/1.0",$creators?creatorType)} {element {fn:QName("http://www.tei-c.org/ns/1.0","name")} {$creators?name}} 
(: creating imprint, any additional data required here? :)
let $imprint := if (empty($rec?data?place) and empty($rec?data?publisher) and empty($rec?data?date)) then () else (<imprint xmlns="http://www.tei-c.org/ns/1.0">{
                    if ($rec?data?place) then (<pubPlace xmlns="http://www.tei-c.org/ns/1.0">{$rec?data?place}</pubPlace>) else (),
                    if ($rec?data?publisher) then (<publisher xmlns="http://www.tei-c.org/ns/1.0">{$rec?data?publisher}</publisher>) else (),
                    if ($rec?data?date) then (<date xmlns="http://www.tei-c.org/ns/1.0">{$rec?data?date}</date>) else ()
                }</imprint>)
(: Transforming tags to relation... if no subject or ms s present, still shows <listRelations\>, I have to fix that :)
let $list-relations := if (empty($rec?data?tags) or empty($rec?data?relations)) then () else (<listRelation>{(
                        for $tag in $rec?data?tags?*?tag
                        return 
                            if (matches($tag,'^\s*(MS|Subject|Part|Section|Book|Provenance|Source|Translator):\s*')) then (
                                element {fn:QName("http://www.tei-c.org/ns/1.0", "relation")} {
                                    attribute active {$local-uri},
                                    if (matches($tag,'^\s*(Subject|Part|Section|Book|Provenance|Source|Translator):\s*')) then (
                                        let $type := replace($tag,'^\s*(.+?):\s*.*','$1')
                                        return
                                        (attribute ref {"dc:subject"},
                                        if (string-length($type)) then 
                                            attribute type {lower-case($type)}
                                            else(),
                                        element {fn:QName("http://www.tei-c.org/ns/1.0", "desc")} {substring-after($tag,concat($type,": "))}
                                    )) else (),
                                    if (matches($tag,'^\s*MS:\s*')) then (
                                        attribute ref{"dcterms:references"},
                                        element desc {
                                            element msDesc {
                                                element msIdentifier {
                                                    element settlement {normalize-space(tokenize(substring-after($tag,"MS: "),",")[1])},
                                                    element collection {normalize-space(tokenize(substring-after($tag,"MS: "),",")[2])},
                                                    element idno {
                                                        attribute type {"shelfmark"},
                                                        normalize-space(replace($tag,"MS:.*?,.*?,",""))
                                                        }
                                                }
                                            }
                                        }
                                    ) else ()
                                }
                            ) else (),
                        let $relations := $rec?data?relations
                        let $rec := $local-uri
                        let $zotero-url := concat('http://zotero.org/groups/',$zt:group,'/items')
                        let $related := string-join(data($relations?*),' ')
                        let $all := normalize-space(concat($local-uri,' ', replace($related,$zotero-url,$zt:base-uri)))
                        where $related != '' and $related != ' '
                        return  
                            element relation {
                                attribute name {'dc:relation'},
                                attribute mutual {$all}
                            }
                    )}</listRelation>)
(: Not sure if that is sufficient for an analytic check? following the TEI-guideline and the other script @github... :)
let $tei-analytic := if($recordType = "analytic" or $recordType = "bookSection" or $recordType = "chapter") then
                         <analytic xmlns="http://www.tei-c.org/ns/1.0">{
                            if($itemType = "bookSection" or $itemType = "chapter") then $creator[self::tei:author]
                            else $creator,
                            $analytic-title,
                            $all-idnos
                         }</analytic>
                         else ()
let $tei-monogr := if($recordType = "analytic" or $recordType = "monograph") then
                    <monogr xmlns="http://www.tei-c.org/ns/1.0">{
                        if($itemType = "bookSection" or $itemType = "chapter") then
                            $creator[self::tei:editor]
                        else if($recordType = "analytic") then ()
                        else $creator,
                        if($recordType = "monograph") then ($analytic-title, $shortBookTitle)
                        else if($itemType = "bookSection" or $itemType = "chapter") then ($bookTitle, $shortBookTitle)
                        else if($itemType = "conferencePaper") then $proceedings-titles
                        else ($series-titles,$journal-titles,$journal-titles-short),(:add abbreviated title?:)
                        if ($tei-analytic) then () else ($all-idnos),
                        if($lang) then ($lang) else (),
                        if ($imprint) then ($imprint) else (),
                        for $p in $rec?data?pages[. != '']
                        return <biblScope unit="pp">{$p}</biblScope>,
                        for $vol in $rec?data?volume[. != '']
                        return <biblScope unit="vol">{$vol}</biblScope>
                    }</monogr>
                     else ()
(: I haven't found an example file with series information to find the JSON equivalence to the tei structure, so have to continue on that :)
let $tei-series := if($series-titles) then 
                        <series>
                        {$series-titles}
                        {for $vol in $rec?data?seriesNumber[. != '']
                        return <biblScope>{$vol}</biblScope>}
                        </series>
                    else()                        
let $citedRange := for $p in $rec?data?tags?*?tag[matches(.,'^\s*PP:\s*')]
                   return <citedRange unit="page" xmlns="http://www.tei-c.org/ns/1.0">{substring-after($p,'PP: ')}</citedRange>
(:  Replaces links to Zotero items in abstract in format {https://www.zotero.org/groups/[...]} with URIs :)
let $abstract :=   for $a in $rec?data?abstractNote[. != ""]
    let $a-link-regex := concat('\{https://www.zotero.org/groups/',$zt:group,'.*?/itemKey/([0-9A-Za-z]+).*?\}')
    let $a-link-replace := $zt:base-uri
    let $a-text-linked := 
        for $node in analyze-string($a,$a-link-regex)/*
        let $url := concat($a-link-replace,'/',$node/fn:group/text())
        let $ref := <ref target='{$url}'>{$url}</ref>
        return if ($node/name()='match') then $ref else $node/text()
    return 
    <note type="abstract" xmlns="http://www.tei-c.org/ns/1.0">{$a-text-linked}</note>
(: checks existing doc to compare editing history, etc. :)
let $existing-doc := doc(concat($zt:data-dir,'/',tokenize($local-id,'/')[last()],'.xml'))
let $existing-zotero-editors := $existing-doc/TEI/teiHeader/fileDesc/titleStmt/respStmt[resp = 'Record edited in Zotero by']
(: Need to include here Vol. URLs contained in notes (see above) as well as DOIs contained in notes :)
let $getNotes := 
                if($rec?meta?numChildren[. gt 0]) then
                    let $url := concat($zt:api,'/groups/',$zt:group,'/items/',tokenize($local-id,'/')[last()],'/children') 
                    let $children := http:send-request(<http:request http-version="1.1" href="{xs:anyURI($url)}" method="get"/>)
                    return 
                        if($children[1]/@status = '200') then 
                                    let $notes := parse-json(util:binary-to-string($children[2]))
                                    for $n in $notes?*
                                    return 
                                        if($n?data?note[matches(.,'^<p>PP:')]) then 
                                            <citedRange unit="page">{replace(substring-after($n?data?note[matches(.,'^<p>PP:')],'PP: '),'<[^>]*>','')}</citedRange>
                                        else ()
                             else()
                else ()
                
let $bibl := 
    let $html-citation := $rec?bib
    let $html-no-breaks := replace($html-citation,'\\n\s*','')
    let $html-i-regex := '((&#x201C;)|(&lt;i&gt;))(.+?)((&#x201D;)|(&lt;/i&gt;))'
    let $html-i-analyze := 
        analyze-string($html-no-breaks,$html-i-regex)
    let $tei-i := 
        for $text in $html-i-analyze/*
        return 
            let $title-level := 
                if ($text/descendant::fn:group/@nr=2) then 'a'
                else 'm'
            return
                if ($text/name()='non-match') then $text/text()
                else element title {attribute level {$title-level},$text/fn:group[@nr=4]/text()}
    let $tei-citation := 
        for $text in $tei-i
        let $no-tags := parse-xml-fragment(replace($text,'&lt;.+?&gt;',''))
        return 
            if ($text/node()) then element {$text/name()} {$text/@*, $no-tags} else $no-tags
    return element bibl {attribute type {'formatted'}, attribute subtype {'bibliography'}, attribute resp {'https://www.zotero.org/styles/chicago-note-bibliography-17th-edition'}, $tei-citation}
let $coins := 
    let $get-coins := $rec?coins
    let $target := substring-before(substring-after($get-coins,"title='"),"'")
    return 
        if($get-coins != '') then 
          element bibl {attribute type {'formatted'}, attribute subtype {'coins'},attribute resp {'https://www.zotero.org/styles/chicago-note-bibliography-17th-edition'}, 
               element ptr {attribute target {$target}}
          }  
        else ()
let $citation := 
    let $html-citation := $rec?citation
    let $html-no-breaks := replace($html-citation,'\\n\s*','')
    let $html-i-regex := '((&#x201C;)|(&lt;i&gt;))(.+?)((&#x201D;)|(&lt;/i&gt;))'
    let $html-i-analyze := 
        analyze-string($html-no-breaks,$html-i-regex)
    let $tei-i := 
        for $text in $html-i-analyze/*
        return 
            let $title-level := 
                if ($text/descendant::fn:group/@nr=2) then 'a'
                else 'm'
            return
                if ($text/name()='non-match') then $text/text()
                else element title {attribute level {$title-level},$text/fn:group[@nr=4]/text()}
    let $tei-citation := 
        for $text in $tei-i
        let $no-tags := parse-xml-fragment(replace($text,'&lt;.+?&gt;',''))
        return 
            if ($text/node()) then element {$text/name()} {$text/@*, $no-tags} else $no-tags
    return element bibl {attribute type {'formatted'}, attribute subtype {'citation'},attribute resp {'https://www.zotero.org/styles/chicago-note-bibliography-17th-edition'}, $tei-citation}        
return
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
        <teiHeader>
            <fileDesc>
                <titleStmt>{(
                    $titles-all,
                    $zt:funder,
                    (: Editors :)
                    for $e in $rec?meta?createdByUser
                    let $uri := $e?links?*?href
                    let $name := if($e?name[. != '']) then $e?name else $e?username
                    return
                        <editor role="creator" ref="{$uri}">{$name}</editor>,
                    for $e in $rec?meta?lastModifiedByUser
                    let $uri := $e?links?*?href
                    let $name := if($e?name[. != '']) then $e?name else $e?username
                    return
                        <editor role="creator" ref="{$uri}">{$name}</editor>,
                    for $e in $existing-zotero-editors[name/@ref != $rec?meta?lastModifiedByUser?links?*?href]
                    let $uri := $existing-zotero-editors/name/@ref
                    let $name := $existing-zotero-editors/name/text()
                    return 
                        <editor role="creator" ref="{$uri}">{$name}</editor>,
                    (: Replacing "Assigned: " with "Edited: " but with no username. 
                    Will duplicate editor if name is listed differently in Zotero. :)
                    (:for $e in $rec?data?tags?*?tag[starts-with(.,'Assigned:')]
                    let $assigned := substring-after($e, 'Assigned: ')
                    where $assigned != $rec?meta?createdByUser?username
                    return
                        <editor role="creator" ref="https://www.zotero.org/{$assigned}">{$assigned}</editor>,:)
                    for $e in $rec?data?tags?*?tag[starts-with(.,'Edited:')]
                    let $edited := substring-after($e, 'Edited: ')
                    where $edited != ($rec?meta?createdByUser?name,$rec?meta?lastModifiedByUser?name)
                    return
                        <editor role="creator">{$edited}</editor>,
                    (: respStmt :)
                    for $e in $rec?meta?createdByUser
                    let $uri := $e?links?*?href
                    let $name := if($e?name[. != '']) then $e?name else $e?username
                    return
                        <respStmt><resp>Record added to Zotero by</resp><name ref="{$uri}">{$name}</name></respStmt>,
                    for $e in $rec?meta?lastModifiedByUser
                    let $uri := $e?links?*?href
                    let $name := if($e?name[. != '']) then $e?name else $e?username
                    return
                        <respStmt><resp>Record edited in Zotero by</resp><name ref="{$uri}">{$name}</name></respStmt>,
                    for $e in $existing-zotero-editors[name/@ref != $rec?meta?lastModifiedByUser?links?*?href]
                    return 
                        $existing-zotero-editors,                    
                    (: Replacing "Assigned: " with "Edited: " but with no username :)
                    (:for $e in $rec?data?tags?*?tag[starts-with(.,'Assigned:')]
                    let $assigned := substring-after($e, 'Assigned: ')
                    where $assigned != $rec?meta?createdByUser?username
                    return 
                        <respStmt><resp>Primary editing by</resp><name ref="https://www.zotero.org/{$assigned}">{$assigned}</name></respStmt>:)
                    for $e in $rec?data?tags?*?tag[starts-with(.,'Edited:')]
                    let $edited := substring-after($e, 'Edited: ')
                    return 
                        <respStmt><resp>Primary editing by</resp><name>{$edited}</name></respStmt>
                    )}</titleStmt>
                <publicationStmt>
                    <authority>{
                        $zt:authority/text()
                    }</authority>
                    <idno type="URI">{$zt:base-uri || $idNumber}.xml</idno>
                    <availability>{$zt:license}</availability>
                    <date>{current-date()}</date>
                </publicationStmt>
                <sourceDesc>
                    <p>Born digital document.</p>
                </sourceDesc>
            </fileDesc>
            <revisionDesc>
                <change when="{current-date()}">CREATED: This bibl record was autogenerated from a Zotero record.</change>
            </revisionDesc>
        </teiHeader>
        <text>
            <body>
                <biblStruct>
                  {$tei-analytic}
                  {$tei-monogr}
                  {$tei-series}
                  {$abstract}
                  {$citedRange}
                  {$getNotes}
                </biblStruct>
            </body>
        </text>
    </TEI>
};

(:~
 : Convert records to Syriaca.org compliant TEI records, using zotero2tei.xqm
 : Save records to the database. 
 : @param $record 
 : @param $index-number
 : @param $format
:)
declare function zt:process-record($record as item()?, $key){
    let $idNumber := tokenize($record?data?extra,': ')[last()]
                 
    (:let $id := local:make-local-uri($index-number):)
    let $file-name := concat($idNumber,'.xml')
    let $new-record := zt:build-new-record-json($record, $key)
    return 
        if($idNumber != '') then 
            try {xmldb:store($zt:data-dir, xmldb:encode-uri($file-name), $new-record)} catch *{
                <response status="fail">
                    <message>Failed to add resource {$file-name}: {concat($err:code, ": ", $err:description)}</message>
                </response>
            } 
        else ()  
};