<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d27">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Liturgical Oath</title>
                <author corresp="authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:7buhk2uru5bgtiwo4mq7qmjmmi</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_decl"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_oath"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="authority-lists.xml#EN" when="2021-07-09">TEI file created</change>
            <change who="authority-lists.xml#SF" when="2021-05-21">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Liturgical oath</head>
            <p xml:id="d27_p1">A liturgical oath was required from every individual who had been
                nominated to a liturgical office [<ref type="descr" target="#gt_nomination">nomination to liturgy</ref>] and then notified of their appointment [<ref target="#gt_programma" type="descr">approval of nomination</ref>] <bibl>
                    <author>Lewis</author>
                    <ptr target="gramm:lewis1997"/>
                    <citedRange>84</citedRange>
                </bibl>.</p>
            <p xml:id="d27_p2">
                <bibl>
                    <author>Seidl</author>
                    <ptr target="gramm:seidl1933"/>
                    <citedRange>76-80</citedRange>
                </bibl> makes a distinction between two kinds of oaths: on the one hand, those that
                merely assert a fact, e.g. an oath denying the embezzlement of agricultural products
                    [<ref type="external" target="https://papyri.info/trismegistos/110153">110153</ref> 34 CE, Ptolemais Melissurgon (Arsinoite nome); <ref type="external" target="https://papyri.info/trismegistos/128303">128303</ref>
                254 CE, Oxyrhynchus]; on the other hand, pledges to act according to a prescription,
                such as undertaking a liturgy. The technical word used in this context to designate
                the oath is <term xml:lang="grc">χειρογραφία</term>, <gloss>written testimony</gloss>
                <bibl>
                    <author>Seidl</author>
                    <ptr target="gramm:seidl1933"/>
                    <citedRange>132-135</citedRange>
                </bibl> [<ref type="external" target="https://papyri.info/trismegistos/18342">18342</ref> 208 CE, nome of Arabia], to be distinguished from <term xml:lang="grc">χειρόγραφον</term>
                <gloss>hand-written contract</gloss>.</p>
            <p xml:id="d27_p3">The oath was the final stage in the nomination process. In the oath,
                which was addressed to the <term xml:lang="lat">strategos</term>, the nominee
                pledged that he would dutifully perform his compulsory public service. Several
                copies of the oath were produced, e.g. [<ref type="internal" target="16555.xml">16555</ref> 277 CE, Oxyrhynchus] which has three copies, one written completely
                in the hand of the liturgist himself; one was presumably kept among official
                records, pasted in a <term xml:lang="grc">τόμος συγκολλήσιμος</term> [<ref type="external" target="https://papyri.info/trismegistos/78601">78601</ref>;
                    <ref type="external" target="https://papyri.info/trismegistos/78602">78602</ref>
                both 288 CE, Herakleopolite nome]. At the end of a [<ref target="#gt_condolence" type="descr">letter of condolence</ref>] the text of an oath is quoted [<ref type="internal" target="17411.xml">17411</ref> 235 CE, Oxyrhynchus]; this is
                presumably an exceptional case.</p>
            <p xml:id="d27_p4">A list of liturgical oaths is provided by <bibl>
                    <author>Lewis</author>
                    <ptr target="gramm:lewis1997"/>
                    <citedRange>117</citedRange>
                </bibl>. A few more have appeared since [<ref type="external" target="https://papyri.info/trismegistos/79121">79121</ref> 138-161 CE; <ref type="external" target="https://papyri.info/trismegistos/15806">15806</ref> 139
                CE; <ref type="external" target="https://papyri.info/trismegistos/15200">15200</ref>
                182-184 CE, both Oxyrhynchus; <ref type="external" target="https://papyri.info/trismegistos/30053">30053</ref> III CE; <ref type="external" target="https://papyri.info/trismegistos/78602">78602</ref>],
                but they offer little new information. A clear definition of the category of
                liturgical oaths is difficult to establish. For instance, an oath [<ref type="internal" target="12074.xml">12074</ref> 24 CE, Arsinoite nome] by <term xml:lang="grc">ἀφεσιοφύλακες</term> (sluice guards) is discounted by <bibl>
                    <author>Lewis</author>
                    <ptr target="gramm:lewis1997"/>
                    <citedRange>117, n. 1</citedRange>
                </bibl>, who does not consider <term xml:lang="grc">ἀφεσιοφυλακία</term> as a
                liturgy at so early a date. It should, however, be noted that: a) although the
                wording of [<ref type="internal" target="12074.xml">12074</ref>] suggests <term xml:lang="grc">ἀφεσιοφυλακία</term> only indirectly, the word itself is attested
                elsewhere [<ref type="external" target="https://papyri.info/trismegistos/13157">13157</ref> 182-184 CE, Oxyrhynchus]; b) in its formulation, the oath found in
                    [<ref type="internal" target="12074.xml">12074</ref>] closely resembles
                liturgical oaths <hi rend="italic">stricto sensu</hi>; c) this quasi-liturgical oath
                precedes liturgical oaths <hi rend="italic">stricto sensu</hi> by one century and
                originates from the Arsinoite nome, which is otherwise poorly represented.</p>
            <p xml:id="d27_p5">Moreover, there exists a second group of liturgical oaths, namely
                those sworn by the guarantors presented to vouch for the nominees. A scribal model
                for this type of document is to be found in [<ref type="external" target="https://papyri.info/trismegistos/27285">27285</ref> II-III CE, Arsinoite nome]. This may be done
                by simply adding a signature at the bottom of the nominee’s own oath [<ref type="internal" target="16555.xml">16555</ref>], or by producing a separate
                document containing the guarantor’s oath [<ref type="external" target="https://papyri.info/trismegistos/9655">9655</ref> 96 CE, <ref type="external" target="https://papyri.info/trismegistos/9656">9656</ref> 205
                CE, both Arsinoite nome; <ref type="internal" target="16554.xml">16554</ref> 253-254
                CE, Oxyrhynchus].</p>
            <p xml:id="d27_p6">In this respect, [<ref type="external" target="https://papyri.info/trismegistos/15806">15806</ref>] constitutes a
                singular case where three steps of the procedure are recorded in a sequence: in the
                first column, a sworn declaration by an individual who was nominated to the service
                of village scribe (<term xml:lang="grc">κωμογραμματεύς</term>); in the second, the
                sworn guarantee provided by the person vouching for the nominee; and in the third, a
                guarantee given by the village elders (<term xml:lang="grc">πρεσβύτεροι</term>) to
                produce the nominee if required.</p>
            <p xml:id="d27_p7">Liturgical oaths <hi rend="italic">stricto sensu</hi> are attested
                from ca. 130 CE [<ref type="external" target="https://papyri.info/trismegistos/23906">23906</ref> Hermopolis (?)]; the
                latest cases date from 303 CE [<ref type="external" target="https://papyri.info/trismegistos/15231">15231</ref>, <ref type="external" target="https://papyri.info/trismegistos/16556">16556</ref> both
                Oxyrhynchus]. Among the several dozens of preserved liturgical oaths, more than half
                – and most of the best-preserved documents – come from the Oxyrhynchite nome. Other
                nomes are represented (Small Oasis [<ref type="external" target="https://papyri.info/trismegistos/21536">21536</ref> 212 CE], Hermopolite
                    [<ref type="internal" target="17503.xml">17503</ref> 221 CE; <ref type="internal" target="22366.xml">22366</ref> 261 CE], Herakleopolite [<ref type="external" target="https://papyri.info/trismegistos/20184">20184</ref>
                180-192 CE], Memphite [<ref type="internal" target="31834.xml">31834</ref> 212-214
                CE], Arabia [<ref type="internal" target="22547.xml">22547</ref> 183 CE]);
                liturgical oaths from the Arsinoite nome are conspicuously scarce [<ref type="external" target="https://papyri.info/trismegistos/13007">13007</ref> 156
                CE; <ref type="external" target="https://papyri.info/trismegistos/41438">41438</ref>
                180-193 CE] and badly preserved.</p>
            <div xml:id="d27_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d27_p8">Liturgical oaths follow a regular pattern. The <seg xml:id="d27_s1" type="syntax" corresp="../authority_lists.xml#descr-contents">[type of
                        liturgy]</seg> is sometimes inscribed in the top margin [<ref type="internal" target="15976.xml" corresp="#d27_s1">15976</ref> 225 CE; <ref type="external" target="https://papyri.info/trismegistos/20741" corresp="#d27_s1">20741</ref> 244-245 CE, both
                    Oxyrhynchus], after which the opening starts with:</p>
                <p xml:id="d27_p9">
                    <seg xml:id="d27_s2" type="syntax" corresp="../authority_lists.xml#introduction">[to strategos &lt;dat.&gt;]</seg> [<ref type="external" target="https://papyri.info/trismegistos/19099" corresp="#d27_s2">19099</ref> 175 CE,
                    Oxyrhynchite nome; <ref type="internal" target="15976.xml" corresp="#d27_s2">15976</ref>; <ref type="internal" target="22459.xml" corresp="#d27_s2">22459</ref> 207 CE, Oxyrhynchite nome ;
                        <ref type="internal" target="16555.xml" corresp="#d27_s2">16555</ref>; <ref type="external" target="https://papyri.info/trismegistos/21582" corresp="#d27_s2">21582</ref> 212-213 CE,
                    Oxyrhynchus]</p>
                <p xml:id="d27_p10">An exception is to be found in [<ref type="internal" target="17503.xml">17503</ref>], which has a typical letter opening, with
                    <term xml:lang="grc">χαίρειν</term>. The names of nominees are sometimes followed by identifiers (age, scars
                    etc.) [<ref type="external" target="https://papyri.info/trismegistos/19099">19099</ref>; <ref type="external" target="https://papyri.info/trismegistos/15806">15806</ref>]. The list of
                    nominees can be quite long [<ref type="external" target="https://papyri.info/trismegistos/15987">15987</ref> 253-256 CE,
                    Oxyrhynchus].</p>
                <p xml:id="d27_p11">The nominee then indicates to what liturgy he has been
                    appointed, e.g. <term xml:lang="grc">εἰσδοθεὶς ὑπὸ τοῦ τῶν [τ]όπων
                        κωμογρ(αμματέως) εἰς νομοφυλακίαν</term>
                    <gloss>having been appointed by the village secretary to the nome police</gloss> [<ref type="internal" target="22459.xml">22459</ref>, l.4-6].</p>
                <p xml:id="d27_p12">This is followed by the oath, starting with <seg xml:id="d27_s3" type="syntax" corresp="../authority_lists.xml#main-text">
                        <term xml:lang="grc">ὀμνύω</term>
                    </seg>
                    <gloss corresp="#d27_s3">I swear</gloss>. The emphatic particles <term xml:lang="grc">ἦ μήν</term>, which
                    often introduce oaths in the Classical period, are not used (exception : [<ref type="internal" target="12074.xml">12074</ref>, l.15] an atypical oath of
                    guarantors, dating from 24 CE). The nominee swears by the emperor himself [<ref type="internal" target="21618.xml" corresp="#d27_s3">21618</ref> 138 CE, Oxyrhynchite nome], or by the emperor’s <term xml:lang="grc">τύχη</term>
                        <gloss>destiny</gloss> [<ref type="internal" target="15976.xml" corresp="#d27_s3">15976</ref>; <ref type="internal" target="31834.xml" corresp="#d27_s3">31834</ref>] <bibl>
                        <author>Seidl</author>
                        <ptr target="gramm:seidl1933"/>
                        <citedRange>23-32</citedRange>
                    </bibl>.</p>
                <p xml:id="d27_p13">In the oath proper, the nominee takes a pledge that he will
                    undertake the task (<term xml:lang="grc">χρεία</term>) that has been attributed
                    to him [<ref type="internal" target="22459.xml">22459</ref>]. This is often
                    emphasized by a pair of adverbs, e.g. <term xml:lang="grc">ἀδιαλείπτως καὶ
                        ἀμέμπτως</term>
                    <gloss>without any interruption or blame</gloss> [<ref type="internal" target="31834.xml">31834</ref>], <term xml:lang="grc">πιστῶς καὶ
                        ἐπιμελῶς</term>
                    <gloss>trustfully and with care</gloss> [<ref type="external" target="https://papyri.info/trismegistos/19981">19981</ref> 138-161 CE],
                        <term xml:lang="grc">ὑγιῶς καὶ πιστῶς</term>
                    <gloss>in a healthy and reliable way</gloss> [<ref type="internal" target="15976.xml">15976</ref>].</p>
                <p xml:id="d27_p14">The nominee is made accountable for his oath, with the formula
                        <term xml:lang="grc">ἢ ἔνοχος εἴην τῷ ὅρκῳ</term>
                    <gloss>or let me be bound my oath</gloss>, presumably a curse against perjury <bibl>
                        <author>Seidl</author>
                        <ptr target="gramm:seidl1933"/>
                        <citedRange>121</citedRange>
                    </bibl> [<ref type="internal" target="21618.xml">21618</ref>; <ref type="external" target="https://papyri.info/trismegistos/19981">19981</ref>;
                        <ref type="internal" target="22459.xml">22459</ref>; <ref type="internal" target="15976.xml">15976</ref>].</p>
                <p xml:id="d27_p15">He then provides the name of one or several guarantors, e.g. [<ref type="internal" target="22547.xml">22547</ref> l.19-20] <term xml:lang="grc">παρέσχ̣ον δὲ ἐμαυτοῦ ἐν[γυ]ητήν</term>
                    <gloss>I have provided as my guarantor...</gloss>, and the guarantor himself states after
                    his own signature (l.30-31), <term xml:lang="grc">ἐνγυῶμαι αὐτόν</term>
                    <gloss>I vouch for him</gloss>. See also [<ref type="internal" target="31834.xml">31834</ref>; <ref type="internal" target="16010.xml">16010</ref> 237 CE,
                    Oxyrhynchus].</p>
                <p xml:id="d27_p16">The oath usually closes with a <seg xml:id="d27_s4" type="syntax" corresp="../authority_lists.xml#date">[date@end]</seg> and a
                        <seg xml:id="d27_s5" type="syntax" corresp="../authority_lists.xml#subscription">[signature]</seg> [<ref type="internal" target="16555.xml" corresp="#d27_s4 #d27_s5">16555</ref>]. In a document from the Arsinoite nome, a notary (<term xml:lang="grc">νομογράφος</term>) writes on behalf of the nominee, who
                    cannot write [<ref type="external" target="https://papyri.info/trismegistos/13007">13007</ref>]. In [<ref type="internal" target="22547.xml">22547</ref>] the guarantor vouches for
                    the liturgist, but also signs on the latter’s behalf.</p>
                <p xml:id="d27_p17">A short summary is sometimes inscribed on the back of the
                    document [<ref type="internal" target="22459.xml">22459</ref>, <ref type="external" target="https://papyri.info/trismegistos/21582">21582</ref>].</p>
            </div>
            <div xml:id="d27_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d27_p18">Given the uneven geographical distribution of well-preserved
                    liturgical oaths, it is easier to describe the format of documents from the
                    Oxyrhynchite nome [<ref type="internal" target="15976.xml">15976</ref>] (which
                    also seems to correspond to that of the nome of Arabia [<ref type="internal" target="22547.xml">22547</ref>], the Memphite [<ref type="internal" target="31834.xml">31834</ref>] and – not invariably – Hermopolite nome
                        [<ref type="internal" target="22366.xml">22366</ref>]). Oxyrhynchite
                    liturgical oaths conform to the demotic style format, also
                    prevalent among other documents pertaining to liturgy [<ref type="descr" target="#gt_nomination">nomination to liturgy</ref>, <ref target="#gt_programma" type="descr">approval of nomination</ref>]. This
                    format consists of a long and narrow vertical sheet (ca. H. 30 x W. 10cm), with
                    the writing following the direction of fibres. The scribe does not necessarily
                    use the whole height of the sheet, thus leaving an ample margin at the bottom
                        [<ref type="internal" target="21618.xml">21618</ref>, <ref type="internal" target="22459.xml">22459</ref>].</p>
                <p xml:id="d27_p19">There are also atypical cases. An oath from Hermopolis (?) was
                    prepared on a squarish sheet [<ref type="internal" target="17503.xml">17503</ref>]. It was written in the form of a letter
                    addressed to the <term xml:lang="lat">strategos</term> by an Alexandrian who
                    owns land in the Oxyrhynchite nome. He refers to some help provided to the local
                    authorities in order to collect taxes in kind (<term xml:lang="grc">ἐβοήθησα
                        πράξας τὸ σ[ι]τικόν</term>), before taking a formal oath to remain in
                    Hermopolis and help. This document was probably prepared by the landowner’s
                    secretary in a format that did not conform to the standard for liturgical
                    oaths.</p>
                <p xml:id="d27_p20">The above-mentioned oath made by sluice guards [<ref type="internal" target="12074.xml">12074</ref> 24 CE, Tebtunis], discounted
                    by <bibl>
                        <author>Lewis</author>
                        <ptr target="gramm:lewis1997"/>
                        <citedRange>117, n. 1</citedRange>
                    </bibl>, displays a shape typical in the Arsinoite nome, following the <term xml:lang="lat">pagina</term> format (H. 26 x W. 13cm).</p>
            </div>
            <div xml:id="d27_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d27_p21">Given the state of preservation of liturgical oaths, the layout
                    can be described only for documents from the Oxyrhynchite nome. By extension,
                    this seems to apply also to the Memphite nome [<ref type="internal" target="31834.xml">31834</ref>] and to the nome of Arabia [<ref type="internal" target="22547.xml">22547</ref>].</p>
                <p xml:id="d27_p22">Liturgical oaths are written in one block of writing [<ref type="internal" target="21618.xml">21618</ref>]; in some cases, the initial
                    address to the <term xml:lang="lat">strategos</term> is highlighted by a small
                    line spacing [<ref type="internal" target="22459.xml">22459</ref>] and a second
                    line in <term xml:lang="lat">eisthesis</term> [<ref type="internal" target="15976.xml">15976</ref>, <ref type="internal" target="16554.xml">16554</ref>]. There can also be a space between the date and the main text
                        [<ref type="internal" target="15976.xml">15976</ref>, <ref type="internal" target="16554.xml">16554</ref>].</p>
                <p xml:id="d27_p23">In the special case of [<ref type="internal" target="12074.xml">12074</ref>] (see above), the text was also written in one block, except
                    for a space between the oath and the signature.</p>
            </div>
        </body>
    </text>
</TEI>