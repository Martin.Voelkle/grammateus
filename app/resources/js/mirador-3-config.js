$(document).ready(function(){

  /* get manifests links */
  var manifest = $("#manifest").find("> a");
  
  /* set mirador only if at least one link exists */
  if(manifest.length){
    
    var mirador_windows = [];
    
    /* for each manifest, create a window object */
    manifest.each(function(index){
      var link = $(this).attr("href");
      
      /* if canvas ID (as for nyu bookplates) then create the setting */
      var canvasID = "";
      
      /* assuming one canvasID */
      if ($(this).children(".canvasID")) {
        canvasID = $(this).children(".canvasID").attr("data-url"); 
      };
      
      var settings = 
      {
        "id": "window"+index,
        "loadedManifest": link,
        //"canvasIndex": 1,
        "canvasId": canvasID,
        "view": "single",
        "thumbnailNavigationPosition": "off",
        "allowClose": false,
	    "defaultSideBarPanel": "attribution",
	    "sideBarOpenByDefault": false,
      }
      mirador_windows.push(settings);
    });
    
    /* setting the window order */
    // TM 12132 has 3 manifests and the third one has the most prominent place
    // if there is three windows
    if(mirador_windows.length == 3){
        // move the first manifest/windows in last position (== 2, 3, 1)
        mirador_windows.push(mirador_windows.shift());
    };
    
    /* create workspace layout ? for getting side-by-side columns */
    var mirador_workspace_layout = {};
  
    /* instantiate mirador */
    var mirador = Mirador.viewer({
      "id": "mirador",
      "windows": mirador_windows,
    // workspace: {
//     layout: {
//       direction: "row",
//       first: {
//         first: "window1",
//         second: "window1",
//         direction: "column"
//       },
//       second: {
//         first: {
//           first: "window2",
//           second: "window2",
//           direction: "column"
//         },
//         second: {
//           first: "window3",
//           second: "window3",
//           direction: "column"
//         },
//         direction: "row"
//       },
//       splitPercentage: 33.33
//     }
//   },
      /*"workspace": {
        "type": "mosaic"
      },*/
      "workspaceControlPanel": {
        "enabled": false,
      }
    });
  }
  
  var mirador_viewer = {
      "id": "mirador",
      "windows": mirador_windows,
      "workspaceControlPanel": {
        "enabled": false,
      }
    };
  
});