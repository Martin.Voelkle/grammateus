xquery version "3.1";

import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare option exist:serialize "method=html media-type=text/html";

    <html>
        <ol>
            {for $d in collection($g:papyri)
                where $d//tei:handNote[2] and $d//tei:locus[@ana eq '../authority-lists.xml#subscription']
                let $tm := gramm:tm($d)
                let $cat := gramm:get-catDesc($d//tei:catRef[@n eq 'format-type'])
                order by $cat
                return
                <li>
                    <a href="https://grammateus.unige.ch/doc/{$tm}">{"TM " || $tm}</a> ({$cat})
                </li>
            }
        </ol>
    </html>
