xquery version "3.1";

module namespace compare="http://grammateus.ch/pages/compare";


import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "../gramm.xql";
import module namespace syntax="http://grammateus.ch/pages/syntax" at "syntax.xql";
import module namespace console="http://exist-db.org/xquery/console";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare function compare:options_orig($id) {
    (: create dropdown options :)
    for $cat in doc($g:taxonomy)//tei:category
    (: previously selected option is the default :)
    let $selected := $cat/@xml:id = $id
    return
        if ($selected) then
            <option selected="true" value="{$cat/@xml:id}" class="level{count($cat/ancestor::tei:category)}">{$cat/tei:catDesc/string()}</option>
        else
            <option value="{$cat/@xml:id}" class="level{count($cat/ancestor::tei:category)}">{$cat/tei:catDesc/string()}</option>
};

declare function compare:options($id) {
    (: create dropdown options :)
    for $type in doc($g:taxonomy)/tei:taxonomy/tei:category
    
    return
        <optGroup label="{$type/tei:catDesc/string()}">
            {for $cat in $type//tei:category
                (: previously selected option is the default :)
                let $selected := $cat/@xml:id = $id
            return
                if ($selected) then
                    <option selected="true" value="{$cat/@xml:id}">{$cat/tei:catDesc/string()}</option>
                else
                    <option value="{$cat/@xml:id}">{$cat/tei:catDesc/string()}</option>
            }
        </optGroup>
        
};

declare function compare:form($pos) {
    let $default := request:get-parameter("cat"||$pos, ())
    return
    <div class="col-6">
        <!-- drop down menu with taxonomy catDesc -->
        <!--<label for="select{$pos}">Select category {$pos}:</label>-->
        <select class="form-control compare" id="select{$pos}" name="cat{$pos}">
            <option value="" style="color:gray;">Select category {$pos}</option>
            {compare:options($default)}
        </select>
    </div>
};

declare function compare:max($papyri, $dim) {
    max($papyri//tei:*[name() = $dim]) 
};

declare function compare:min($papyri, $dim) {
    min($papyri//tei:*[name() = $dim])
};

declare function compare:avg($papyri, $dim) {
    round(avg($papyri//tei:*[name() = $dim]))
};

declare function compare:get-papyri($id) {
    for $doc in collection($g:papyri)
    where $doc//tei:catRef[matches(@target, $id)]
    return $doc
};

declare function compare:get-descr($id) {
    for $doc in collection($g:descr)
    where $doc//tei:catRef[matches(@target, $id)]
    return $doc 
};

declare function compare:size-svg($id) {
    let $papyri := compare:get-papyri($id)
    let $hmin := compare:min($papyri, "height")
    let $havg := compare:avg($papyri, "height")
    let $hmax := compare:max($papyri, "height")
    let $wmin := compare:min($papyri, "width")
    let $wavg := compare:avg($papyri, "width")
    let $wmax := compare:max($papyri, "width")
    return
    <svg width="300" height="340">
	    <defs>
            <!-- arrowhead marker definition -->
            <marker id="arrow" viewBox="0 0 10 10" refX="5" refY="5"
                markerWidth="6" markerHeight="6"
                orient="auto-start-reverse">
                <path d="M 0 0 L 10 5 L 0 10 z" />
            </marker>
    </defs>
    
    <text x="0" y="20">cm</text>
    
    <!--width-->
    <text x="60" y="20">{$wmin}</text>
    <text x="160" y="20" text-anchor="middle">{$wavg}</text>
    <text x="260" y="20" text-anchor="end">{$wmax}</text>
    
    <line x1="60" y1="35" x2="255" y2="35" stroke="grey" stroke-width="2" marker-end="url(#arrow)"/>
    
    <!--height-->
    <text x="0" y="60">{$hmin}</text>
    <text x="0" y="160">{$havg}</text>
    <text x="0" y="250">{$hmax}</text>
    
    <line x1="45" y1="50" x2="45" y2="245" stroke="grey" stroke-width="2" marker-end="url(#arrow)"/>
   
    <!--rectangle-->
    <!--<rect x="60" y="50" width="200" height="200" style="fill:rgb(255,255,255);stroke-width:3;stroke:rgb(0,0,0)" />-->
</svg>
};

declare function compare:size-txt($id, $dim) {
    let $papyri := compare:get-papyri($id)
    let $max := compare:max($papyri, $dim)
    let $min := compare:min($papyri, $dim)
    let $avg := compare:avg($papyri, $dim)
    return
    <ul>
        <li>Minimum: {$min} cm</li>
        <li>Average: {$avg} cm</li>
        <li>Maximum: {$max} cm</li>
    </ul>
};

declare function compare:category($id, $n) {
    let $category := doc($g:taxonomy)//id($id)
    let $name := $category/tei:catDesc/string()
    let $level := if (doc($g:taxonomy)//id($id)[not(parent::tei:category)]) then "type"
        else if (doc($g:taxonomy)//id($id)[parent::tei:category] and doc($g:taxonomy)//id($id)/parent::tei:category[not(parent::tei:category)]) then "subtype"
        else "variation"
    return
        (
    <div>
        <h3 class="h3">{$name}</h3>
        <p>{
            switch ($level)
            case "subtype" 
                return doc($g:taxonomy)//id($id)/parent::tei:category/tei:catDesc/string() 
                        || " > " 
                        || $name
            case "variation" 
                return doc($g:taxonomy)//id($id)/parent::tei:category/parent::tei:category/tei:catDesc/string() 
                        || " > " 
                        || doc($g:taxonomy)//id($id)/parent::tei:category/tei:catDesc/string()
                        ||" > " 
                        || $name
            default return 
                (: keep only capital intials from the Type level - EE / TI /OS / RI :)
                "(" || replace($category/tei:catDesc/string(), "[a-z\s]", "") || ")"
        }</p>
    </div>,
    <div>
        <h5 class="h5">Links</h5>
        <p>
            <ol>
                <li>
                    <a href="{$g:root}papyri.html?facet:f-{$level}={$category/tei:catDesc/normalize-space(string())}&amp;">Papyri</a> ({count(compare:get-papyri($id))})</li>
                <li>
                    <a href="{$g:root}descriptions/{$category/tei:catDesc/tei:ptr[@type eq 'descr']/replace(@target/string(), ".xml", "")}">Description</a>
                </li>
                <li>
                    <a href="{$g:root}classification/{$category/tei:catDesc/tei:ptr[@type eq 'class']/replace(@target/string(), ".xml", "")}">Classification</a>
                </li>
            </ol>
        </p>
    </div>,
    <div>
        <h5 class="h5">Dimensions</h5>
        <p>Minimum, average and maximum.</p>
        <div>{compare:size-svg($id)}</div>
        
        <h5 class="h5">Syntax elements</h5>
        <div>{syntax:table("cat"||$n, compare:get-descr($id), $id)}</div>
    </div>
        )
};

declare function compare:result() {
    (: get categories :)
    let $cat1 := request:get-parameter("cat1", ())
    let $cat2 := request:get-parameter("cat2", ())
    
    let $note := if($cat1 or $cat2) then
        <div class="alert alert-primary" role="alert">
            <i class="fas fa-exclamation-circle"/><b> Note on the tables</b>
            <p>Types and subtypes include all formulas from their subcategories: for instance the table for the private letter includes as well the formulas from Letter of Condolence and Recommendation.</p>
            <p>The formulas are ordered by text section, which is very roughly the order in which sections appear in a text (there are always exceptions). The date is a movable element and its position is indicated directly in the formulas with [date@start] or [date@end].</p>
            <p>Some formulas are alternatives, i.e. there is in general a single opening salutation in a text. However this distinction does not appear in the table - for context, click on the description link.</p>
            <p>The content of the table relies on the text of the descriptions. If a description states that a type of letter has the same opening formula as a private letter without explicitly stating the formula, it will be missing from the table as well. The description is ultimately the reference to understand the structure of a category of document.</p>
            <p>You can find all the formulas gathered in the <a href="{$g:root}syntax">Syntax</a> page.</p>
        </div>
        else ()
    
    return
    (<div class="row">
        <div class="col">
            {if ($cat1) then compare:category($cat1, "1") else ()}
        </div>
        <div class="col">
            {if ($cat2) then compare:category($cat2, "2") else ()}
        </div>
    </div>,
    $note
    )
        
};

declare function compare:page() {
    <div>
        <h1 class="h1">Compare</h1>
        <p>Compare two categories of Greek documentary papyri.</p>
        <form action="{$g:root}compare" method="get">
            <div class="row">
                {
                    (:  first dropdown :)
                    compare:form("1")
                }
                {
                    (:  second dropdown :)
                    compare:form("2")
                }
            </div>
            <button type="submit" class="btn btn-secondary" style="margin: 10px 0 10px 0">Go!</button>
        </form>
        {compare:result()}
    </div>
};