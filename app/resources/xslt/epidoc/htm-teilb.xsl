<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id$ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.tei-c.org/ns/1.0" xmlns:EDF="http://epidoc.sourceforge.net/ns/functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="#all" version="2.0">
    <!-- Actual display and increment calculation found in teilb.xsl -->
    <xsl:import href="teilb.xsl"/>

    <xsl:template match="t:lb">
        <xsl:param name="parm-edn-structure" tunnel="yes" required="no"/>
        <xsl:param name="parm-edition-type" tunnel="yes" required="no"/>
        <xsl:param name="parm-leiden-style" tunnel="yes" required="no"/>
        <xsl:param name="parm-line-inc" tunnel="yes" required="no"/>
        <xsl:param name="parm-verse-lines" tunnel="yes" required="no"/>
        <xsl:param name="location"/>

        <xsl:choose>
            <xsl:when test="ancestor::t:lg and $parm-verse-lines = 'on'">
                <xsl:apply-imports/>
                <!-- use the particular templates in teilb.xsl -->
            </xsl:when>

            <xsl:otherwise>
                <xsl:variable name="div-loc">
                    <xsl:for-each select="ancestor::t:div[@type = 'textpart']">
                        <xsl:value-of select="@n"/>
                        <xsl:text>-</xsl:text>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="line">
                    <xsl:if test="@n">
                        <!-- EN: line counter. It does not count lines that are not counted as lines of text in papyri.info 
                        -lines which are encoded twice (because within a choice/rdg/del element, 
                        -lines which contains 
                            *only one or more seal, 
                            *or only a vacat, 
                            *or only a gap of @extent unknown and @unit line,
                            *or only a gap of @unit line, @reason illegible (traces) e.g. 22479?
                            *or only a gap of @unit line, @reason lost and @precision low (ca. traces)
                            *or only a note (219272)
                            *or only a milestone of @rend box, @unit undefined (e.g. 8789)
                            *or lines with a letter from a to l (e.g 33a), but not 35,ms which should be counted (TM 30321) => matches letter that is not m
                            *or lines with a slash (e.g. 32/33), as they are not a unique line but a combination, and not recommended and likely to disappear (TM12078)
                        -lines which are in the margins. 
                            *or lines with a note (e.g. 219272) = lb followed by note, followed by lb or spaces+lb
                            *or lines with nb 00, because they are not edited in printed ed.
                            *or lines with only a crux (g @type stauros), e.g TM 22009, 37866, 22010 
                        -->
                        
                        <!--lines with a single X (e.g. 45254, 45255) are not counted on papyri.info
                            but we need to include them to highlight official marks.
                            or following-sibling::*[1][self::t:g[@type = 'x']] -->
                        <xsl:number count="t:lb[not(                         ancestor::t:reg                         or ancestor::t:corr                         or ancestor::t:rdg                         or ancestor::t:del[parent::t:subst]                         or following-sibling::node()[1][self::t:space][following-sibling::node()[1][self::t:lb]]                         or following-sibling::*[1][self::t:figure][not(following-sibling::*)]                         or following-sibling::*[1][self::t:figure][following-sibling::*[1][self::t:lb]]                         or following-sibling::*[1][self::t:gap[@extent = 'unknown' and @unit = 'line']]                         or following-sibling::*[1][self::t:gap[@unit = 'line' and @reason = 'illegible']]                         or following-sibling::*[1][self::t:gap[@unit = 'line' and @reason = 'lost' and @precision = 'low']]                         or following-sibling::node()[1][self::t:note][following-sibling::node()[1][self::t:lb] or following-sibling::node()[1][self::text() and matches(., '^\s+$') and following-sibling::node()[1][self::t:lb]]]                         or matches(@n, '^00$')                         or following-sibling::*[1][self::t:milestone[@rend = 'box' and @unit = 'undefined']]                         or matches(@n, '[abc]{1}')                         or matches(@n, '[/]')                         or ancestor::t:div[@n = 'ms']                         or following-sibling::node()[1][self::t:g and @type eq 'stauros'][following-sibling::node()[1][self::t:lb] or following-sibling::node()[1][self::text() and matches(., '^\s+$') and following-sibling::node()[1][self::t:lb]]]                         )]" level="any"/>                   
                    </xsl:if>
                </xsl:variable>

                <!--EN: trying to count missing lines. 
            If there is an <lb n="x"><gap reason="lost" unit="lines" quantity="x"/> => add x-1 to the lines variable -->
                <xsl:variable name="missing-lines-counter">
                    <xsl:if test="@n">
                        <xsl:value-of select="sum(preceding::t:gap[@unit = 'line' and @reason = 'lost' and @quantity and not(@precision = 'low')]/@quantity, 0) - count(preceding::t:gap[@unit = 'line' and @reason = 'lost' and @quantity and not(@precision = 'low')])"/>
                    </xsl:if>
                </xsl:variable>

                <!-- print hyphen if break=no  -->
                <xsl:if test="(@break = 'no' or @type = 'inWord')">
                    <xsl:choose>
                        <!--    *unless* edh web  -->
                        <xsl:when test="$parm-leiden-style = 'eagletxt'"/>
                        <!--    *unless* diplomatic edition  -->
                        <xsl:when test="$parm-edition-type = 'diplomatic'"/>
                        <!--    *or unless* the lb is first in its ancestor div  -->
                        <xsl:when test="generate-id(self::t:lb) = generate-id(ancestor::t:div[1]/t:*[child::t:lb][1]/t:lb[1])"/>
                        <xsl:when test="$parm-leiden-style = 'ddbdp' and ((not(ancestor::*[name() = 'TEI'])) or $location = 'apparatus')"/>
                        <!--   *or unless* the second part of an app in ddbdp  -->
                        <xsl:when test="($parm-leiden-style = 'ddbdp' or $parm-leiden-style = 'sammelbuch') and (ancestor::t:corr or ancestor::t:reg or ancestor::t:rdg or ancestor::t:del[parent::t:subst])"/>
                        <!--  *unless* previous line ends with space / g / supplied[reason=lost]  -->
                        <!-- in which case the hyphen will be inserted before the space/g r final ']' of supplied
                     (tested by EDF:f-wwrap in teig.xsl, which is called by teisupplied.xsl, teig.xsl and teispace.xsl) -->
                        <xsl:when test="preceding-sibling::node()[1][local-name() = 'space' or local-name() = 'g' or (local-name() = 'supplied' and @reason = 'lost') or (normalize-space(.) = '' and preceding-sibling::node()[1][local-name() = 'space' or local-name() = 'g' or (local-name() = 'supplied' and @reason = 'lost')])]"/>
                        <!-- *or unless* this break is accompanied by a paragraphos mark -->
                        <!-- in which case the hypen will be inserted before the paragraphos by code in htm-teimilestone.xsl -->
                        <xsl:when test="preceding-sibling::node()[not(self::text() and normalize-space(self::text()) = '')][1]/self::t:milestone[@rend = 'paragraphos']"/>
                        <xsl:otherwise>
                            <xsl:text>-</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>

                <xsl:choose>
                    <xsl:when test="$parm-leiden-style = ('edh-itx', 'edh-names')">
                        <xsl:variable name="cur_anc" select="generate-id(ancestor::node()[local-name() = 'lg' or local-name() = 'ab'])"/>
                        <xsl:if test="preceding::t:lb[1][generate-id(ancestor::node()[local-name() = 'lg' or local-name() = 'ab']) = $cur_anc]">
                            <xsl:choose>
                                <xsl:when test="$parm-leiden-style = 'edh-names' and not(@break = 'no' or ancestor::t:w | ancestor::t:name | ancestor::t:placeName | ancestor::t:geogName)">
                                    <xsl:text> </xsl:text>
                                </xsl:when>
                                <xsl:when test="$parm-leiden-style = ('edh-names')"/>
                                <xsl:when test="@break = 'no' or ancestor::t:w | ancestor::t:name | ancestor::t:placeName | ancestor::t:geogName">
                                    <xsl:text>/</xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text> / </xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>

                    <xsl:when test="$parm-leiden-style = 'eagletxt'">
                        <xsl:variable name="cur_anc" select="generate-id(ancestor::node()[local-name() = 'lg' or local-name() = 'ab'])"/>
                        <xsl:if test="preceding::t:lb[1][generate-id(ancestor::node()[local-name() = 'lg' or local-name() = 'ab']) = $cur_anc]">

                            <xsl:choose>
                                <xsl:when test="not(@break = 'no' or ancestor::t:w | ancestor::t:name | ancestor::t:placeName | ancestor::t:geogName)">
                                    <xsl:text> / </xsl:text>
                                </xsl:when>
                                <xsl:when test="@break = 'no' or ancestor::t:w | ancestor::t:name | ancestor::t:placeName | ancestor::t:geogName">
                                    <xsl:text>/</xsl:text>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:if>
                    </xsl:when>

                    <xsl:otherwise>
                        <xsl:text>
</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>



                <!-- print arrows right of line if R2L or explicitly L2R -->
                <!-- arrows after final line handled in htm-teiab.xsl -->
                <xsl:if test="not($parm-leiden-style = ('ddbdp', 'sammelbuch')) and not(position() = 1) and preceding::t:lb[1][@rend = 'left-to-right']">
                    <xsl:text>  →</xsl:text>
                </xsl:if>
                <xsl:if test="not($parm-leiden-style = ('ddbdp', 'sammelbuch')) and not(position() = 1) and preceding::t:lb[1][@rend = 'right-to-left']">
                    <xsl:text>  ←</xsl:text>
                </xsl:if>

                <xsl:choose>
                    <!-- replaced test using generate-id() with 'is' -->
                    <xsl:when test="self::t:lb is ancestor::t:div[1]/t:*[child::t:lb][1]/t:lb[1]">
                        <!-- EN: removed a{$div-loc} -->
                        <xsl:choose>
                            <xsl:when test="ancestor::t:div[@n = 'ms' or @n = 'r,ms']">
                                <a id="ms-l{@n}">
                                    <xsl:comment>0</xsl:comment>
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <a id="l{number(replace(string(number($line)), 'NaN', '0'))+number($missing-lines-counter)}">
                                    <xsl:comment>0</xsl:comment>
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>

                        <!-- for the first lb in a div, create an empty anchor instead of a line-break -->
                    </xsl:when>
                    <xsl:when test="($parm-leiden-style = 'ddbdp' or $parm-leiden-style = 'sammelbuch') and (ancestor::t:sic or ancestor::t:reg or ancestor::t:rdg or ancestor::t:del[ancestor::t:choice]) or ancestor::t:del[@rend = 'corrected'][parent::t:subst]">
                        <xsl:choose>
                            <xsl:when test="@break = 'no' or @type = 'inWord'">
                                <xsl:text>|</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text> | </xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="$parm-leiden-style = 'ddbdp' and ((not(ancestor::*[name() = 'TEI'])) or $location = 'apparatus')">
                        <xsl:choose>
                            <xsl:when test="@break = 'no' or @type = 'inWord'">
                                <xsl:text>|</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text> | </xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- EN: remove textpart/div-loc information. do not use counter for margin text, but line number from @n -->
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="ancestor::t:div[@n = 'ms' or @n = 'r,ms']">
                                <br id="ms-l{@n}"/>
                            </xsl:when>
                            <!-- assuming vacats do not happen in margin text -->
                            <!--<xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="following-sibling::*[1][self::t:space][following-sibling::*[1][self::t:lb]]">
                                    <br id="l{number($line)+number($missing-lines-counter)}" class="vacat"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <br id="l{number($line)+number($missing-lines-counter)}"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>-->
                            <xsl:otherwise>
                                <br id="l{number($line)+number($missing-lines-counter)}"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="$location = 'apparatus'"/>
                    <xsl:when test="not(number(@n)) and ($parm-leiden-style = 'ddbdp' or $parm-leiden-style = 'sammelbuch')">
                        <!--         non-numerical line-nos always printed in DDbDP         -->
                        <xsl:call-template name="margin-num"/>
                    </xsl:when>
                    <xsl:when test="number(@n) and @n mod number($parm-line-inc) = 0 and not(@n = 0) and not(following::t:*[1][local-name() = 'gap' or local-name() = 'space'][@unit = 'line'] and ($parm-leiden-style = 'ddbdp' or $parm-leiden-style = 'sammelbuch'))">
                        <!-- prints line-nos divisible by stated increment, unless zero
                     and unless it is a gap line or vacat in DDbDP -->
                        <xsl:call-template name="margin-num"/>
                    </xsl:when>
                    <xsl:when test="$parm-leiden-style = 'ddbdp' and preceding-sibling::t:*[1][local-name() = 'gap'][@unit = 'line']">
                        <!-- always print line-no after gap line in ddbdp -->
                        <xsl:call-template name="margin-num"/>
                    </xsl:when>
                    <xsl:when test="$parm-leiden-style = 'ddbdp' and following::t:lb[1][ancestor::t:reg[following-sibling::t:orig[not(descendant::t:lb)]]]">
                        <!-- always print line-no when broken orig in line, in ddbdp -->
                        <xsl:call-template name="margin-num"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="margin-num">
        <xsl:param name="parm-leiden-style" tunnel="yes" required="no"/>
        <xsl:choose>
            <!-- don't print marginal line number inside tags that are relegated to the apparatus (ddbdp) -->
            <xsl:when test="$parm-leiden-style = 'eagletxt'"/>
            <xsl:when test="($parm-leiden-style = 'ddbdp' or $parm-leiden-style = 'sammelbuch') and (ancestor::t:sic or ancestor::t:reg or ancestor::t:rdg or ancestor::t:del[ancestor::t:choice]) or ancestor::t:del[@rend = 'corrected'][parent::t:subst]"/>
            <xsl:otherwise>
                <span>
                    <xsl:choose>
                        <xsl:when test="$parm-leiden-style = 'ddbdp' and following::t:lb[1][ancestor::t:reg[following-sibling::t:orig[not(descendant::t:lb)]]]">
                            <xsl:attribute name="class">
                                <xsl:text>linenumberbroken</xsl:text>
                            </xsl:attribute>
                            <xsl:attribute name="title">
                                <xsl:text>line-break missing in orig</xsl:text>
                            </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="class">
                                <xsl:text>linenumber</xsl:text>
                            </xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:value-of select="@n"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>