$(document).ready(function () {
            $('#formulas').DataTable({
                //default order
                order: [[5, "asc"],[1, 'asc']],
                // hide ID column
                "columnDefs": [
                    { "visible": false, "targets": [7] }
                ]
            });
        });