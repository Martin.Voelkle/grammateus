<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d40">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Sitologos Receipt</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:bwzzphoysjduja5qclcjmhhmqa</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_os_receipt"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_sitologos"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2021-03-02">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2020-11-20">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Sitologos Receipt</head>
            <p xml:id="d40_p1">The <term xml:lang="lat">sitologos</term> was the official in charge
                of the administration of taxes paid in-kind on different categories of land. The
                office was a liturgical one in the Roman period (see [<ref type="descr" target="#gt_nomination">Nomination to Liturgy</ref>] and <bibl>
                    <author>Lewis</author>
                    <ptr target="gramm:lewis1997"/>
                    <citedRange>45</citedRange>
                </bibl>), and the <term xml:lang="lat">sitologos</term> was responsible for the
                local granary (<term xml:lang="grc">θησαυρός</term>) of a particular area. The <term xml:lang="lat">sitologos</term> received tax grain directly from the threshing
                floors, issued receipts, and provided a regular report to the <term xml:lang="lat">strategos</term> (see [<ref type="internal" target="21360.xml">21360</ref> 223 CE,
                Oxyrhynchus] and [<ref type="internal" target="16560.xml">16560</ref> 242 CE,
                Oxyrhynchus] for a list of duties, usually performed by a subordinate). For an
                overview of the collection of grain tax see <bibl>
                    <author>Wallace</author>
                    <ptr target="gramm:wallace1938"/>
                    <citedRange>31-46</citedRange>
                </bibl>.</p>
            <p xml:id="d40_p2">While the <term xml:lang="lat">sitologos</term> was responsible for
                this public service, there was also a large stock of private grain held in public
                granaries which he administered, enabling <q>giro</q> type transfers into and out of
                private stocks or accounts, rather like a bank. Indeed [<ref type="descr" target="#gt_order">Order to Pay</ref>] addressed to a <term xml:lang="lat">sitologos</term> from a private individual are composed in the same way as
                those addressed to a banker. </p>
            <p xml:id="d40_p3">This overview contains a description of <term xml:lang="lat">sitologos</term> receipts issued for the payment of land taxes, as well as
                receipts issued for some private transactions, i.e. <term xml:lang="lat">metrema</term> receipts and <term xml:lang="lat">diastolikon</term> receipts,
                the latter issued following an original order to transfer an amount from one account
                to another for the payment of rent, dues, or other payments. For more information on
                these private receipts, see <bibl>
                    <author>Litinas</author>
                    <ptr target="gramm:litinas2007"/>
                </bibl> with bibliography. Receipts issued by the <term xml:lang="lat">sitologos</term> for the payment of tax on catoecic land are treated
                separately, see [<ref type="anchor" target="#monartabos">monartabos tax</ref>]. </p>
            <div xml:id="d40_div1_structure" type="section" n="1">

                <head xml:id="structure">Structure</head>
                <p xml:id="d40_p4">Taxes on land were assessed according to the type of land and the
                    size of the harvest. Official tax receipts carry the date of issue at the
                    beginning of the document followed by a statement from the <term xml:lang="lat">sitologoi</term>, usually with the main verb in the first person plural,
                    e.g. [<ref type="internal" target="12198.xml">12198</ref> II CE, Karanis; <ref type="internal" target="12333.xml">12333</ref> II CE, Tebtunis]. <list type="unordered">
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#date" xml:id="d40_s1">[date@start]</seg>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d40_s2">[<hi rend="italic">names</hi> &lt;nom.&gt; (<term xml:lang="grc">σιτολόγοι καὶ μέτοχοι</term>)]</seg>
                            <gloss corresp="#d40_s2">
                                <term xml:lang="lat">sitologoi</term> and
                                associates</gloss>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s3">[<term xml:lang="grc">μεμετρήμεθα</term>]</seg>
                            <term xml:lang="grc" corresp="#d40_s3" xml:id="d40_t1">μεμετρήμεθα...
                                πυροῦ μέτρῳ δημοσίῳ</term>
                            <gloss corresp="#d40_t1 #d40_s3">we have received by public
                                measure</gloss> [<ref target="10210.xml" type="internal" corresp="#d40_s3 #d40_t1">10210</ref> II CE, Theadelphia] or <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s4">[<term xml:lang="grc">ἐμετρήθησαν</term>]</seg>
                                [<ref type="internal" target="15078.xml" corresp="#d40_s4">15078</ref> II CE, Herakleia]</item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s5">[year of assessment]</seg>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s6">[for <hi rend="italic">name</hi> &lt;dat.&gt; (<hi rend="italic">taxpayer</hi>)]</seg>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s7">[<hi rend="italic">amount</hi> &lt;acc.&gt;]</seg>
                            written first in words and then numerically.</item>
                        <item>A statement of transmission may be added by the <term xml:lang="lat">sitologos</term> in a second hand: <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d40_s8">[<hi rend="italic">name</hi> &lt;nom&gt; <term xml:lang="grc">σιτολόγος</term>
                                <term xml:lang="grc">μεμέτρημαι ὡς πρόκειται</term>]</seg>
                            <gloss corresp="#d40_s8">I have received as stated</gloss> [<ref type="internal" target="11256.xml" corresp="#d40_s8">11256</ref> II
                            CE, Arsinoite nome].</item>
                    </list>
                </p>
                <!-- syngraphe formulas in §5. -->
                <p xml:id="d40_p5">Some Ptolemaic sitologos receipts are composed as objective
                    statements in the form of a [<ref type="descr" target="#gt_syngr">syngraphe</ref>], with an infinitive construction, e.g. [<ref type="internal" target="43263.xml">43263</ref> II BCE, Soknopaiou Nesos;<ref type="internal" target="3124.xml">3124</ref> II BCE, Hermopolite nome; <ref type="internal" target="22229.xml">22229</ref> I CE, Oxyrhynchus]:
                    [date@start] [<term xml:lang="grc">ὁμολογεῖ</term>...<term xml:lang="grc">μεμετρῆ(σθαι)</term>] [<term xml:lang="grc">παρὰ</term>
                    <hi rend="italic">name</hi> &lt;gen.&gt; (<hi rend="italic">taxpayer</hi>)], followed by an
                        [<hi rend="italic">amount</hi> &lt;acc.&gt;] written first in words and then
                    numerically. An <term xml:lang="grc">ἀντιγραφεύς</term> or clerk signs to say
                    that he has checked the amount and adds a [date@end].</p>
                <p xml:id="d40_p6">Receipts issued for private transactions (<term xml:lang="lat">metrema</term> receipts) can be distinguished by the use of the verb in the
                    third person <term xml:lang="grc">μεμέ(τρηται)</term>, e.g. [<ref type="internal" target="78619.xml">78619</ref> II CE, Oxyrhynchite nome],
                    although the abbreviation has been resolved variously as <term xml:lang="grc">μεμέ(τρηνται)</term> and <term xml:lang="grc">μεμέ(τρηκεν)</term>; on this
                    see <bibl>
                        <author>Litinas</author>
                        <ptr target="gramm:litinas2007"/>
                        <citedRange>196 n.3, 197 n.7</citedRange>
                    </bibl>. There is usually no date of issue for this receipt, but there is always
                    mention made of the year of the harvest from which the amount is to be taken.
                        <list type="unordered">
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s14">[<term xml:lang="grc">μεμέτρηται</term>]</seg>
                            <term xml:lang="grc" xml:id="d40_t2" corresp="#d40_s14">μεμέτρηται εἰς
                                τὸ δημόσιον πυροῦ γενήματος</term> (date of harvest) <gloss corresp="#d40_t2">measured into the public granary from the crop of
                                year x</gloss> [<ref type="internal" target="114271.xml" corresp="#d40_s14 #d40_t2">114271</ref> 211 Oxyrhynchite
                            nome].</item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s15">[through <term xml:lang="grc">διὰ σιτολόγος</term>
                                <hi rend="italic">name</hi> &lt;gen.&gt;][<hi rend="italic">place</hi> &lt;gen.&gt;]</seg>
                            <gloss corresp="#d40_s15">through the <term xml:lang="lat">sitologos</term>
                                <hi rend="italic">N</hi> of (village/district)</gloss>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s16">[<hi rend="italic">name</hi> &lt;nom.&gt; (<hi rend="italic">depositor</hi>)]</seg>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s17">[<hi rend="italic">amount</hi> &lt;nom.&gt;]</seg>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d40_s18">[<hi rend="italic">name</hi> &lt;nom&gt; <term xml:lang="grc">σιτολόγος</term>] [<term xml:lang="grc">σεσημείωμαι</term>]</seg>
                            <gloss corresp="#d40_s18">I <hi rend="italic">N</hi>
                                <term xml:lang="lat">sitologos</term> have signed</gloss>.</item>
                    </list>
                </p>
                <p xml:id="d40_p7">Other receipts are structured around the verb <term xml:lang="grc">διαστέλλω</term>. These <term xml:lang="lat">diastolikon</term> receipts record a transfer of grain from privately held
                    stocks in the public granary; they follow an original order to pay addressed to
                    the <term xml:lang="lat">sitologos</term> (see [<ref type="descr" target="#gt_order">Order to Pay</ref>]). They are almost identical in
                    structure to the <term xml:lang="lat">metrema</term> receipts, but record a
                    (virtual) transfer of grain rather than a payment, and name both the payer and
                    payee, e.g. [<ref type="internal" target="114268.xml">114268</ref> 175-176 CE,
                    Oxyrhynchite nome; <ref target="15653.xml" type="internal">15653</ref> 216 CE,
                    Oxyrhynchus]. <list type="unordered">
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s19">[<term xml:lang="grc">διεστάλ(η)/διέσταλ(ται)</term>]</seg>
                            <term xml:lang="grc" xml:id="d40_t3" corresp="#d40_s19">διεστάλ(η)/διέσταλ(ται) πυροῦ γενήματος (date of harvest)</term>
                            <gloss corresp="#d40_t3">transferred from the crop of year x</gloss>
                                [<ref type="internal" target="114268.xml" corresp="#d40_s19 #d40_t3">114268</ref>]</item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s20">[through <term xml:lang="grc">διὰ σιτολόγος</term>
                                <hi rend="italic">name</hi> &lt;gen.&gt;][<hi rend="italic">place</hi> &lt;gen.&gt;]</seg>
                            <gloss corresp="#d40_s20">through the <term xml:lang="lat">sitologos</term>
                                <hi rend="italic">N</hi> of (village/district)</gloss>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s21">[from <term xml:lang="grc">ἀπὸ θέματος</term>
                                <hi rend="italic">name</hi> &lt;gen.&gt; (<hi rend="italic">payer</hi>)] [to <hi rend="italic">name</hi> &lt;dat.&gt; (<hi rend="italic">payee</hi>)]</seg>
                            <gloss corresp="#d40_s21">from the deposit of <hi rend="italic">N</hi>
                                to <hi rend="italic">N</hi>
                            </gloss>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s22">[<hi rend="italic">amount</hi>
                            &lt;acc.&gt;]</seg>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d40_s23">[<hi rend="italic">name</hi> &lt;nom&gt; <term xml:lang="grc">σιτολόγος</term>] [<term xml:lang="grc">σεσημείωμαι</term>]</seg>
                            <gloss corresp="#d40_s23">I <hi rend="italic">N</hi>
                                <term xml:lang="lat">sitologos</term> have signed</gloss>.</item>
                    </list>
                </p>
            </div>
            <div xml:id="d40_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d40_p8">All shapes of papyri are found: <term xml:lang="lat">pagina</term> format e.g. [<ref type="internal" target="11653.xml">11653</ref> III CE, Herakleia; <ref type="internal" target="10210.xml">10210</ref> II CE, Theadelphia; <ref type="internal" target="22229.xml">22229</ref>] (horizontal fibres), [<ref type="internal" target="11812.xml">11812</ref> II CE, Nilopolis] (vertical fibres), <term xml:lang="lat">transversa charta</term> [<ref type="internal" target="15943.xml">15943</ref> III CE, Oxyrhynchus], or in this orientation with horizontal
                    fibres [<ref type="internal" target="11256.xml">11256</ref>, <ref type="internal" target="15048.xml">15048</ref>], and squarish [<ref type="internal" target="9329.xml">9329</ref> II CE, Herakleia] (horizontal
                        fibres),[<ref type="internal" target="11732.xml">11732</ref> III CE,
                    Nilopolis] (vertical fibres). The sheet with the single receipt of [<ref type="internal" target="3124.xml">3124</ref>] has a large left- hand margin
                    to which is attached part of another blank sheet, and the same is apparent at
                    the right-hand edge. Another receipt also has a blank sheet attached to the
                    left-hand edge [<ref type="internal" target="9562.xml">9562</ref> II CE,
                    Euhemeria], and there are <term xml:lang="lat">kolleseis</term> visible on [<ref type="internal" target="20672.xml">20672</ref> II CE, Oxyrhynchus] and [<ref type="internal" target="78622.xml">78622</ref> II CE, Oxyrhynchite
                    nome].</p>
            </div>
            <div xml:id="d40_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d40_p9">The text of these receipts is usually laid out on the sheet in a
                    single block, often with an enlarged first letter e.g. <term xml:lang="grc">ἔτους</term> [<ref type="internal" target="11732.xml">11732</ref>; <ref type="internal" target="9314.xml">9314</ref> III CE, Karanis], <term xml:lang="grc">διεστάλ(ησαν)</term> [<ref type="internal" target="16905.xml">16905</ref> I CE, Oxyrhynchite nome]; there is little use of <hi rend="italic">ekthesis</hi> or <hi rend="italic">eisthesis</hi>.
                    Occasionally the statement of transmission is separated from the main text [<ref type="internal" target="9513.xml">9513</ref> II CE, Philadelphia], or
                    written in <term xml:lang="lat">ekthesis</term> [<ref type="internal" target="11265.xml">11265</ref> I CE, Philadelphia]. Often there is a large
                    bottom margin, e.g. [<ref type="internal" target="10210.xml">10210</ref>; <ref type="internal" target="12199.xml">12199</ref> II CE, Karanis; <ref type="internal" target="11694.xml">11694</ref> II CE, Bakchias].</p>
                <p xml:id="d40_p10">There can be more than one receipt on a single sheet: [<ref type="internal" target="11674.xml">11674</ref> I CE, Arsinoite nome] has
                    three receipts in a single column, evenly spaced apart; [<ref type="internal" target="78622.xml">78622</ref>] carries ten receipts in two columns, also
                    evenly spaced apart, written by seven different hands. The second column of
                        [<ref type="internal" target="9105.xml">9105</ref> II CE, Herakleia] records
                    a money payment and is narrower than the first column, a characteristic noted in
                    letters of more than one column (see <bibl>
                        <author>Sarri</author>
                        <ptr target="gramm:sarri2018"/>
                        <citedRange>112</citedRange>
                    </bibl>).</p>
            </div>
            <div xml:id="d40_div4_monartabos" type="section" n="4">
                <head xml:id="monartabos">Monartabos Tax receipt</head>
                <head type="subtitle">Receipt for tax on catoecic land applied at the rate of one
                    artaba per aroura (<term xml:lang="grc">μονάρταβος γῆ</term>).</head>
                <p xml:id="d40_p11">The category of cleruchic or catoecic land, i.e. plots (<term xml:lang="lat">kleroi</term>) attributed to military settlers in the
                    Ptolemaic period, was still in existence in the Roman period, although it bore
                    no relation to the army anymore. It retained, however, a taxation rate distinct
                    from that applied to other categories of land. In some cases, especially in the
                    Arsinoite nome, catoecic land was described as <term xml:lang="lat">monartabos</term>, i.e. <gloss>(taxed at the rate of) one <term xml:lang="lat">artaba</term> (per <term xml:lang="lat">aroura</term>)</gloss>
                    <bibl>
                        <author>P.Köln IV</author>
                        <ptr target="gramm:kramer1982"/>
                        <citedRange>196</citedRange>
                    </bibl>. In modern editions, the corresponding tax is sometimes referred to as
                        <term xml:lang="lat">monartabia</term> [e.g. <ref type="external" target="https://papyri.info/ddbdp/p.oxy;44;3170">15932</ref> III CE, Oxyrhynchus], following <bibl>
                        <author>Wallace</author>
                        <ptr target="gramm:wallace1938"/>
                        <citedRange>13</citedRange>
                    </bibl>, but the word is nowhere attested in papyri, which carry only <term xml:lang="lat">monartabos</term> (scil. <term xml:lang="lat">kleros</term>)
                [<ref type="external" target="https://papyri.info/ddbdp/p.gen;2;116">17391</ref> l.29, 31, 247 CE; <ref type="internal" target="17008.xml">17008</ref> l.18, III CE, both
                    Oxyrhynchus] or <term xml:lang="lat">artabeia</term> [<ref type="internal" target="10941.xml">10941</ref> II CE, Theadelphia (rent receipt); <ref type="external" target="https://papyri.info/ddbdp/p.kron;;28">11549</ref> 125 CE, Tebtunis], see <bibl>
                        <author>P.Diog</author>
                        <ptr target="gramm:schubert1990"/>
                        <citedRange>131-132</citedRange>
                    </bibl>. Therefore, it seems preferable to refer here to the tax on catoecic
                    land at the rate of one <term xml:lang="lat">artaba</term> per <term xml:lang="lat">aroura</term>.</p>
                <p xml:id="d40_p12">For the most part, catoecic land consisted of wheat land. <term xml:lang="lat">Sitologoi</term> produced many kinds of receipts, but only a
                    few, scattered through the I-II CE, pertain to <term xml:lang="lat">monartabos
                        ge</term>: four of them come from the Arsinoite nome, and a fifth from the
                    Herakleopolite nome. For other types of tax on catoecic land see [<ref type="descr" target="#gt_telos">Tax on Catoecic Land: <term xml:lang="grc">τέλος καταλοχισμῶν</term>
                    </ref>].</p>
                <div xml:id="d40_div5_monartabos_structure" type="subsection" n="1">
                    <head xml:id="monartabos_structure">Structure</head>
                    <p xml:id="d40_p13">These receipts are similar in structure to other <term xml:lang="lat">sitologoi</term> receipts. However, receipts from the
                        Arsinoite and Herakleopolite nomes differ from each other: those from the
                        Arsinoite are written in the form of a statement from the <term xml:lang="lat">sitologoi</term>; the only remaining Herakleopolite
                        receipt displays an opening address in letter form.</p>
                    <p xml:id="d40_p14">Arsinoite nome [<ref type="internal" target="9349.xml" corresp="#d40_s24 #d40_s25 #d40_s26 #d40_s27 #d40_s28">9349</ref> I CE,
                        Karanis; <ref type="internal" target="9430.xml" corresp="#d40_s24 #d40_s25 #d40_s26 #d40_s27 #d40_s28">9430</ref> I CE]:
                            <list type="unordered">
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#date" xml:id="d40_s24">[date@start]</seg>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s25">[<hi rend="italic">name</hi> &lt;nom.&gt; (<hi rend="italic">sitologoi</hi>)][<term xml:lang="grc">μεμετρήμεθα</term>]</seg>
                                <term xml:lang="grc" corresp="#d40_s25" xml:id="d40_t4">μεμετρήμεθα
                                    ἀπὸ τῶν γενημάτων τοῦ αὐτοῦ ἔτους</term>
                                <gloss corresp="#d40_t4">we have received payment in kind from the
                                    crop of the same year</gloss>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s26">[<hi rend="italic">tax</hi> &lt;gen.&gt;] [<hi rend="italic">village</hi> &lt;gen.&gt;]</seg>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s27">[for <hi rend="italic">name</hi> &lt;dat.&gt;
                                        (<hi rend="italic">taxpayer</hi>)]</seg>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s28">[<hi rend="italic">amount</hi>
                                    &lt;acc.&gt;]</seg>
                            </item>
                            <item>The signature of a <term xml:lang="lat">sitologos</term> in a
                                second hand may be added at the end [<ref type="internal" target="14446.xml">14446</ref> I CE].</item>
                        </list>
                    </p>
                    <p xml:id="d40_p15">Herakleopolite nome [<ref type="internal" target="23281.xml" corresp="#d40_s29 #d40_s30 #d40_s31 #d40_s32 #d40_s33">23281</ref> I
                        CE]: <list type="unordered">
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d40_s29">[from <hi rend="italic">name</hi> &lt;nom.&gt;
                                        (<hi rend="italic">sitologoi</hi>)] [to <hi rend="italic">name</hi> &lt;dat.&gt; (<hi rend="italic">taxpayer</hi>)]
                                        <term xml:lang="grc">χαίρειν</term>
                                </seg>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s30">[<term xml:lang="grc">μεμετρήμεθα</term>] <term xml:lang="grc"> εἰς τὸ δημόσιον διά λόγου</term> [<hi rend="italic">name</hi> &lt;gen.&gt; (<hi rend="italic">village</hi>)]</seg>
                                <gloss corresp="#d40_s30">we have received payment in kind to the
                                    public treasure through the account of (<hi rend="italic">village</hi>)</gloss>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s31">[tax &lt;acc.&gt;]</seg>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s32">[<hi rend="italic">for</hi>
                                    <term xml:lang="grc">εἰς ἀρίθμησιν</term> (<hi rend="italic">period</hi>)]</seg>
                                <gloss corresp="#d40_s32">for the reckoning of the period</gloss>,
                                    <hi rend="italic">(including months and regnal
                                years)</hi>
                            </item>
                            <item>
                                <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d40_s33">[<hi rend="italic">amount</hi>
                                    &lt;acc.&gt;]</seg>
                            </item>
                        </list>
                    </p>
                </div>
                <div xml:id="d40_div6_monartabos_format" type="subsection" n="2">
                    <head xml:id="monartabos_format">Format</head>
                    <p xml:id="d40_p16">The preserved receipts do not reflect a consistent practice
                        regarding format. There seems to be a preference for the <term xml:lang="lat">pagina</term> format [<ref type="internal" target="9430.xml">9430</ref>, <ref type="internal" target="9349.xml">9349</ref>, <ref type="internal" target="23281.xml">23281</ref>], but
                        one receipt displays a squarish shape [<ref type="internal" target="14446.xml">14446</ref>]. The same applies to fibre direction:
                        contrary to the usual practice of writing in the direction of fibres, [<ref type="internal" target="9430.xml">9430</ref>] was initially cut from a
                        roll in the shape of a horizontal rectangle, then turned at a right angle in
                        order to be used in the <term xml:lang="lat">pagina</term> format (there is
                        a horizontal <term xml:lang="lat">kollesis</term> across the middle of the
                        sheet), with the writing at a right angle in relation to the fibres.</p>
                </div>
                <div xml:id="d40_div7_monartabos_layout" n="3" type="subsection">
                    <head xml:id="monartabos_layout">Layout</head>
                    <p xml:id="d40_p17">Here again, the preserved examples show little consistency.
                        The sheet may contain one [<ref type="internal" target="9349.xml">9349</ref>] or several [<ref type="internal" target="9430.xml">9430</ref>]
                        receipts; and the text may cover the whole surface of the sheet [<ref type="internal" target="14446.xml">14446</ref>], or only a smaller area,
                        leaving a blank space before and after the text [<ref type="internal" target="9349.xml">9349</ref>]. A left margin is sometimes provided [<ref type="internal" target="9430.xml">9430</ref>, <ref type="internal" target="14446.xml">14446</ref>]. When a date appears at the beginning of
                        the text, the initial <term xml:lang="lat">epsilon</term> of <term xml:lang="grc">ἔτους</term> is often enlarged.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>