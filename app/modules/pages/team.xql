xquery version "3.1";

module namespace team="http://grammateus.ch/pages/team";

import module namespace g="http://grammateus.ch/global" at "../global.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function team:page() {
    doc($g:team)//body/div
};

declare function team:team() {
    <div>
        <ul>
            {
                let $doc := doc($g:authority)
                for $member in $doc//tei:respStmt
                return
                    <li><a href="{$member//tei:ref/@target}">{$member/tei:name}</a>: {$member/tei:resp}</li>
            }
        </ul>
    </div>
};

