<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d20">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Letter of Condolence</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <author corresp="../authority-lists.xml#RLC">
                    <forename>Ruey-Lin</forename>
                    <surname>Chang</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#GB">
                        <forename>Gianluca</forename>
                        <surname>Bonagura</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName>
                    <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                    <ref target="https://data.snf.ch/grants/grant/212424">Project 212424</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:rvczy4zq45b5jiqwgac4pp2hlq</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.
                    Previous version(s) archived on Yareta:
                    <list>
                        <item>
                            Ferretti, L., Fogarty, S., Nury, E., and Schubert, P. 2023. <title>Description of Greek Documentary Papyri: Letter of Condolence</title>. grammateus project. DOI: 
                            <idno type="DOI">10.26037/yareta:qgx7o7andraaff4bctcpr7elo4</idno>
                        </item>
                    </list>
                    </p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_letterPrivate"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_condolence"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2024">Archived on Yareta</change>
            <change who="../authority_lists.xml#RLC ../authority_lists.xml#EN" when="2024">Updated with Byzantine period</change>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2021-04-01">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2021-01-30">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Letter of Condolence</head>
            <head type="subtitle">
                <term xml:lang="grc">παραμυθητική</term>
            </head>
            <p xml:id="d20_p1">This variation of the private letter is relatively rare – there are
                only 15 examples from the I – VII CE, 9 of which are from the Roman period and none
                currently extant from the Ptolemaic period. The letter of condolence was sent to
                express sympathy to another upon the death of a loved one or acquaintance; they
                refer to the deaths of both adults [<ref type="internal" target="41797.xml">41797</ref> III CE, Arsinoite nome] and children [<ref type="internal" target="28407.xml">28407</ref> II CE, Oxyrhynchus]. The work of reference for
                the letter of condolence is <bibl>
                    <author>Chapa</author>
                    <ptr target="gramm:chapa1998"/>
                </bibl>.</p>
            <p xml:id="d20_p2">The letters made use of typical phrases of consolation some of which
                reflected phrases found on contemporary epitaphs, see <bibl>
                    <author>Kotsifou</author>
                    <ptr target="gramm:kotsifou2012"/>
                    <citedRange>395-396</citedRange>
                </bibl>, as well as some literary tropes: a letter from a Roman officer to an
                Alexandrian magistrate on the death of his daughter clearly reflects the education
                of the author in this respect [<ref type="internal" target="30997.xml">30997</ref>
                III-IV CE, Hermopolis (?)].</p>
            <p xml:id="d20_p3">Once sympathy had been expressed it was not uncommon for the letters
                to then move onto more mundane matters, e.g. [<ref type="internal" target="17411.xml">17411</ref> 235 CE, Oxyrhynchus] where Mnesthianos, after
                consoling the recipient on the loss of his son, continues with news of the
                apprehension of men fleeing a liturgy and attaches a relevant document, see <bibl>
                    <author>Worp</author>
                    <ptr target="gramm:worp1995"/>
                    <citedRange>153</citedRange>
                </bibl>.</p>
            <p xml:id="d20_p4">Other letters were dedicated wholly to expressing sympathy e.g. [<ref type="internal" target="30997.xml">30997</ref>]. A letter of condolence from
                Eirene [<ref type="internal" target="28407.xml">28407</ref>] was written to Philo on
                the death of his child; separately to this, on the same day, Eirene wrote two other
                letters to Philo concerning their business affairs, thus maintaining a clear
                distinction between both communications, see <bibl>
                    <author>Kotsifou</author>
                    <ptr target="gramm:kotsifou2012"/>
                    <citedRange>398</citedRange>
                </bibl>. </p>
            <div xml:id="d20_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d20_p5">The opening formula is as for [<ref target="#gt_letterPrivate" type="descr">private letters</ref>] but sometimes [<term xml:lang="grc">χαίρειν</term>] is replaced by <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d20_s1">[<term xml:lang="grc">εὐψυχεῖν</term>]</seg>
                    <gloss corresp="#d20_s1">be of good courage</gloss> [<ref type="internal" target="28407.xml" corresp="#d20_s1">28407</ref>
                    II CE, Oxyrhynchus], or <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d20_s2">[<term xml:lang="grc">εὐθυμεῖν</term>]</seg>
                    <gloss corresp="#d20_s2">be of good heart</gloss> [<ref type="internal" target="17411.xml" corresp="#d20_s2">17411</ref>];
                    one letter ends the opening formula with <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d20_s3">[<term xml:lang="grc">εὖ
                            πράσσειν</term>]</seg>
                    <gloss>be well</gloss>, lit.<gloss>be prosperous</gloss> [<ref target="https://papyri.info/ddbdp/p.ross.georg;3;2" type="external">17952</ref> III CE, Alexandria]. The opening formula can include a <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d20_s4">[kinship
                    expression]</seg> e.g. [<ref type="internal" target="30997.xml" corresp="#d20_s4">30997</ref>]; one
                    example has the extension <term xml:lang="grc" xml:id="d20_t1">τῇ τιμιωτάτῃ πλεῖστα
                        χαίρειν</term> [<ref type="internal" target="41797.xml" corresp="#d20_t1">41797</ref>].
                    Another letter is more like the opening of a [<ref type="descr" target="#gt_businessNote">business note</ref>] than a letter of condolence:
                        [<ref type="internal" target="28089.xml" corresp="#d20_s5">28089</ref> II CE] opens with the
                    formula <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d20_s5">[to <hi rend="italic">name</hi> &lt;dat.&gt;][<hi rend="italic">from</hi>
                        <term xml:lang="grc">παρά</term>
                        <hi rend="italic">name</hi> &lt;gen.&gt;]</seg>.</p>
                <p xml:id="d20_p6">The main text was usually short with standardised phrasing
                    expressing condolence, see <bibl>
                        <author>Chapa</author>
                        <ptr target="gramm:chapa1998"/>
                        <citedRange>25-43</citedRange>
                    </bibl>, and <bibl>
                        <author>Kotsifou</author>
                        <ptr target="gramm:kotsifou2012"/>
                        <citedRange>394-396</citedRange>
                    </bibl>; two documents are unusually long [<ref type="internal" target="30997.xml">30997</ref> ; <ref target="https://papyri.info/ddbdp/p.ross.georg;3;2" type="external">17952</ref>].</p>
                <p xml:id="d20_p7">The closing formula can be <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d20_s6">[<term xml:lang="grc">ἐρρῶσθαί σε εὔχομαι</term>]</seg> [<ref type="internal" target="41797.xml" corresp="#d20_s6">41797</ref>]. There are examples where the opening and
                    closing formulae are mixed: [<ref target="https://papyri.info/ddbdp/p.ross.georg;3;2" type="external" corresp="#d20_s3 #d20_s7">17952</ref>] opens with <term xml:lang="grc">εὖ πράσσειν</term>
                    <gloss>be well</gloss> and closes the letter <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d20_s7">[<term xml:lang="grc">εὐθυμεῖ</term>]</seg>
                    <gloss corresp="#d20_s7">take heart</gloss>; [<ref type="internal" target="28407.xml" corresp="#d20_s1">28407</ref>] opens
                    with <term xml:lang="grc">εὐψυχεῖν</term>
                    <gloss>be of good courage</gloss> and closes <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d20_s8">[<term xml:lang="grc">εὖ
                            πράττετε</term>]</seg>
                    <gloss corresp="#d20_s8">be well</gloss>. Another example [<ref type="internal" target="17411.xml" corresp="#d20_s2 #d20_s6 #d20_t2">17411</ref>] appears more symmetrical in its opening and closing formulae
                    in that one clearly reflects the other: it opens with <term xml:lang="grc">εὐθυμεῖν</term> and includes the same sentiment in its rather elaborate
                    closing, <term xml:lang="grc" xml:id="d20_t2" corresp="#d20_s6">ἐρρῶσθαί σε εὔχομαι, κύριέ μου, μετὰ κυρίου μου
                        Σπαρτιάτου θεοῖς πᾶσιν εὐθυμοῦντα</term>
                    <gloss corresp="#d20_t2">I pray that you fare well, my lord, with my lord Spartiates be of good heart
                        with all the gods</gloss>, all written in the hand of the author. A date is not
                    usually present, but Eirene added a date to the end of her letter to Philo [<ref type="internal" target="28407.xml">28407</ref>]; the date at the end of
                        [<ref type="internal" target="17411.xml">17411</ref>] relates to the
                    document appended rather than the letter of condolence.</p>
            </div>
            <div xml:id="d20_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d20_p8">The surviving letters are mostly in <term xml:lang="lat">pagina</term> format with horizontal fibres; one is squarish [<ref type="internal" target="28407.xml">28407</ref>] (H. 7.9 x W. 7.6).</p>
            </div>
            <div xml:id="d20_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d20_p9">The layout is usually careful, e.g. [<ref type="internal" target="28407.xml">28407</ref>; <ref type="internal" target="30997.xml">30997</ref>; <ref type="internal" target="17411.xml">17411</ref>]. The
                    letter to the Alexandrian magistrate [<ref type="internal" target="30997.xml">30997</ref>] is probably unfinished: there is no closing greeting and the
                    document may have continued to a second column, see <bibl>
                        <author>Rea</author>
                        <ptr target="gramm:rea1986"/>
                        <citedRange>78, 20-21n.</citedRange>
                    </bibl>. There is no closing formula at the end of [<ref type="internal" target="28089.xml">28089</ref>] even though there is a large enough space to
                    accommodate one. The closing formula for the condolences of [<ref type="internal" target="17411.xml">17411</ref>], written by the author, is
                    tightly squeezed between the letter and the document enclosed.</p>
            </div>
            <div xml:id="d20_div4_byzantine" type="section" n="4">
                <head xml:id="byzantine">Byzantine Letter of Condolence</head>
                <p xml:id="d20_p10">Letters of condolence traditionally start with offering
                consolation and end in practicalities <bibl>
                    <author>Parsons</author>
                    <ptr target="gramm:parsons2007"/>
                    <citedRange>129-131</citedRange>
                </bibl>. The consolatory part is stereotyped, with
                euphemistic, fatalistic and stoical phrasing <listBibl>
                    <bibl>
                        <author>Chapa</author>
                        <ptr target="gramm:chapa1998"/>
                        <citedRange>34-43</citedRange>
                    </bibl>
                    <bibl>
                        <author>Kotsifou</author>
                        <ptr target="gramm:kotsifou2012"/>
                        <citedRange>394-396</citedRange>
                    </bibl>
                </listBibl>. This pattern is well-established, as proved by [<ref
                    target="https://papyri.info/trismegistos/78276" type="external">78276</ref> early II CE (?)], a
                Roman model letter of condolence <listBibl>
                    <bibl>
                        <author>Kotsifou</author>
                        <ptr target="gramm:kotsifou2012"/>
                        <citedRange>393</citedRange>
                    </bibl>
                    <bibl>
                        <author>Fogarty</author>
                        <ptr target="gramm:fogarty2022"/>
                        <citedRange>264</citedRange>
                    </bibl>
                </listBibl>. Concerning the
                practicalities, they consist either of enquiries as to the receiver’s needs, or
                other business that should be taken care of despite the emotional circumstances
                <bibl>
                    <author>P.Hamb. 4</author>
                    <ptr target="gramm:kramerHagedorn1998"/>
                    <citedRange>No. 254, introduction: 99</citedRange>
                </bibl>.</p>
                <p xml:id="d20_p11">So far, only one Byzantine letter of condolence is completely
                    preserved [<ref type="internal" target="35213.xml">35213</ref> V CE, Herakleopolite nome]. In terms
                    of contents, letters of condolence dating from the Byzantine period do not really
                    deviate from former practices <bibl>
                        <author>Palme</author>
                        <ptr target="gramm:palme2009"/>
                        <citedRange>362-363</citedRange>
                    </bibl>. Byzantine consolatory
                    expressions based on Christian beliefs surface in [<ref
                        target="https://papyri.info/trismegistos/32798" type="external">32798</ref> IV CE; <ref
                            target="https://papyri.info/trismegistos/37872" type="external">37872</ref> VI-VII CE,
                    Oxyrhynchus; <ref target="https://papyri.info/trismegistos/92464" type="external">92464</ref> VII
                    CE, Arsinoite or Herakleopolite nome] <bibl>
                        <author>Kotsifou</author>
                        <ptr target="gramm:kotsifou2012"/>
                        <citedRange>398-400</citedRange>
                    </bibl>. In [<ref target="https://papyri.info/trismegistos/92452" type="external"
                        >92452</ref> 2<hi rend="superscript">nd</hi> half of VI CE, Arsinoite or
                    Herakleopolite nome] and [<ref target="https://papyri.info/trismegistos/37872" type="external"
                        >37872</ref>], business comes before mourning. [<ref
                            target="https://papyri.info/trismegistos/32798" type="external">32798</ref>] is torn before any
                    practicalities can be read.</p>
                <div xml:id="d20_div5_byz_structure" type="subsection" n="5">
                    <head xml:id="byz_structure">Structure</head>
                    <p xml:id="d20_p12">Available evidence does not suggest that a Byzantine letter
                        of condolence adopts a specific structure, different from that of a [<ref type="descr" target="#gt_letterPrivate"
                            >Private Letter</ref>].</p>
                    <p xml:id="d20_p13">[<ref type="internal" target="35213.xml" corresp="#d20_s9">35213</ref>] displays an inverted epistolary
                        address  <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d20_s9"
                            >[to <hi rend="italic">name</hi> &lt;dat.&gt;] [from <hi rend="italic"
                                >name</hi> &lt;nom.&gt;]</seg> without any verb of greetings. Both [<ref
                                    target="https://papyri.info/trismegistos/33737" type="external">33737</ref> 1<hi
                                        rend="superscript">st</hi> half of IV CE, Oxyrhynchus] and [<ref
                                            target="https://papyri.info/trismegistos/32798" type="external">32798</ref>] open with this kind
                        of address, but keep [<term xml:lang="grc">χαίρειν</term>]. [<ref
                            target="https://papyri.info/trismegistos/37872" type="external">37872</ref>] may have opened
                        without a prescript, just as an ordinary Byzantine private letter  <bibl>
                            <author>Chapa</author>
                            <ptr target="gramm:chapa1998"/>
                            <citedRange>152</citedRange>
                        </bibl>.</p>
                    <p xml:id="d20_p14">The main text is usually a combination of emotions and
                        matter-of-factness. [<ref target="https://papyri.info/trismegistos/92452" type="external"
                            >92452</ref>], where consolation simply forms part of an ordinary private letter
                        set in the general structure of this type, may not be considered as a stand-alone
                        letter of condolence. In [<ref target="https://papyri.info/trismegistos/92464" type="external"
                            >92464</ref>], there is not even consolation clearly addressed to the
                        bereaved.</p>
                    <p xml:id="d20_p15">The closing is only preserved in [<ref
                        target="https://papyri.info/trismegistos/32798" type="external">32798</ref>] and
                        [<ref type="internal" target="35213.xml">35213</ref>], both still using the
                        Roman formula [<term xml:lang="grc">ἐρρῶσθαί σε εὔχομαι πολλοῖς χρόνοις</term>] <gloss>I pray that
                            you stay in good shape for many years</gloss>.</p>
                </div>
                <div xml:id="d20_div6_byz_format" type="subsection" n="6">
                    <head xml:id="byz_format">Format</head>
                    <p xml:id="d20_p16">Similarly to Byzantine private letters, letters of condolence
                        in IV-V CE continue to employ the <term xml:lang="lat">pagina</term> format with writing
                        along the fibres [<ref target="https://papyri.info/trismegistos/33737" type="external">33737</ref>;
                        <ref target="https://papyri.info/trismegistos/32798" type="external">32798</ref>; <ref type="internal" target="35213.xml">35213</ref>], while those from VI-VII CE shift to <term xml:lang="lat"
                            >transversa charta</term> with writing across the fibres [<ref
                                target="https://papyri.info/trismegistos/37872" type="external">37872</ref>; <ref
                                    target="https://papyri.info/trismegistos/92452" type="external">92452</ref>; <ref
                                        target="https://papyri.info/trismegistos/92464" type="external">92464</ref>]. The height of
                        [<ref type="internal" target="35213.xml">35213</ref>] corresponds to that of a standard roll unrolling
                        horizontally.</p>
                </div>
                <div xml:id="d20_div7_byz_layout" type="subsection" n="7">
                    <head xml:id="byz_layout">Layout</head>
                    <p xml:id="d20_p17">[<ref type="internal" target="35213.xml">35213</ref>] adopts the Roman tripartite layout
                        that distinguishes the opening, main text and closing. The second line of its
                        opening, as well as all the lines of its closing, are in <term xml:lang="lat"
                            >eisthesis</term>. Below the closing, there is a three-line addition on a
                        practical issue, set in a column as wide as the main text. The closing and addition
                        are written in a style different from that of the opening and main text.</p>
                    <p xml:id="d20_p18">The layout of [<ref
                        target="https://papyri.info/trismegistos/33737" type="external">33737</ref>] and [<ref
                            target="https://papyri.info/trismegistos/32798" type="external">32798</ref>] also sets the
                        opening apart, by putting in <term xml:lang="lat">eisthesis</term> the line containing
                        [<term xml:lang="grc">χαίρειν</term>]. The closing of [<ref target="https://papyri.info/trismegistos/32798" type="external"
                            >32798</ref>] is written downwards against the fibres in the left-hand
                        margin.</p>
                    <p xml:id="d20_p19">The initial letters of the openings and main texts of [<ref
                        target="https://papyri.info/trismegistos/33737" type="external">33737</ref>] and [<ref type="internal" target="35213.xml">35213</ref>] are enlarged and appear to be slightly in <term
                            xml:lang="lat">ekthesis</term>.</p>
                    <p xml:id="d20_p20">Although their fragmentary state of preservation does not
                        offer clear clues, [<ref target="https://papyri.info/trismegistos/37872" type="external"
                            >37872</ref>], [<ref target="https://papyri.info/trismegistos/92452" type="external">92452</ref>]
                        and [<ref target="https://papyri.info/trismegistos/92464" type="external">92464</ref>] may have
                        adopted the single-block layout, just as ordinary Byzantine private letters do.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>