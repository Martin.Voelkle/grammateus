<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d25">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Description of Greek Documentary Papyri: List and Register</title>
            <author corresp="../authority-lists.xml#SF">
               <forename>Susan</forename>
               <surname>Fogarty</surname>
            </author>
            <author corresp="../authority-lists.xml#PS">
               <forename>Paul</forename>
               <surname>Schubert</surname>
            </author>
            <respStmt>
               <resp>Encoded in TEI P5 by</resp>
               <name corresp="authority-lists.xml#EN">
                  <forename>Elisa</forename>
                  <surname>Nury</surname>
               </name>
            </respStmt>
            <respStmt>
               <resp>With the collaboration of</resp>
               <name corresp="../authority-lists.xml#LF">
                  <forename>Lavinia</forename>
                  <surname>Ferretti</surname>
               </name>
            </respStmt>
            <funder>
               <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
            </funder>
         </titleStmt>
         <publicationStmt>
            <authority>Grammateus Project</authority>
            <idno type="DOI">10.26037/yareta:xxzzr5fnzvh4xdplt6rfu3wnga</idno>
            <availability>
               <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
            </availability>
         </publicationStmt>
         <sourceDesc>
            <p>Born digital document. Encoded from a Microsoft Word document.</p>
         </sourceDesc>
      </fileDesc>
      <encodingDesc>
         <classDecl>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
         </classDecl>
         <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
         </p>
      </encodingDesc>
      <profileDesc>
         <langUsage>
            <language ident="lat">Latin</language>
            <language ident="grc">Greek</language>
         </langUsage>
         <textClass>
            <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ri"/>
            <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_list"/>
            <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_register"/>
         </textClass>
      </profileDesc>
      <revisionDesc>
         <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
         <change who="authority-lists.xml#SF" when="2022-11-08">Revised text</change>
         <change who="authority-lists.xml#EN" when="2020-05-04">TEI file created</change>
      </revisionDesc>
   </teiHeader>
   <text>
      <body>
         <head>List and Register</head>
         <p xml:id="d25_p1">A list records information by displaying a series of items in a repeated
            and consistent format. It may be informal and contain a record of personal items, such
            as household goods [<ref target="https://papyri.info/ddbdp/p.bingen;;117" type="external">44516</ref> III-IV CE, Karanis] and clothing [<ref target="https://papyri.info/ddbdp/p.oxy;44;3201" type="external">30247</ref> III CE,
            Oxyrhynchus], or it may consist of a more formal inventory [<ref target="https://papyri.info/ddbdp/p.oxy;1;109" type="external">31342</ref> III-IV CE,
            Oxyrhynchus]; see <bibl>
               <author>Husselman</author>
               <ptr target="gramm:husselman1976"/>
               <citedRange>553-554</citedRange>
            </bibl>. Some lists appear in other documents, such as a list of books appended to a
            private letter [<ref target="https://papyri.info/ddbdp/p.mil.vogl;1;11" type="external">78532</ref>, l. 12-17, II CE, Oxyrhynchus].</p>
         <p xml:id="d25_p2">A register is an adminiatrative record containing information for the
            purpose of transmission or archiving. There are registers for taxation, (names
               [<ref type="internal" target="7767.xml">7767</ref> 254-231 BCE Arsinoite nome]; land
               [<ref type="internal" target="10193.xml">10193</ref> 117-138 CE, Theadelphia]), for
               [<term xml:lang="lat">epikrisis</term>], or registers recording day-to-day
            transactions [<ref target="https://papyri.info/ddbdp/sb;16;12676" type="external">27192</ref> II CE, Theadelphia]. A list often forms the central element of a
            register [<ref type="internal" target="8826.xml">8826</ref>; <ref type="internal" target="12627.xml">12627</ref>,
            both 182-187 CE, Arsinoite nome], and so the two are described together in this document.</p>
         <p xml:id="d25_p3">A distinction should be made between: <list type="ordered">
               <item>A register (one roll).</item>
               <item>A register made of re-used rolls pasted together, e.g. [<ref target="https://papyri.info/hgv/25668" type="external">25668</ref> 75-99 CE,
                  Oxyrhynchus (?) and <ref target="https://papyri.info/ddbdp/p.oxy;6;984" type="external">20395</ref> 91-92 CE, Lykopolis] pasted together and re-used on
                  the other side for a copy of Pindar’s <title rend="italic">Hymns</title> and
                     <title rend="italic">Paeans</title> [<ref target="https://papyri.info/dclp/62532" type="external">62532</ref> 100-150 CE,
                  Oxyrhynchus].</item>
               <item>A series of single declarations pasted together and making up a roll (<term xml:lang="grc">τόμος συγκολλήσιμος</term>) [<ref target="https://papyri.uni-koeln.de/stueck/DS_d1e43147/img/00310/orig/PK310ar_neu.jpg" type="external">12687</ref> and <ref target="https://papyri.uni-koeln.de/stueck/DS_d1e43147/img/00310/orig/PK310ar.jpg" type="external">8803</ref> pasted <ref target="https://papyri.uni-koeln.de/stueck/DS_d1e43147/img/00310/orig/PK310r.jpg" type="external">together</ref>, 184 CE, Arsinoite nome; <ref target="https://papyri.info/ddbdp/p.brux;1;1" type="external">16038</ref> - <ref type="external" target="https://papyri.info/ddbdp/p.brux;1;21">16057</ref> 174-207 CE, Prosopite nome].
                  From a typological point of view, a <term xml:lang="grc">τόμος
                     συγκολλήσιμος</term> does not correspond to a list or register. It is a
                  composite, and while it may display some features also found in long lists, such
                  as page numbering, each separate sheet – i.e. column – originally carried a
                  free-standing text.</item>
               <item>An abstract from a register, i.e. a single page copied from a roll that
               contained a register with a list [<ref type="external" target="https://papyri.info/ddbdp/chr.wilck;;146">9458</ref> 222 CE, Arsinoite nome] – see [<ref type="descr" target="#gt_decl_epikrisis">abstract<ptr target="#abstract"/>
                        </ref>].</item>
            </list>
            </p>
         <p xml:id="d25_p4">This overview will bear mainly on category 1, but the other three will
            also be addressed where appropriate.</p>
         <p xml:id="d25_p5">The structure, format, and layout of lists and registers do not show
            notable and consistent variations throughout the Ptolemaic and early Roman periods. On
            the contrary, consistency can be observed over a period of several centuries - compare
            the praescripts of [<ref target="https://papyri.info/ddbdp/p.tebt;1;88" type="external">3724</ref> 115 BCE, Kerkeosiris] and [<ref type="internal" target="44599.xml">44599</ref> 216/7 CE, Philadelphia].</p>
         <div xml:id="d25_div1_structure" type="section" n="1">
            <head xml:id="structure">Structure</head>
            <p xml:id="d25_p6">The structure may consist of an opening praescript, a transitional
               phrase, a list, and a closing section. Stand-alone lists usually display a looser
               structure than formal registers; they can simply comprise a bare list, without a
               praescript or closing section of any kind, e.g. [<ref type="internal" target="65756.xml">65756</ref> III BCE] a list of herbs, and [<ref type="internal" target="3117.xml">3117</ref> 139 BCE, Hemopolis Magna] a list of metal objects; or
               the praescript can be very short, e.g. [<ref type="internal" target="30209.xml">30209</ref> III CE] a list of food; <ref type="internal" target="28256.xml">28256</ref> II-III CE; <ref type="internal" target="25890.xml">25890</ref> 41-54
               CE, both Arsinoite nome].</p>
            <p xml:id="d25_p7">The praescript in more formal lists and registers may consist of some
               of the following: <list type="unordered">
                  <item>A <term xml:lang="grc">ὑπόμνημα</term> opening: <seg xml:id="d25_s1" type="syntax" corresp="../authority_lists.xml#introduction">[to name
                        &lt;dat.&gt;][from <term xml:lang="grc">παρά</term> + name
                        &lt;gen.&gt;]</seg> [<ref type="internal" target="9146.xml" corresp="#d25_s1">9146</ref> 208 CE, Soknopaiou Nesos]</item>
                  <item>A description of the contents, e.g. <seg xml:id="d25_s2" type="syntax" corresp="../authority_lists.xml#descr-contents">[<term xml:lang="grc">γραφὴ
                           ἱερῶν</term>]</seg>
                     <gloss corresp="#d25_s2">register of shrines</gloss> [<ref target="https://papyri.info/ddbdp/p.tebt;1;88" type="external" corresp="#d25_s2">3724</ref>] <seg xml:id="d25_s3" type="syntax" corresp="../authority_lists.xml#descr-contents">[<term xml:lang="grc">κατ’
                           ἄνδρα</term>]</seg>
                     <gloss corresp="#d25_s3">list of persons</gloss> [<ref type="internal" target="44599.xml" corresp="#d25_s3">44599</ref>, l.2].</item>
                  <item>A total of the items listed below [<ref target="https://papyri.info/ddbdp/p.tebt;1;66" type="external">3702</ref> 120
                     BCE, Kerkeosiris].</item>
                  <item>The date to which the accounts pertain, <term xml:lang="grc">τοῦ ἐνεστῶτος
                        ιϛ (ἔτους)</term> [<ref type="internal" target="9146.xml">9146</ref>], <term xml:lang="grc">τοῦ ἐνεστῶτος κϛ (ἔτους)</term> [<ref type="internal" target="12684.xml">12684</ref> 185-186 CE, Ptolemais Hormou].</item>
               </list>
                </p>
            <p xml:id="d25_p8">The transitional phrase is often <seg xml:id="d25_s4" type="syntax" corresp="../authority_lists.xml#trans-clause">[<term xml:lang="grc">ἔστι
                  δέ</term>]</seg>
               <gloss corresp="#d25_s4">those are (the listed items)</gloss> [<ref type="internal" target="12684.xml" corresp="#d25_s4">12684</ref> l.3, 185-186 CE; <ref type="internal" target="8760.xml" corresp="#d25_s4">8760</ref>, l.11, 185 CE, both
               Ptolemais Hormou] or <seg xml:id="d25_s5" type="syntax" corresp="../authority_lists.xml#trans-clause">[<term xml:lang="grc">εἶναι
                     δέ</term>]</seg> [<ref type="internal" target="21632.xml">21632</ref> l.5,
               94-95 CE, Oxyrhynchus], the infinitive implying an introductory verb such as <term xml:lang="grc">σημαίνει</term>
               <gloss>(the scribe) indicates</gloss>. The wording <seg xml:id="d25_s6" type="syntax" corresp="../authority_lists.xml#trans-clause">[<term xml:lang="grc">οὕτως
                     (ἔχει)</term>]</seg>
               <gloss corresp="#d25_s6">such (is the result)</gloss> is also attested [<ref target="https://papyri.info/ddbdp/p.prag;2;137" type="external" corresp="#d25_s6">12794</ref> l.12, 222 CE, Ptolemais Euergetis].</p>
            <p xml:id="d25_p9">This is followed by the list of items being recorded. An explicit
               introductory verb may appear, e.g. <term xml:lang="grc">σημαίνει</term> [<ref target="https://papyri.info/ddbdp/p.sijp;;30" type="external">110164</ref> II CE,
               Hermopolite nome]. The nature of listed items varies considerably:<list type="unordered">
                  <item>Names [<ref type="internal" target="9274.xml">9274</ref> c.200 CE, Soknopaiu
                     Nesos], gender [<ref target="https://papyri.info/ddbdp/p.count;;3" type="external">44106</ref> 229 BCE, Arsinoite nome], personal status [<ref target="https://papyri.info/ddbdp/p.tebt;4;1116" type="external">3787</ref>
                     B.2, l.30, 132-121 BCE, Kerkeosiris], professions [<ref target="https://papyri.info/ddbdp/p.count;;3" type="external">44106</ref>
                     col. ii, 230-229 BCE, Arsinoite nome], more specifically fishermen [<ref target="https://papyri.info/ddbdp/p.oxy;64;4440" type="external">26161</ref>
                     I CE, Oxyrhynchus], compulsory services (liturgies) [<ref target="8762.xml" type="internal">8762</ref> 185 CE, Arsinoite nome].</item>
                  <item>Amounts (to be paid out or to be received) [<ref target="https://papyri.info/ddbdp/p.sijp;;30" type="external">110164</ref>],
                     assets (<term xml:lang="grc">πόρος</term>) owned by candidates for liturgies
                        [<ref type="internal" target="8760.xml">8760</ref>], amounts recorded by a
                     pawnbroker [<ref target="https://papyri.info/ddbdp/sb;16;12421" type="external">26726</ref> II CE, Arsinoite nome].</item>
                  <item>Land surfaces, crops [<ref target="https://papyri.info/ddbdp/p.tebt;4;1128" type="external">3804</ref>, l.1-2, 115-114 BCE, Kerkeosiris; <ref type="internal" target="9470.xml">9470</ref> 172 CE, Theadelphia].</item>
                  <item>Shrines, priests, days of service [<ref target="https://papyri.info/ddbdp/p.tebt;1;88" type="external">3724</ref> 115
                     BCE, Kerkeosiris].</item>
                  <item>Types of documents, summary of transactions, [<ref type="internal" target="11965.xml">11965</ref> 42 CE, Tebtunis].</item>
               </list>
                </p>
            <p xml:id="d25_p10">The lists may be ordered alphabetically [<ref target="https://papyri.info/ddbdp/bgu;9;1898" type="external">9469</ref> 172 CE,
               Theadelphia; <ref type="internal" target="44599.xml">44599</ref>; <ref target="https://papyri.info/ddbdp/bgu;9;1897" type="external">9467</ref> 166 CE,
               Theadelphia; <ref type="internal" target="8826.xml">8826</ref>]; in geographical
               order [<ref target="https://papyri.info/ddbdp/p.oxy;47;3362" type="external">29101</ref> II CE, Oxyrhynchus (nomes)]; or chronologically [<ref type="internal" target="12275.xml">12275</ref> 51 CE, Philadelphia, (by year)], [<ref type="internal" target="12301.xml">12301</ref> I CE, Arsinoite nome, (by month)]
               and [<ref type="internal" target="9523.xml">9523</ref> (by day)].</p>
            <p xml:id="d25_p11">A list can display single or multiple levels of classification, e.g.
               name, then month and amount paid [<ref type="internal" target="9521.xml">9521</ref>
               140 CE, Philadelphia]. A tax register from Philadelphia displays two orders of
               classification: it is first divided into three sections (corresponding to Alexandrian
               magistrates, local magistrates, and villagers) and within each of these sections the
               information is presented in alphabetical order.</p>
            <p xml:id="d25_p12">In addition to the grand total, which might appear in the
               praescript, intermediate sub-totals also appear [<ref type="internal" target="11662.xml">11662</ref> 114 CE, Arsinoite nome].</p>
            <p xml:id="d25_p13">The postscript may contain:<list type="unordered">
                  <item>
                            <seg xml:id="d25_s7" type="syntax" corresp="../authority_lists.xml#date">[date@end]</seg> [<ref type="internal" target="8760.xml" corresp="#d25_s7">8760</ref>; <ref type="internal" target="44599.xml" corresp="#d25_s7">44599</ref>].</item>
                  <item>Transmission note <seg xml:id="d25_s8" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἐπιδέδωκα</term>]</seg>
                     <gloss corresp="#d25_s8">I have submitted</gloss> [<ref type="internal" target="44599.xml" corresp="#d25_s8">44599</ref>].</item>
               </list>
            </p>
         </div>
         <div xml:id="d25_div2_format" type="section" n="2">
            <head xml:id="format">Format</head>
            <p xml:id="d25_p14">Many simple lists are written on a single sheet of papyrus [<ref type="external" target="https://papyri.info/ddbdp/p.cair.zen;4;59680">1308</ref> 258-256 BCE, Alexandria(?)], while
               many registers are written on wide [<ref type="intro" target="concepts.xml">rolls<ptr target="#roll"/>
                    </ref>], i.e. a papyrus that displays several columns of text
               and is long enough to be rolled up [<ref type="internal" target="8747.xml">8747</ref>
               42 CE, Arsinoite nome]. Single sheets can be in <term xml:lang="lat">pagina</term>
               format with horizontal [<ref type="internal" target="28256.xml">28256</ref> II-III
               CE, Arsinoite nome] or vertical fibres [<ref type="internal" target="28175.xml">28175</ref> II CE, Arsinoite nome]; or on a horizontally oriented sheet with
               horizontal [<ref type="internal" target="28255.xml">28255</ref> II-III CE, Arsinoite
               nome], or vertical fibres [<ref type="internal" target="2272.xml">2272</ref> III BCE,
               Philadelphia]; or squarish [<ref type="internal" target="10600.xml">10600</ref> 56
               CE, Philadelphia (horizontal fibres); <ref type="internal" target="30390.xml">30390</ref> III CE, Oxyrhynchus (vertical fibres)]. </p>
            <p xml:id="d25_p15">Registers are often very long documents and constitute some of the
               widest rolls found in documentary papyri, e.g. a tax register [<ref type="internal" target="20001.xml">20001</ref> 143 CE, Thebes (H. 35.5 x W. 320cm)], with
               horizontal fibres; or [<ref type="internal" target="9470.xml">9470</ref> (H. 21 x W.
               207.5cm)] a register of vineland written against the fibres. Some are also found in
                  <term xml:lang="lat">pagina</term> [<ref type="internal" target="9146.xml">9146</ref>] or squarish [<ref type="internal" target="8762.xml">8762</ref>]
               formats.</p>
            <p xml:id="d25_p16">The ink is usually black, but red ink occurs when there is a copy of
               a list [<ref type="internal" target="8760.xml">8760</ref>], or an [<ref type="descr" target="#gt_decl_epikrisis">abstract<ptr target="#abstract"/>
                    </ref>] from a
               register [<ref type="internal" target="20195.xml">20195</ref> (epikrisis)]. Red ink
               may also be used for corrections or additions, e.g. [<ref target="15020.xml" type="internal">15020</ref>
               (minutes of court proceedings); <ref type="internal" target="14014.xml">14014</ref>
               (abstract from register of eprkrisis)]</p>
         </div>
         <div xml:id="d25_div3_layout" n="3" type="section">
            <head xml:id="layout">Layout</head>
            <p xml:id="d25_p17">A simple list may be written as a single block of text, without
               filling the whole sheet of papyrus [<ref type="internal" target="2007.xml">2007</ref>; <ref type="internal" target="12684.xml">12684</ref>], but longer lists
               and registers tend to stretch to many columns of text [<ref type="internal" target="44599.xml">44599</ref>]. Columns are generally evenly spaced [<ref type="internal" target="10216.xml">10216</ref> 162 CE, Theadelphia], but
               occasionally a column may snake around the vertical outline of the preceding one,
               e.g. [<ref type="internal" target="12627.xml">12627</ref> 182-187, Arsinoite nome], a
               document produced by a scribe who had limited abilities (<term xml:lang="grc">βραδὺς
                  γράφων</term>). A land register from the Kynopolite nome is particularly uniform
               and elegant and clearly produced in bookform by a scribe used to copying literary
               texts; the bottom margin here is also conspicuously generous [<ref type="internal" target="22479.xml">22479</ref> 196-197 CE].</p>
            <p xml:id="d25_p18">Multiple columns are sometimes numbered [<ref target="https://papyri.info/ddbdp/p.prag;2;137" type="external">12794</ref> 221
               CE, Ptolemais Euergetis], by page (<term xml:lang="grc">σελίς</term>) or by sheet
                  (<term xml:lang="grc">κόλλημα</term>), as evidenced in abstracts, e.g. [<ref type="internal" target="14014.xml">14014</ref> l. 6, 188 CE, Karanis; <ref target="https://papyri.info/ddbdp/bgu;1;274" type="external">28248</ref> II-III
               CE, Arsinoite nome]. </p>
            <p xml:id="d25_p19">The scribe often makes use of <term xml:lang="lat">ekthesis</term>
               and <term xml:lang="lat">eisthesis</term> to highlight sections of information, e.g.
                  [<ref type="internal" target="12690.xml">12690</ref> 182-187 CE, Arsinoite nome;
                  <ref type="internal" target="9470.xml">9470</ref>; <ref type="internal" target="8857.xml">8857</ref>]. He may also separate sections by adding a
               horizontal line [<ref type="internal" target="9131.xml">9131</ref> 276 CE, Arsinoite
               nome], or divide them with an empty space [<ref type="internal" target="10193.xml">10193</ref>; <ref type="internal" target="41441.xml">41441</ref> 235 CE,
               Arsinoite nome]. There may also be control marks in the margins [<ref type="internal" target="4021.xml">4021</ref> I BCE, Herakleopolite nome].</p>
         </div>
      </body>
   </text>
</TEI>