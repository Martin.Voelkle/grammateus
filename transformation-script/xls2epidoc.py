#!/usr/bin/env python
# coding: utf-8

# ## Import

# import modules
from openpyxl import load_workbook # work with excel
from lxml import etree # work with XML structure
from copy import deepcopy # create copy of XML template
import re # regular expressions
from datetime import date
import sys # get arguments
import os # manipulate file paths

# take as argument a path to a folder with matrices
folder_path = os.path.normpath(sys.argv[1])

# get list of files in the folder as absolute paths
files_path = [os.path.join(folder_path, x) for x in os.listdir(folder_path)]


# ## Functions

def strip_ns_prefix(tree):
    #iterate through only element nodes (skip comment node, text node, etc) :
    for element in tree.xpath('descendant-or-self::*'):
        #if element has prefix...
        if element.prefix:
            #replace element name with its local name
            element.tag = etree.QName(element).localname
    return tree


# ## Variables

nb_record_created = 0

# TEI namespace
ns = {'tei': 'http://www.tei-c.org/ns/1.0'}

# etree parser
parser = etree.XMLParser(remove_blank_text=True)

# lists of links between epidoc files and external links files
#links_history = {}
#links_texts = {}
links_images = {}



# ## Create Epidoc Files

# for each matrix file, create an epidoc xml from template
for file in files_path:
    # load matrix into workbook (wb)
    wb = load_workbook(file)

    # load first sheet in wb
    ws = wb.worksheets[0]

    # create header from first row
    for h in ws.iter_rows(max_row=1):
        header = [cell.value for cell in h]

    # for each row of the matrix, except header row 1     
    for row in ws.iter_rows(min_row=2):
        
        # get cells as a dictionary - header:cell-value as strings
        cells = [str(cell.value) for cell in row]
        rowdata = dict(zip(header, cells))
        
        # create new xml tree from template
        tree = etree.parse('grammateus-template.xml', parser)
        root = tree.getroot()

        # ----- title and IDs -----
        # create title for display on webpages - from series-[volume]-number
        t = rowdata['series']
        if rowdata['volume'] != '':
            t = t+' '+rowdata['volume']
        t = t+' '+rowdata['number']
        
		# title 
        title = root.find('.//tei:titleStmt/tei:title', ns)
        title.text = t
        
        # TM number
        tm = root.find('.//tei:idno[@type="TM"]', ns)
        tm.text = rowdata['TM']
        
        # HGV number(s)
        hgv = root.find('.//tei:idno[@type="hgv"]', ns)
        hgv.text = rowdata['HGV']
		# save as a list
        hgv_id = rowdata['HGV'].split(' ')
        
        # ddb-hybrid
        ddbhybrid = root.find('.//tei:idno[@type="ddb-hybrid"]', ns)
        ddbhybrid.text = rowdata['DDB']
        
        # ddb-filename
        ddb = rowdata['DDB'].replace(';', '.')
        ddb = ddb.replace('..', '.')
        ddbfilename = root.find('.//tei:idno[@type="ddb-filename"]', ns)
        ddbfilename.text = ddb

        
        # ----- support -----
        # fibres
        support = root.find('.//tei:support', ns)
        support.set('ana', '../authority-lists.xml#'+rowdata['fibres'])
        
        # dimensions
        height = support.find('.//tei:height', ns)
        height.text = rowdata['height']
        width = support.find('.//tei:width', ns)
        width.text = rowdata['width']
        
        # ----- layout -----
        # columns
        layout = root.find('.//tei:layout', ns)
        layout.set('columns', rowdata['columns'])
        
        # margins
        top = layout.find('.//tei:dim[@n="top"]', ns)
        top.text = rowdata['margin-top']
        bottom = layout.find('.//tei:dim[@n="bottom"]', ns)
        bottom.text = rowdata['margin-bottom']
        right = layout.find('.//tei:dim[@n="right"]', ns)
        right.text = rowdata['margin-right']
        left = layout.find('.//tei:dim[@n="left"]', ns)
        left.text = rowdata['margin-left']
        
        # blank spaces
        if rowdata['blank-after'] != 'None':
            lines = rowdata['blank-after'].split(', ')
            #blank_height = rowdata['blank hgt.'].split(', ')
            blank = layout.find('.//tei:ab[@type="blank"]', ns)
            for l in lines:
                locus = etree.SubElement(blank, 'locus')
                locus.set('from', l)
                locus.set('to', str(int(l)+1))
        
        # text sections
        sections = root.find('.//tei:ab[@type="section"]', ns)
        # counter for each section according to section-nb
        s = 1
        #for s in range(1, int(rowdata['section-nb'])+1):
        while (rowdata['section'+str(h)] and rowdata['section'+str(h)] != ''):
            # create element with xml ID
            sec = etree.SubElement(sections, 'locus')
            sec.set("{http://www.w3.org/XML/1998/namespace}id", 'section'+str(s))
            if rowdata['section'+str(s)] == 'missing':
                # if missing section
                sec.text = 'missing'
            else:
                # else add attributes
                lines, hand, sectionName = rowdata['section'+str(s)].split('/')
                sec.set('from', lines.split('-')[0])
                sec.set('to', lines.split('-')[1])
                sec.set('corresp', hand)
                sec.set('ana', '../authority-lists.xml#'+sectionName)
            # increase counter
            s = s+1
        
        # ----- handDesc -----
        handDesc = root.find('.//tei:handDesc', ns)
        # counter for each hand
        h = 1
        #for h in range(1, int(rowdata['hand-nb'])+1):
        # while there is a row m1, m2, etc that is not empty
        while (rowdata['m'+str(h)] and rowdata['m'+str(h)] != ''):
            # create element with xml ID
            handNote = etree.SubElement(handDesc, 'handNote')
            handNote.set("{http://www.w3.org/XML/1998/namespace}id", 'm'+str(h))
            # add attributes
            section, scribe = rowdata['m'+str(h)].split('/')
            handNote.set('corresp', section)
            if scribe:
                handNote.set('scribeRef', '../authority-lists.xml#'+scribe)
            # increase counter
            h = h+1
        
        # ----- sealDesc -----
        # TODO: use papyri.info encoding?
        physDesc = root.find('.//tei:physDesc', ns)
        sealDesc = etree.SubElement(physDesc, 'sealDesc')
        seal = etree.SubElement(sealDesc, 'seal')
        p = etree.SubElement(seal, 'p')
        p.text = rowdata['seal']
        
        # ----- profileDesc -----
        # text type/subtype
        # this is assuming only 1 type and 1 subtype
        t_type = root.find('.//tei:keywords[@n="text-type"]/tei:term', ns)
        t_type.text = rowdata['text-type']
        t_subtype = root.find('.//tei:keywords[@n="text-subtype"]/tei:term', ns)
        t_subtype.text = rowdata['text-subtype']
        
        # ----- facsimile -----
        # image
        image = rowdata['image'].split(' and ') # in rare cases there are more than one link
        facs = root.find('.//tei:facsimile/tei:graphic', ns)
        facs_corresp = '#link_facs_'+rowdata['TM']
        facs.set('corresp', facs_corresp)
        
        # ----- text -----
        #textdiv = root.find('.//tei:body/tei:div', ns)
        #text_corresp = '#link_text_'+rowdata['TM']
        #textdiv.set('corresp', text_corresp)
        
        # ----- history -----
        #history = root.find('.//tei:history', ns)
        #history_corresp = '#link_history_'+rowdata['TM']
        #history.set('corresp', history_corresp)

        # ----- lists for external files -----
        # get link to history
        #links_history[history_corresp] = ['https://papyri.info/hgv/'+n+'/source' for n in hgv_id]
        # get link to images
        #links_images[facs_corresp] = image
        # get link to papyri.info
        #links_texts[text_corresp] = 'https://papyri.info/ddbdp/'+rowdata['DDB']+'/source'
        
        # ----- cleaning and saving -----
        # date of record created
        recordcreated = root.find('.//tei:revisionDesc/tei:change', ns)
        recordcreated.set('when', date.today().isoformat())
        
        # strip namespace prefixes from results
        strip_ns_prefix(tree)
       
        # save
        outpath = '../data/papyri/'+rowdata['TM']+'.xml'
        tree.write(outpath, pretty_print=True)
        
    # print number of records created
    x = sum(1 for row in ws.iter_rows(min_row=2))
    print(str(x)+' records created.')
    
    # add them to the total
    nb_record_created = nb_record_created + x
	
# print total nb of records created
print(nb_record_created)

# ## Update Links Files 
# removed the links to history/texts
# replaced with url variable + HGV/DDB identifiers

""" # ----- history -----
# load empty file for history links
tree = etree.parse('empty_external_links_history.xml', parser)
root = tree.getroot()

for key, value in links_history.items():
    # create item element
    item = etree.SubElement(root, 'item')
    # @xml:id as corresp link without the '#'
    item.set('{http://www.w3.org/XML/1998/namespace}id', key[1:])
    # create ref element(s)
    for v in value:
        ref = etree.SubElement(item, 'ref')
        # @target attribute is actual link to HGV file
        ref.set('target', v)           
# strip namespace prefixes from results
strip_ns_prefix(tree)
# save
tree.write('../data/external_links_history.xml', pretty_print=True)

# ----- texts -----
# load empty file for texts links
tree = etree.parse('empty_external_links_texts.xml', parser)
root = tree.getroot()

for key, value in links_texts.items():
    # create item element
    item = etree.SubElement(root, 'item')
    # @xml:id as corresp link without the '#'
    item.set('{http://www.w3.org/XML/1998/namespace}id', key[1:])
    # create unique ref element
    ref = etree.SubElement(item, 'ref')
    # @target attribute is link to papyri.info
    ref.set('target', value)
# strip namespace prefixes from results
strip_ns_prefix(tree)
# save
tree.write('../data/external_links_texts.xml', pretty_print=True) """

# ----- images -----
# TODO: deal with 'None' value (when no image available)
# load empty file for images links
tree = etree.parse('empty_external_links_images.xml', parser)
root = tree.getroot()

for key, value in links_images.items():
    # create item element
    item = etree.SubElement(root, 'item')
    # @xml:id as corresp link without the '#'
    item.set('{http://www.w3.org/XML/1998/namespace}id', key[1:])
    # create ref element(s)
    for v in value:
        ref = etree.SubElement(item, 'ref')
        # @target attribute is actual link to image
        ref.set('target', v)
# strip namespace prefixes from results
strip_ns_prefix(tree)
# save
tree.write('../data/external_links_images.xml', pretty_print=True)
