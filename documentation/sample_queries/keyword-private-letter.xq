xquery version "3.1";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace functx = "http://www.functx.com";

declare option exist:serialize "method=html media-type=text/html";

declare function local:isDate($origDate) {
    let $when :=
        if (matches($origDate/@when/string(), "\d{4}-02-29")) then xs:date($origDate/@when/replace(string(), '29', '28')) (: special case for illegal 29 feb. :)
        else if (matches($origDate/@when/string(), "\d{4}-\d{2}-\d{2}")) then xs:date($origDate/@when/string())
        else if (matches($origDate/@when/string(), "\d{4}-\d{2}")) then xs:date(concat($origDate/@when/string(), "-01"))
        else if ($origDate/@when) then xs:date(concat($origDate/@when/string(), "-12-31"))
        else xs:date("2020-01-01") (: will not count as valid date :)
    
    let $notAfter :=
        if (matches($origDate/@notAfter/string(), "\d{4}-02-29")) then xs:date($origDate/@notAfter/replace(string(), '29', '28')) (: special case for illegal 29 feb. :)
        else if (matches($origDate/@notAfter/string(), "\d{4}-\d{2}-\d{2}")) then xs:date($origDate/@notAfter/string())
        else if (matches($origDate/@notAfter/string(), "\d{4}-\d{2}")) then xs:date(concat($origDate/@notAfter/string(), "-01"))
        else if ($origDate/@notAfter) then xs:date(concat($origDate/@notAfter/string(), "-12-31"))
        else xs:date("2020-01-01") (: will not count as valid date :)
        
    let $notBefore :=
        if (matches($origDate/@notBefore/string(), "\d{4}-02-29")) then xs:date($origDate/@notBefore/replace(string(), '29', '28')) (: special case for illegal 29 feb. :)
        else if (matches($origDate/@notBefore/string(), "\d{4}-\d{2}-\d{2}")) then xs:date($origDate/@notBefore/string())
        else if (matches($origDate/@notBefore/string(), "\d{4}-\d{2}")) then xs:date(concat($origDate/@notBefore/string(), "-01"))
        else if ($origDate/@notBefore) then xs:date(concat($origDate/@notBefore/string(), "-12-31"))
        else xs:date("2020-01-01") (: will not count as valid date :)
        
    let $isDate :=
        if (xs:date("0301-01-01") <= $when and $when < xs:date("0701-01-01")) then "true"
        else if (not($origDate/@notAfter) and xs:date("0301-01-01") <= $notBefore+xs:yearMonthDuration("P50Y") and $notBefore <= xs:date("0701-01-01")) then "true"
        else if (not($origDate/@notBefore) and xs:date("0301-01-01") <= $notAfter and $notAfter - xs:yearMonthDuration("P50Y")<= xs:date("0701-01-01")) then "true"
        else if ($origDate/@notAfter and $origDate/@notBefore and xs:date("0301-01-01") <= $notAfter and $notBefore <= xs:date("0701-01-01")) then "true"
        else "false"
        
    return $isDate
};

(: HGV collection :)
let $collection := collection("/db/apps/idp-data-hgv")

(:  Private letters :)

let $result :=
for $doc in $collection
(: keywords and title :)
where (($doc//tei:term[matches(text(), 'brief', 'i')] and $doc//tei:term[matches(text(), 'privat', 'i')])
or ($doc//tei:title[matches(text(), 'brief', 'i')] and $doc//tei:title[matches(text(), 'privat', 'i')]))
(: date :)
and local:isDate($doc//tei:origDate[1]) eq "true"

 (: identifiers, HGV info :)
let $tm := $doc//tei:idno[@type eq 'TM']
let $hgv := $doc//tei:idno[@type eq 'filename']
let $date-high := if ($doc//tei:origDate[@notBefore]) then $doc//tei:origDate/@notBefore/string() else $doc//tei:origDate
let $date-low := if ($doc//tei:origDate[@notAfter]) then $doc//tei:origDate/@notAfter/string() else $doc//tei:origDate
let $ddb := $doc//tei:idno[@type eq 'ddb-hybrid']
let $papyri-info := "https://papyri.info/ddbdp/"||$ddb
let $hgv-title := $doc//tei:titleStmt/tei:title/string()
let $hgv-provenance := $doc//tei:origPlace
let $hgv-subjects := string-join($doc//tei:term, ',')
let $image := string-join($doc//tei:graphic/@url/string(), ' and ')
let $printed-image := $doc//tei:bibl[@type eq 'illustrations']

(: dimensions from apis catalogue :)
let $apis-dimension :=
if (matches($ddb/string(), '[a-z0-9\.;]') and not(matches($ddb/string(), '[A-Z\s\(\)]')) and doc-available($papyri-info||'/source') and not(doc($papyri-info||'/source')//tei:ref[@type eq 'reprint-in'])) then
let $htmlpage := unparsed-text($papyri-info)
let $apisID := substring-after($htmlpage, '<meta property="dc:relation" content="http://papyri.info/apis/') => substring-before('/source')
let $apis-doc := doc('https://papyri.info/apis/'||$apisID||'/source')
return $apis-doc//tei:support/string()
else ()
            
(: title info :)
let $bibl := $doc//tei:bibl[matches(@subtype, 'principal', 'i')]
let $series := $bibl//tei:title[@level eq 's' and @type eq 'abbreviated']/string()
let $volume := $bibl//tei:biblScope[@type eq 'volume']/string()
let $fascicle := $bibl//tei:biblScope[@type eq 'fascicle']/string()
let $numbers := $bibl//tei:biblScope[@type eq 'numbers']/string()
let $side := $bibl//tei:biblScope[@type eq 'side']/string()
let $generic := $bibl//tei:biblScope[@type eq 'generic']/string()
let $page := $bibl//tei:biblScope[@type eq 'pages']/string()
let $number := $bibl//tei:biblScope[@type eq 'number']/string()
            
            
(: typology :)
let $format-type := 'gt_ee'
let $format-subtype := 'gt_letterPrivate'
let $format-variation := ()

    
order by xs:integer($doc//tei:idno[@type eq "TM"]/text()) ascending

return
    <doc>
        <TM>{$tm}</TM>
        <HGV>{$hgv}</HGV>
        <date-high>{$date-high}</date-high>
        <date-low>{$date-low}</date-low>
        <hgv-provenance>{$hgv-provenance}</hgv-provenance>
        <DDB>{$ddb}</DDB>
        <papyri-info>{$papyri-info}</papyri-info>
        <series>{$series}</series>
        <volume>{$volume}</volume>
        <fascicle>{$fascicle}</fascicle>
        <numbers>{$numbers}</numbers>
        <side>{$side}</side>
        <generic>{$generic}</generic>
        <page>{$page}</page>
        <number>{$number}</number>
        <hgv-title>{$hgv-title}</hgv-title>
        <format-type>{$format-type}</format-type>
        <format-subtype>{$format-subtype}</format-subtype>
        <format-variation>{$format-variation}</format-variation>
        <hgv-subjects>{$hgv-subjects}</hgv-subjects>
        <hgv-en></hgv-en>
        <iframe></iframe>
        <IIIF></IIIF>
        <canvasID></canvasID>
        <image>{$image}</image>
        <printed-image>{$printed-image}</printed-image>
        <image-rights></image-rights>
        <fibres></fibres>
        <height>{$apis-dimension}</height>
        <width>{$apis-dimension}</width>
        <shape></shape>
        <columns></columns>
        <seal></seal>
        <hand-nb></hand-nb>
        <section-nb></section-nb>
        <m1></m1>
        <m2></m2>
        <m3></m3>
        <m4></m4>
        <m5></m5>
        <m6></m6>
        <m7></m7>
        <m8></m8>
        <m9></m9>
        <m10></m10>
        <m11></m11>
        <section1></section1>
        <section2></section2>
        <section3></section3>
        <section4></section4>
        <section5></section5>
        <section6></section6>
        <section7></section7>
        <section8></section8>
        <section9></section9>
        <section10></section10>
        <section11></section11>
        <section12></section12>
        <section13></section13>
        <section14></section14>
        <section15></section15>
        <section16></section16>
        <section17></section17>
        <section18></section18>
        <section19></section19>
        <section20></section20>
    </doc>

(: filter results for language greek, and for no gap in transcription
 : <gap reason="lost" extent="unknown" unit="line"/>
 : 
 : Possible to also eliminate results without an image
 : where $transcription//tei:div[@xml:lang eq 'grc'] and not($transcription//tei:gap[@reason eq 'lost'][@extent eq 'unknown'])
 : and not(functx:has-empty-content($pap/image) and functx:has-empty-content($pap/printed-image))
 : 
 : if not used, let $filter_result := $result
 :  :)

let $filter_result := $result



return

    <html>
        <p>Total papyri: {count(collection("/db/apps/papyri-hgv-apis/HGV/"))}</p>
        <p>Results: {count($filter_result)}</p>
    <table>
        <tr>
            {for $elem in $filter_result[1]/*
                return
                    <th>{$elem/name()}</th>
            }
        </tr>
        {for $r in $filter_result
            return
                <tr>
                    {for $info in $r/*
                        return
                            <td>{$info}</td>
                    }
                </tr>
        }
    </table>
    </html>

