xquery version "3.1";

import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace hgv = "http://grammateus.ch/hgv";

declare function hgv:update() {
    for $doc in collection($g:papyri)//tei:TEI

    let $log := console:log($doc//tei:idno[@type eq "hgv"])

    let $hgv := gramm:doc-expand-history($doc)

    let $history := element { node-name($hgv)}
                        {attribute {'corresp'} {$doc//tei:history/@corresp/string()},
                        $hgv/@*,
                        $hgv/node() }

    return update replace $doc//tei:history with $history
};

hgv:update()
