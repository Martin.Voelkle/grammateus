xquery version "3.1";

import module namespace array="http://www.w3.org/2005/xpath-functions/array";
import module namespace zt="http://grammateus.ch/zotero" at "modules/zotero.xql";


declare option exist:serialize "method=html media-type=text/html";

(: get top-level items in zotero (bibliographic references) :)
let $keys := zt:request($zt:items || "/top?format=keys")

(: for each bibliographic reference :)
for $key in tokenize($keys[2], '\n')

return
    
    (: check that the key is not an empty string :)
    if ($key !='') 
    (:get the JSON and process it to create a TEI file :)
    then (zt:process-record(zt:item($key), $key), $key)
    else()
    
