xquery version "3.1";

module namespace g="http://grammateus.ch/global";

(: external sites :)
declare variable $g:papinfo := 'https://papyri.info/';
declare variable $g:papinfoddb := 'https://papyri.info/ddbdp/';
declare variable $g:papinfohgv := 'https://papyri.info/hgv/';
declare variable $g:hgv := 'https://aquila.zaw.uni-heidelberg.de/hgv/';
declare variable $g:tm := 'https://www.trismegistos.org/text/';
declare variable $g:fns := 'https://p3.snf.ch/project-182205';
declare variable $g:license := 'https://creativecommons.org/licenses/by-nc/4.0/';

(:: APP ::)
(: eXist app root - links to resources (css, js, img,..) :)
declare variable $g:root := "/exist/apps/grammateus/";
(:declare variable $g:root := "https://grammateus.unige.ch/";:)

(: resources :)
declare variable $g:css := $g:root || 'resources/css/';
declare variable $g:js := $g:root || 'resources/js/';
declare variable $g:img := $g:root || 'resources/img/';
declare variable $g:fonts := $g:root || 'resources/fonts/';
declare variable $g:mirador := $g:root || 'resources/mirador/';

(:: DATABASE ::)
(: eXist database root - access to files in modules :)
declare variable $g:db := "/db/apps/grammateus/";

(: data :)
declare variable $g:data := $g:db || 'data/';
declare variable $g:papyri := concat($g:data,'papyri/');
declare variable $g:phase1 := concat($g:data,'papyri/phase1/');
declare variable $g:byzantine := concat($g:data,'papyri/byzantine/');
declare variable $g:descr := concat($g:data,'descriptions/');
declare variable $g:class := concat($g:data,'classification/');
declare variable $g:bibl := concat($g:data,'bibl/');
declare variable $g:intro := concat($g:data,'introduction/');
declare variable $g:editorial := concat($g:data,'editorial/');
declare variable $g:team := concat($g:editorial,'team.html');
declare variable $g:cite := concat($g:editorial,'howto-cite.html');
declare variable $g:rights := concat($g:editorial,'rights.html');
declare variable $g:tuto := concat($g:editorial,'tutorials.html');

(: grammateus classification of documentary papyri :)
declare variable $g:taxonomy := concat($g:db, 'data/grammateus_taxonomy.xml');

(: authority lists :)
declare variable $g:authority := concat($g:db,'data/authority_lists.xml');

(: external links :)
declare variable $g:history := $g:data || 'external_links_history.xml';
declare variable $g:images := $g:data || 'external_links_images.xml';
declare variable $g:texts := $g:data || 'external_links_texts.xml';

(: xslt :)
declare variable $g:xslt := $g:db || 'resources/xslt/';
declare variable $g:epidoc := $g:db || 'resources/xslt/epidoc/';
declare variable $g:typologyToHTML := $g:xslt || "typologyToHTML.xslt";
declare variable $g:typologyToTOC := $g:xslt || "typologyToTOC.xslt";
declare variable $g:taxonomyToList := $g:xslt || "taxonomyToList.xslt";
declare variable $g:sidebar := $g:xslt || "sidebar.xslt";
declare variable $g:seg := $g:xslt || "seg.xslt";
declare variable $g:ref := $g:xslt || "ref.xslt";

