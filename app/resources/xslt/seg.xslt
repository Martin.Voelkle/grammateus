<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exist="http://exist-db.org/xquery/kwic" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0">
    
    <xsl:strip-space elements="tei:bibl tei:listBibl"/>


    
    <!-- ********** -->
    <!-- Templates  -->
    <!-- ********** -->
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    
    <xsl:template match="tei:hi[@rend = 'bold']">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    
    <xsl:template match="tei:q[not(parent::tei:q)]">

        <xsl:apply-templates/>

    </xsl:template>
    
    <xsl:template match="tei:q[parent::tei:q]">
        <xsl:text>‘</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>’</xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:term[@xml:lang = 'grc']">
        <span class="grc">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:term[@xml:lang = 'lat']">
        <span class="lat" style="font-style:italic;">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    
</xsl:stylesheet>