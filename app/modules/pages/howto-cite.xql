xquery version "3.1";

module namespace howtocite="http://grammateus.ch/pages/howtocite";

import module namespace g="http://grammateus.ch/global" at "../global.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function howtocite:page() {
    doc($g:editorial||"howto-cite.html")//body/div
};