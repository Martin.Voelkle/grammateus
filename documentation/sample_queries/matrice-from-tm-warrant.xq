xquery version "3.1";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare option exist:serialize "method=html media-type=text/html";

(: get matrix from TM numbers - warrants :)

let $hgv_list := "22179
38314
38524
316224"

return 
    <html>
        <body>
            <table>
                <tr>
                    <th>TM</th>
                    <th>HGV</th>
                    <th>date-high</th>
                    <th>date-low</th>
                    <th>DDB</th>
                    <th>papyri-info</th>
                    <th>series</th>
                    <th>volume</th>
                    <th>fascicle</th>
                    <th>numbers</th>
                    <th>side</th>
                    <th>generic</th>
                    <th>page</th>
                    <th>number</th>
                    <th>hgv-title</th>
                    <th>hgv-provenance</th>
                    <th>format-type</th>
                    <th>format-subtype</th>
                    <th>format-variation</th>
                    <th>hgv-subjects</th>
                    <th>hgv-en</th>
                    <th>iframe</th>
                    <th>canvasID</th>
                    <th>image</th>
                    <th>printed-image</th>
                    <th>image-rights</th>
                    <th>fibres</th>
                    <th>height</th>
                    <th>width</th>
                    <th>shape</th>
                    <th>columns</th>
                    <th>seal</th>
                    <th>hand-nb</th>
                    <th>section-nb</th>
                    <th>m1</th>
                    <th>m2</th>
                    <th>m3</th>
                    <th>m4</th>
                    <th>m5</th>
                    <th>m6</th>
                    <th>m7</th>
                    <th>m8</th>
                    <th>m9</th>
                    <th>m10</th>
                    <th>m11</th>
                    <th>section1</th>
                    <th>section2</th>
                    <th>section3</th>
                    <th>section4</th>
                    <th>section5</th>
                    <th>section6</th>
                    <th>section7</th>
                    <th>section8</th>
                    <th>section9</th>
                    <th>section10</th>
                    <th>section11</th>
                    <th>section12</th>
                    <th>section13</th>
                    <th>section14</th>
                    <th>section15</th>
                    <th>section16</th>
                    <th>section17</th>
                    <th>section18</th>
                    <th>section19</th>
                    <th>section20</th>
                </tr>
        {(:  ROWS :)
            for $hgv in tokenize($hgv_list, '\s+?')
            let $doc := doc("https://papyri.info/hgv/"||$hgv||"/source")
            
            (: identifiers, HGV info :)
            let $tm := $doc//tei:idno[@type eq 'TM']
            let $date-high := if ($doc//tei:origDate[@notBefore]) then $doc//tei:origDate/@notBefore/string() else $doc//tei:origDate
            let $date-low := if ($doc//tei:origDate[@notAfter]) then $doc//tei:origDate/@notAfter/string() else $doc//tei:origDate
            let $ddb := $doc//tei:idno[@type eq 'ddb-hybrid']
            let $papyri-info := "https://papyri.info/ddbdp/"||$ddb
            let $hgv-title := $doc//tei:titleStmt/tei:title
            let $hgv-provenance := $doc//tei:origPlace
            let $hgv-subjects := string-join($doc//tei:term, ',')
            let $image := string-join($doc//tei:graphic/@url/string(), ' and ')
            let $printed-image := $doc//tei:bibl[@type eq 'illustrations']
            
            (: title info :)
            let $bibl := $doc//tei:bibl[@subtype eq 'principal']
            let $series := $bibl//tei:title[@level eq 's' and @type eq 'abbreviated']
            let $volume := $bibl//tei:biblScope[@type eq 'volume']
            let $fascicle := $bibl//tei:biblScope[@type eq 'fascicle']
            let $numbers := $bibl//tei:biblScope[@type eq 'numbers']
            let $side := $bibl//tei:biblScope[@type eq 'side']
            let $generic := $bibl//tei:biblScope[@type eq 'generic']
            let $page := $bibl//tei:biblScope[@type eq 'pages']
            let $number := $bibl//tei:biblScope[@type eq 'number']
            
            (: typology :)
            let $format-type := ()
            let $format-subtype := ()
            let $format-variation := ()
            
            return
                <tr>
                    <td>{$tm}</td>
                    <td>{$hgv}</td>
                    <td>{$date-high}</td>
                    <td>{$date-low}</td>
                    <td>{$ddb}</td>
                    <td>{$papyri-info}</td>
                    <td>{$series}</td>
                    <td>{$volume}</td>
                    <td>{$fascicle}</td>
                    <td>{$numbers}</td>
                    <td>{$side}</td>
                    <td>{$generic}</td>
                    <td>{$page}</td>
                    <td>{$number}</td>
                    <td>{$hgv-title}</td>
                    <td>{$hgv-provenance}</td>
                    <td>{$format-type}</td>
                    <td>{$format-subtype}</td>
                    <td>{$format-variation}</td>
                    <td>{$hgv-subjects}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{$image}</td>
                    <td>{$printed-image}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>    
                }
            </table>
        </body>
    </html>

