xquery version "3.1";

module namespace newhome="http://grammateus.ch/pages/newhome";

import module namespace g="http://grammateus.ch/global" at "../global.xql";

declare function newhome:page() {
    <div class="home card-deck">
        <div class="row">   
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-home-TM30319.jpg" class="card-img-top" alt="SB 12 11106"/>
                    <div class="home-img">
                        <a href="{$g:root}introduction/general">Introduction</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        Grammateus project Introduction
                    </h5>
                    <p class="card-text">An <a href="{$g:root}introduction/general">overview</a> of the project explains our perspective on the typology of Greek documentary papyri, and how to use the website. The <a href="{$g:root}introduction/concepts">key concepts</a> offer definitions of the main concepts used to classify papyri in the typology.</p>
                </div>
            </div>
            
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-home-TM12167-dark.jpg" class="card-img-top" alt="P.Mich. 6 364"/>
                    <div class="home-img">
                        <a href="{$g:root}typology">Typology</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        Typology of Documentary Papyri
                    </h5>
                <p class="card-text">This <a href="{$g:root}typology">typology</a> is an attempt to identify the main types of document, and provide a detailed <a href="{$g:root}descriptions">description</a> of each type as well as an explanation of the scientific decisions behind the <a href="{$g:root}classification">classification</a>.</p>
<!--                    <p>It takes into account the textual content as well as material aspects of papyri: size and shape of the papyrus, direction of the fibres, organization of the text on the page.</p>-->
                </div>
            </div>
            
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-home-TM78.jpg" class="card-img-top" alt="P.Koln 1 50"/>
                    <div class="home-img">
                        <a href="{$g:root}papyri.html">Papyri</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        Papyri
                    </h5>
                <p class="card-text">The database of <a href="{$g:root}papyri.html">papyri</a> can be searched and filtered according to various criteria.</p>
                </div>
            </div>
            
        </div>
        <div class="row">   
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-home-TM5245.jpg" class="card-img-top" alt="P.Mich. 3 200"/>
                    <div class="home-img">
                        <a href="{$g:root}compare">Compare</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        Compare types of papyri
                    </h5>
                    <p class="card-text">With the <a href="{$g:root}compare">Compare</a> tool, get an overview of two categories of papyri side-by-side.</p>
                </div>
            </div>
            
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-home-TM8337.jpg" class="card-img-top" alt="P.Mich. 3 173"/>
                    <div class="home-img">
                        <a href="{$g:root}syntax">Syntax</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        Syntax
                    </h5>
                <p class="card-text">On the <a href="{$g:root}syntax">Syntax</a> page, it is possible to search a table of the formulas identified in the <a href="{$g:root}introduction/concepts#syntax">text structure</a> and relevant to the typology.</p>
                </div>
            </div>
            
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-home-TM21351.jpg" class="card-img-top" alt="P.Mich. 3 221"/>
                    <div class="home-img">
                        <a href="{$g:root}howto-cite">How&#160;to&#160;Cite</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        How to Cite grammateus
                    </h5>
                <p class="card-text">Guide for citing the various parts of the grammateus project.</p>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="card">
                <div class="home-img-container">
                    <!--<img src="{$g:img}bg-peripleo.jpg" class="card-img-top" alt="Peripleo map for grammateus"/>-->
                    <div class="home-img">
                        <a href="#"></a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        
                    </h5>
                <p class="card-text">
                    
                </p>
                </div>
            </div>
            <div class="card">
                <div class="home-img-container">
                    <img src="{$g:img}bg-peripleo.jpg" class="card-img-top" alt="Peripleo map for grammateus"/>
                    <div class="home-img-dark">
                        <a href="https://enury.github.io/peripleo-grammateus">Map</a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        Map
                    </h5>
                <p class="card-text">
                    View the papyri of our database on a <a href="https://enury.github.io/peripleo-grammateus">map</a> created with <a href="https://britishlibrary.github.io/locating-a-national-collection/home.html">Peripleo</a>.
                </p>
                </div>
            </div>
            <div class="card">
                <div class="home-img-container">
                    <!--<img src="{$g:img}bg-peripleo.jpg" class="card-img-top" alt="Peripleo map for grammateus"/>-->
                    <div class="home-img">
                        <a href="#"></a>
                    </div>
                </div>
                <div class="card-body">
                    <h5 class="card-title h5">
                        
                    </h5>
                <p class="card-text">
                    
                </p>
                </div>
            </div>
        </div>
    </div>
};